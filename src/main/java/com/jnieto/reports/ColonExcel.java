package com.jnieto.reports;

import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.jnieto.dao.RegistroColonDAO;
import com.jnieto.entity.RegistroColon;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;

/**
 * The Class ColonExcel. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class ColonExcel {

	private static final Logger logger = LogManager.getLogger(ColonExcel.class);

	private static String[] columns = { "Nhc", "Apellidos y nombre", "Fecha TSOH,", "TSHO", "Cita Colon", "Inf.Colon ",
			"Fecha Anato ", "Observaciones" };

	private final LocalDate desde;

	private final LocalDate hasta;

	private final String filenameString = Constantes.DIRECTORIOREPORTS + Long.toString(Utilidades.getHoraNumeroAcual())
			+ "colon.xlsx";

	private final String urlfile = Constantes.URLREPORTS + Long.toString(Utilidades.getHoraNumeroAcual())
			+ "colon.xlsx";;

	private DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");

	public ColonExcel(LocalDate desde, LocalDate hasta) {

		this.desde = desde;

		this.hasta = hasta;

		try {
			Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("Registrocolon");

			Font headerFont = workbook.createFont();

			headerFont.setBold(true);

			headerFont.setFontHeightInPoints((short) 14);

			headerFont.setColor(IndexedColors.RED.getIndex());

			CellStyle headerCellStyle = workbook.createCellStyle();

			headerCellStyle.setFont(headerFont);

			org.apache.poi.ss.usermodel.Row headerRow = sheet.createRow(0);

			for (int i = 0; i < columns.length; i++) {
				Cell cell = headerRow.createCell(i);

				cell.setCellValue(columns[i]);

				cell.setCellStyle(headerCellStyle);
			}
			CellStyle dateCellStyle = workbook.createCellStyle();

			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

			ArrayList<RegistroColon> listadatos = new RegistroColonDAO().listaRegistroColon(desde, hasta);

			int rowNum = 1;

			for (RegistroColon rc : listadatos) {

				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(rc.getId());

				row.createCell(1).setCellValue(rc.getPaciente().getApellidosNombre());

				Cell dateOfBirthCell = row.createCell(2);

				if (rc.getAnaliticaDate() != null) {

					dateOfBirthCell.setCellValue(fechadma.format(rc.getAnaliticaDate()));

					dateOfBirthCell.setCellStyle(dateCellStyle);
				}

				if (rc.getResultadoString() != null) {

					row.createCell(3).setCellValue(rc.getResultadoString());

				} else {

					row.createCell(3);

				}

				row.createCell(4);
				if (rc.getCitaColonoDate() != null) {
					dateOfBirthCell.setCellValue(fechadma.format(rc.getCitaColonoDate()));
					// dateOfBirthCell.setCellStyle(dateCellStyle);
				}
				row.createCell(5);
				if (rc.getInformeColonoDate() != null) {
					dateOfBirthCell.setCellValue(fechadma.format(rc.getInformeColonoDate()));
					// dateOfBirthCell.setCellStyle(dateCellStyle);
				}
				row.createCell(6);
				if (rc.getInformeAnatoDate() != null) {
					dateOfBirthCell.setCellValue(fechadma.format(rc.getInformeAnatoDate()));
					// dateOfBirthCell.setCellStyle(dateCellStyle);
				}
				if (rc.getObservacionesValor() != null) {
					row.createCell(7).setCellValue(rc.getObservacionesValor());
				} else {
					row.createCell(7);
				}

			}
			for (int i = 0; i < columns.length; i++) {
				sheet.autoSizeColumn(i);
			}

			FileOutputStream fileOut;

			fileOut = new FileOutputStream(filenameString);

			workbook.write(fileOut);
			fileOut.close();
			workbook.close();

		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
	}

	public String getFilename() {
		return filenameString;
	}

	public String getUrl() {
		return urlfile;
	}

}
