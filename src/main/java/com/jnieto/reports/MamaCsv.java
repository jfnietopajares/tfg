package com.jnieto.reports;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.dao.RegistroMamaDAO;
import com.jnieto.entity.RegistroMama;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;
import com.opencsv.CSVWriter;

public class MamaCsv {

	private static final Logger logger = LogManager.getLogger(MamaCsv.class);

	public static final String DESTINO = Constantes.DIRECTORIOREPORTS + Long.toString(Utilidades.getHoraNumeroAcual())
			+ "mama.csv";

	private final String urlfile = Constantes.URLREPORTS + Long.toString(Utilidades.getHoraNumeroAcual()) + "mama.csv";

	private LocalDate desde;

	private LocalDate hasta;

	public MamaCsv(LocalDate desde, LocalDate hasta) {
		this.desde = desde;
		this.hasta = hasta;
		this.generaFichero();
	}

	public String getNombreFichero() {
		return DESTINO;
	}

	public String getUrl() {
		return urlfile;
	}

	public void generaFichero() {
		try {
			ArrayList<RegistroMama> listadatos = new RegistroMamaDAO().getListaRegistros(desde, hasta);

			Writer writer = new FileWriter(DESTINO);

			// using custom delimiter and quote character
			CSVWriter csvWriter = new CSVWriter(writer, ',', '\'');

			List<String[]> data = toStringArray(listadatos);

			csvWriter.writeAll(data);

			csvWriter.close();
		} catch (IOException e) {
			logger.error(NotificacionInfo.EXCEPTION_IO, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
	}

	private static List<String[]> toStringArray(ArrayList<RegistroMama> datos) {
		DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		List<String[]> records = new ArrayList<String[]>();

		records.add(new String[] { "Nhc", "Apellidos y nombre", "Fecha AP.,", "Birdras", "Tnm", "Fecha RX.",
				"Birdras 2", "Fecha Gin", "F. Comite", "F.TTo.", "Observaciones" });

		for (RegistroMama rm : datos) {

			records.add(new String[] { (rm.getPaciente() == null) ? "" : rm.getPaciente().getId().toString(),
					(rm.getPaciente() == null) ? "" : rm.getPaciente().getApellidosNombre(),
					(rm.getFechaapValue() == null) ? "" : fechadma.format(rm.getFechaapValue()),
					(rm.getBirads().getValor() == null) ? " " : rm.getBirads().getValor(),
					(rm.getTnm().getValor() == null) ? " " : rm.getTnm().getValor(),
					(rm.getFechapruebaValue() == null) ? "" : fechadma.format(rm.getFechapruebaValue()),
					(rm.getBiradscorregido().getValor() == null) ? "" : rm.getBiradscorregido().getValor(),
					(rm.getFechagineValue() == null) ? "" : fechadma.format(rm.getFechagineValue()),
					(rm.getFechacomiteValue() == null) ? "" : fechadma.format(rm.getFechacomiteValue()),
					(rm.getFechatratamientoValue() == null) ? "" : fechadma.format(rm.getFechatratamientoValue()),
					(rm.getObservaciones().getValor() == null) ? " " : rm.getObservaciones().getValor() });
		}
		return records;
	}
}
