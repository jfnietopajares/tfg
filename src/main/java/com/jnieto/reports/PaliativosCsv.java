package com.jnieto.reports;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.dao.ProcesoDAO;
import com.jnieto.entity.Proceso;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.opencsv.CSVWriter;

public class PaliativosCsv {
	private static final Logger logger = LogManager.getLogger(MamaCsv.class);

	public static final String DESTINO = Constantes.DIRECTORIOREPORTS + "mama.csv";

	private LocalDate desde;

	private LocalDate hasta;

	public PaliativosCsv(LocalDate desde, LocalDate hasta) {
		this.desde = desde;
		this.hasta = hasta;
		this.generaFichero();
	}

	public String getNombreFichero() {
		return DESTINO;
	}

	public void generaFichero() {
		try {
			ArrayList<Proceso> listadatos = new ProcesoDAO().getListaProcesos(Proceso.SUBAMBITO_PALIATIVOS, desde,
					hasta);

			Writer writer = new FileWriter(DESTINO);

			CSVWriter csvWriter = new CSVWriter(writer, ',', '\'');

			List<String[]> data = toStringArray(listadatos);

			csvWriter.writeAll(data);

			csvWriter.close();
		} catch (IOException e) {
			logger.error(NotificacionInfo.EXCEPTION_IO, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
	}

	private static List<String[]> toStringArray(ArrayList<Proceso> datos) {
		DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		List<String[]> records = new ArrayList<String[]>();

		records.add(new String[] { "Nhc", "Apellidos y nombre", "Fecha Ini.,", "Motivo", "Fecha Fin", "Motivo" });

		for (Proceso proceso : datos) {

			records.add(new String[] { proceso.getPaciente().getNumerohc().toString(),
					proceso.getPaciente().getApellidosNombre(),
					(proceso.getFechaini() == null) ? "" : fechadma.format(proceso.getFechaini()),
					(proceso.getMotivo() == null) ? " " : proceso.getMotivo(),
					(proceso.getFechafin() == null) ? " " : fechadma.format(proceso.getFechafin()),
					(proceso.getObservaciones() == null) ? " " : proceso.getObservaciones() });
		}
		return records;
	}
}
