package com.jnieto.reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.io.IOException;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.jnieto.dao.ProcesoDAO;
import com.jnieto.entity.Proceso;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;

public class OxigenoAltasBajasPdf {

	private static final Logger logger = LogManager.getLogger(PaliativosPdf.class);

	public static final String DEST = Constantes.DIRECTORIOREPORTS + "oxigenoAltasBajas"
			+ Long.toString(Utilidades.getHoraNumeroAcual()) + ".pdf";

	private File file;

	private LocalDate desde;

	private LocalDate hasta;

	DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");

	public OxigenoAltasBajasPdf(LocalDate desde, LocalDate hasta) {
		this.desde = desde;
		this.hasta = hasta;
		file = new File(DEST);
		if (file.exists())
			file.delete();

		file.getParentFile().mkdirs();

		this.createPdf(DEST);

	}

	public static String getDest() {
		return DEST;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public void createPdf(String dest) {

		PdfDocument pdf;
		try {

			pdf = new PdfDocument(new PdfWriter(dest));

			Document document = new Document(pdf, PageSize.A4).setTextAlignment(TextAlignment.JUSTIFIED);

			document.setMargins(75, 36, 75, 36);

			PdfEventoPagina evento = new PdfEventoPagina(document, "Listado de casos de paliativos desde "
					+ fechadma.format(desde) + " hasta " + fechadma.format(hasta));

			pdf.addEventHandler(PdfDocumentEvent.END_PAGE, evento);

			PdfFont normal = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);

			PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);

			Float altura = new Float(30f);
			int sizeFont = 7;

			float[] anchos = { 30f, 140f, 45f, 105f, 45f, 110f };
			Table tabla = new Table(anchos);

			String[] tituloSrings = { "Nhc", "Apellidos y nombre", "Fecha I,", "Motivo", "Fecha Fin ", "Motivo Baja." };
			tabla.setMarginTop(10);
			for (int i = 0; i < tituloSrings.length; i++) {
				Cell celdaCell = new Cell();
				celdaCell.add(new Paragraph(tituloSrings[i]).setFontSize(sizeFont));
				celdaCell.setHeight(altura);
				tabla.addCell(celdaCell);
			}
			ArrayList<Proceso> listadatos = new ProcesoDAO().getListaProcesos(Proceso.SUBAMBITO_OXIGENO, desde, hasta);

			for (Proceso proceso : listadatos) {

				if (proceso.getPaciente() != null && proceso.getPaciente().getNumerohc() != null) {
					tabla.addCell(
							new Cell().add(new Paragraph(proceso.getPaciente().getNumerohc())).setFontSize(sizeFont));
				} else {
					tabla.addCell(new Cell().add(new Paragraph("")));
				}

				if (proceso.getPaciente() != null && proceso.getPaciente().getApellidosNombre() != null) {
					if (proceso.getPaciente().getApellidosNombre().length() > 32) {
						tabla.addCell(new Cell()
								.add(new Paragraph(proceso.getPaciente().getApellidosNombre().substring(0, 32)))
								.setFontSize(sizeFont));
					} else {
						tabla.addCell(new Cell().add(new Paragraph(proceso.getPaciente().getApellidosNombre()))
								.setFontSize(sizeFont));
					}
				} else {
					tabla.addCell(new Cell().add(new Paragraph("")));
				}

				if (proceso.getFechaini() != null) {
					tabla.addCell(new Cell().add(new Paragraph(fechadma.format(proceso.getFechaini())))
							.setFontSize(sizeFont));
				} else {
					tabla.addCell(new Cell().add(new Paragraph("")));
				}

				if (proceso.getMotivo() != null) {
					tabla.addCell(new Cell().add(new Paragraph(proceso.getMotivo())).setFontSize(sizeFont));
				} else {
					tabla.addCell(new Cell().add(new Paragraph("")));
				}
			}
			document.add(tabla);

			document.close();
		} catch (FileNotFoundException e) {
			logger.error(NotificacionInfo.EXCEPTION_FILENOTFOUND, e);
		} catch (IOException e) {
			logger.error(NotificacionInfo.EXCEPTION_IO, e);
		} catch (java.io.IOException e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}

	}
}
