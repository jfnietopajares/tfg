package com.jnieto.entity;

/**
 * The Class RegistroOxiAerosol. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class RegistroOxiAerosol extends RegistroOxi {

	private Variable sesionesDia;

	private Variable numeroDias;

	public final static String TERAPIA_VALOR_DEFECTO = "Aerosolterapia";

	public final static Long PLANTILLLA_EDITOR_REGISTROAEROSOL = new Long(10000001);

	public final Variable VAR_SESIONES_DIA = new Variable("13818048", "99G2", new Long(13818048), "Sesiones día");

	public final Variable VAR_NUMERO_DE_DIAS = new Variable("13818049", "99G2", new Long(13818049), "Número de días");

	/**
	 * Instantiates a new registro oxi aerosol.
	 */
	public RegistroOxiAerosol() {
		super();
		this.iniciaAerosol();
	}

	/**
	 * Instantiates a new registro oxi aerosol.
	 *
	 * @param id the id
	 */
	public RegistroOxiAerosol(Long id) {
		super(id);
		this.iniciaAerosol();
	}

	/**
	 * Instantiates a new registro oxi aerosol.
	 *
	 * @param r the r
	 */
	public RegistroOxiAerosol(RegistroOxiAerosol r) {
		super(r);
		this.sesionesDia = r.sesionesDia;
		this.numeroDias = r.numeroDias;
	}

	/**
	 * Inicia aerosol.
	 */
	public void iniciaAerosol() {
		setTerapia(RegistroOxiAerosol.TERAPIA_VALOR_DEFECTO);
		setPlantilla_edior(RegistroOxiAerosol.PLANTILLLA_EDITOR_REGISTROAEROSOL);
		setDescripcion(RegistroOxiAerosol.TERAPIA_VALOR_DEFECTO);
		this.sesionesDia = VAR_SESIONES_DIA;
		this.numeroDias = VAR_NUMERO_DE_DIAS;
	}

	public Variable getVariableSesionesDia() {
		return this.sesionesDia;
	}

	public String getSesionesDiaString() {
		return this.sesionesDia.getValor();
	}

	public void setSesionesDia(Variable sesionesDia) {
		this.sesionesDia = sesionesDia;
	}

	public void setSesionesDia(String valor) {
		this.sesionesDia.setValor(valor);
	}

	public Variable getVariableNumeroDias() {
		return this.numeroDias;
	}

	public String getNumeroDiasString() {
		return this.numeroDias.getValor();
	}

	public void setNumeroDias(Variable numeroDias) {
		this.numeroDias = numeroDias;
	}

	public void setNumeroDias(String valor) {
		this.numeroDias.setValor(valor);
	}
}
