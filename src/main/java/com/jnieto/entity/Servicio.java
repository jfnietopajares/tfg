package com.jnieto.entity;

/**
 * The Class Servicio. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class Servicio {

	/** The numero orden. */
	private int numeroOrden;

	/** The id. */
	private Long id;

	/** The codigo. */
	private String codigo;

	/** The descripcion. */
	private String descripcion;

	/** The clase. */
	private Long clase;

	/**
	 * Instantiates a new servicio.
	 */
	public Servicio() {
		this.id = new Long(0);
	}

	/**
	 * Instantiates a new servicio.
	 *
	 * @param id the id
	 */
	public Servicio(Long id) {
		this.id = id;
	}

	/**
	 * Instantiates a new servicio.
	 *
	 * @param id          the id
	 * @param codigo      the codigo
	 * @param descripcion the descripcion
	 */
	public Servicio(Long id, String codigo, String descripcion) {
		this.id = id;
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	/**
	 * Instantiates a new servicio.
	 *
	 * @param codigo      the codigo
	 * @param descripcion the descripcion
	 */
	public Servicio(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	/**
	 * Gets the numero orden.
	 *
	 * @return the numero orden
	 */
	public int getNumeroOrden() {
		return numeroOrden;
	}

	/**
	 * Sets the numero orden.
	 *
	 * @param numeroOrden the new numero orden
	 */
	public void setNumeroOrden(int numeroOrden) {
		this.numeroOrden = numeroOrden;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Gets the string id.
	 *
	 * @return the string id
	 */
	public String getStringId() {
		return Long.toString(getId());
	}

	/**
	 * Sets the id.
	 *
	 * @param i the new id
	 */
	public void setId(Long i) {
		this.id = i;
	}

	/**
	 * Sets the string id.
	 *
	 * @param id the new string id
	 */
	public void setStringId(String id) {
		this.id = Long.parseLong(id);
	}

	/**
	 * Gets the codigo.
	 *
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Sets the codigo.
	 *
	 * @param codigo the new codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Gets the clase.
	 *
	 * @return the clase
	 */
	public Long getClase() {
		return clase;
	}

	/**
	 * Sets the clase.
	 *
	 * @param clase the new clase
	 */
	public void setClase(Long clase) {
		this.clase = clase;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	public String toString() {
		return "id=" + this.getId() + "\n" + "codigo=" + this.getCodigo() + "\n" + "descripcion="
				+ this.getDescripcion() + "\n" + "clase=" + this.getClase();
	}
}
