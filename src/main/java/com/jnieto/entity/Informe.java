package com.jnieto.entity;

import java.io.File;

import com.mysql.jdbc.Blob;

/**
 * The Class Informe. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class Informe {

	/** The numero orden. */
	private int numeroOrden;

	/** The id. */
	private Long id;

	/** The descripcion. */
	private String descripcion;

	/** The paciente. */
	private Paciente paciente;

	/** The episodio. */
	private Long episodio;

	/** The centro. */
	private Centro centro;

	/** The servicio. */
	private Servicio servicio;

	/** The referecia. */
	private String referecia;

	/** The fecha. */
	private Long fecha;

	/** The hora. */
	private Long hora;

	/** The estado. */
	private int estado;

	/** The docuxml. */
	private Blob docuxml;

	/** The tipoxml. */
	private int tipoxml;

	/** The docubin. */
	private Blob docubin;

	/** The tipobin. */
	private int tipobin;

	/** The peticion. */
	private Long peticion;

	/** The userid. */
	private Usuario userid;

	/** The canal. */
	private Long canal;

	/** The tipoinforme. */
	private int tipoinforme;

	/** The useridauth. */
	private Usuario useridauth;

	/** The srvauth. */
	private Servicio srvauth;

	/** The useridredactor. */
	private Usuario useridredactor;

	/** The plantalla editor. */
	private Long plantalla_editor;

	/** The flag. */
	private int flag;

	/** The pertenece. */
	private Long pertenece;

	/** The version. */
	private int version;

	/** The nive visibilidad. */
	private int nive_visibilidad;

	/** The subservicio. */
	private Long subservicio;

	/** The useridpeticionario. */
	private Usuario useridpeticionario;

	/** The visto. */
	private int visto;

	/** The ultimoguardado. */
	private Long ultimoguardado;

	/** The bloqueado. */
	private int bloqueado;

	/** The almacenamiento. */
	private Long almacenamiento;

	/** The tipo documento. */
	private Long tipo_documento;

	/** The ambito. */
	private Long ambito;

	/** The servicio realizador. */
	private Servicio servicio_realizador;

	/** The fecha proceso. */
	private Long fecha_proceso;

	/** The referencia almacenamiento. */
	private String referencia_almacenamiento;

	/** The num accesos. */
	private int num_accesos;

	/** The problema. */
	private Proceso problema;

	/** The user visto. */
	private Usuario user_visto;

	/** The fecha visto. */
	private Long fecha_visto;

	/** The comentario. */
	private String comentario;

	/** The fichero informe file. */
	private File ficheroInformeFile;

	/** The Constant TIPO_DOCUMENTO_MAMA. */
	public final static Long TIPO_DOCUMENTO_MAMA = new Long(21);

	/** The Constant VAR_RESGISTRO_ESTADO_NORMAL. */
	public final static int VAR_RESGISTRO_ESTADO_NORMAL = 2;

	/** The Constant VAR_RESGISTRO_ESTADO_SUSTITUIDO. */
	public final static int VAR_RESGISTRO_ESTADO_SUSTITUIDO = 5;

	/** The Constant CANAL_DEFECTO. */
	public final static Long CANAL_DEFECTO = new Long(6);

	/**
	 * Instantiates a new informe.
	 */
	public Informe() {
		this.id = new Long(0);
		centro = Centro.CENTRO_DEFECTO;
		estado = VAR_RESGISTRO_ESTADO_NORMAL;
		tipoxml = 0;
		tipobin = 1;
		canal = CANAL_DEFECTO;
		flag = 0;
		version = 1;
		ultimoguardado = new Long(0);
		bloqueado = 0;
		ambito = new Long(17);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Long getEpisodio() {
		return episodio;
	}

	public void setEpisodio(Long episodio) {
		this.episodio = episodio;
	}

	public Centro getCentro() {
		return centro;
	}

	public void setCentro(Centro centro) {
		this.centro = centro;
	}

	public Servicio getServicio() {
		return servicio;
	}

	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

	public String getReferecia() {
		return referecia;
	}

	public void setReferecia(String referecia) {
		this.referecia = referecia;
	}

	public Long getFecha() {
		return fecha;
	}

	public void setFecha(Long fecha) {
		this.fecha = fecha;
	}

	public Long getHora() {
		return hora;
	}

	public void setHora(Long hora) {
		this.hora = hora;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Blob getDocuxml() {
		return docuxml;
	}

	public void setDocuxml(Blob docuxml) {
		this.docuxml = docuxml;
	}

	public int getTipoxml() {
		return tipoxml;
	}

	public void setTipoxml(int tipoxml) {
		this.tipoxml = tipoxml;
	}

	public Blob getDocubin() {
		return docubin;
	}

	public void setDocubin(Blob docubin) {
		this.docubin = docubin;
	}

	public int getTipobin() {
		return tipobin;
	}

	public void setTipobin(int tipobin) {
		this.tipobin = tipobin;
	}

	public Long getPeticion() {
		return peticion;
	}

	public void setPeticion(Long peticion) {
		this.peticion = peticion;
	}

	public Usuario getUserid() {
		return userid;
	}

	public void setUserid(Usuario userid) {
		this.userid = userid;
	}

	public Long getCana() {
		return canal;
	}

	public void setCana(Long canal) {
		this.canal = canal;
	}

	public int getTipoinforme() {
		return tipoinforme;
	}

	public void setTipoinforme(int tipoinforme) {
		this.tipoinforme = tipoinforme;
	}

	public Usuario getUseridauth() {
		return useridauth;
	}

	public void setUseridauth(Usuario useridauth) {
		this.useridauth = useridauth;
	}

	public Servicio getSrvauth() {
		return srvauth;
	}

	public void setSrvauth(Servicio srvauth) {
		this.srvauth = srvauth;
	}

	public Usuario getUseridredactor() {
		return useridredactor;
	}

	public void setUseridredactor(Usuario useridredactor) {
		this.useridredactor = useridredactor;
	}

	public Long getPlantalla_editor() {
		return plantalla_editor;
	}

	public void setPlantalla_editor(Long plantalla_editor) {
		this.plantalla_editor = plantalla_editor;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public Long getPertenece() {
		return pertenece;
	}

	public void setPertenece(Long pertenece) {
		this.pertenece = pertenece;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getNive_visibilidad() {
		return nive_visibilidad;
	}

	public void setNive_visibilidad(int nive_visibilidad) {
		this.nive_visibilidad = nive_visibilidad;
	}

	public Long getSubservicio() {
		return subservicio;
	}

	public void setSubservicio(Long subservicio) {
		this.subservicio = subservicio;
	}

	public Usuario getUseridpeticionario() {
		return useridpeticionario;
	}

	public void setUseridpeticionario(Usuario useridpeticionario) {
		this.useridpeticionario = useridpeticionario;
	}

	public int getVisto() {
		return visto;
	}

	public void setVisto(int visto) {
		this.visto = visto;
	}

	public Long getUltimoguardado() {
		return ultimoguardado;
	}

	public void setUltimoguardado(Long ultimoguardado) {
		this.ultimoguardado = ultimoguardado;
	}

	public int getBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(int bloqueado) {
		this.bloqueado = bloqueado;
	}

	public Long getAlmacenamiento() {
		return almacenamiento;
	}

	public void setAlmacenamiento(Long almacenamiento) {
		this.almacenamiento = almacenamiento;
	}

	public Long getTipo_documento() {
		return tipo_documento;
	}

	public void setTipo_documento(Long tipo_documento) {
		this.tipo_documento = tipo_documento;
	}

	public Long getAmbito() {
		return ambito;
	}

	public void setAmbito(Long ambito) {
		this.ambito = ambito;
	}

	public Servicio getServicio_realizador() {
		return servicio_realizador;
	}

	public void setServicio_realizador(Servicio servicio_realizador) {
		this.servicio_realizador = servicio_realizador;
	}

	public Long getFecha_proceso() {
		return fecha_proceso;
	}

	public void setFecha_proceso(Long fecha_proceso) {
		this.fecha_proceso = fecha_proceso;
	}

	public String getReferencia_almacenamiento() {
		return referencia_almacenamiento;
	}

	public void setReferencia_almacenamiento(String referencia_almacenamiento) {
		this.referencia_almacenamiento = referencia_almacenamiento;
	}

	public int getNum_accesos() {
		return num_accesos;
	}

	public void setNum_accesos(int num_accesos) {
		this.num_accesos = num_accesos;
	}

	public Proceso getProblema() {
		return problema;
	}

	public void setProblema(Proceso problema) {
		this.problema = problema;
	}

	public Usuario getUser_visto() {
		return user_visto;
	}

	public void setUser_visto(Usuario user_visto) {
		this.user_visto = user_visto;
	}

	public Long getFecha_visto() {
		return fecha_visto;
	}

	public void setFecha_visto(Long fecha_visto) {
		this.fecha_visto = fecha_visto;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public int getNumeroOrden() {
		return numeroOrden;
	}

	public void setNumeroOrden(int numeroOrden) {
		this.numeroOrden = numeroOrden;
	}

	public File getFicheroInformeFile() {
		return ficheroInformeFile;
	}

	public void setFicheroInformeFile(File ficheroInformeFile) {
		this.ficheroInformeFile = ficheroInformeFile;
	}

	/**
	 * Gets the descripcion subambito.
	 *
	 * @param subambito the subambito
	 * @return the descripcion subambito
	 */
	public static String getDescripcionSubambito(Long subambito) {
		String deString = "";
		if (subambito.equals(Proceso.SUBAMBITO_MAMA)) {
			deString = " Informe Screening Mama";
		} else if (subambito.equals(Proceso.SUBAMBITO_COLON)) {
			deString = " Informe Screening Colon";
		} else if (subambito.equals(Proceso.SUBAMBITO_PALIATIVOS)) {
			deString = " Informe paliativos";
		} else if (subambito.equals(Proceso.SUBAMBITO_OXIGENO)) {
			deString = " Informe Oxígenoterapia ";
		}
		return deString;
	}

	/**
	 * Gets the tipo documento subambito.
	 *
	 * @param subambito the subambito
	 * @return the tipo documento subambito
	 */
	public static long getTipoDocumentoSubambito(Long subambito) {
		Long tipoLong = new Long(0);
		if (subambito.equals(Proceso.SUBAMBITO_MAMA)) {
			tipoLong = new Long(21);
		} else if (subambito.equals(Proceso.SUBAMBITO_COLON)) {
			tipoLong = new Long(21);
		} else if (subambito.equals(Proceso.SUBAMBITO_PALIATIVOS)) {
			tipoLong = new Long(21);
		} else if (subambito.equals(Proceso.SUBAMBITO_OXIGENO)) {
			tipoLong = new Long(21);
		}
		return tipoLong;

	}

}
