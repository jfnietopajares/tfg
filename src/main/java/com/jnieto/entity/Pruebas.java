package com.jnieto.entity;

/**
 * The Class Pruebas.
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class Pruebas {

	private Long id;

	private Long peticion;

	private Long catalogo;

	private String valor;

	private String descripcion;

	private Long catalogo_valor;

	private String grupo;

	private Long Catalogo_grupo;

	private Long tipo_opcion;

	private String opcion;

	private String code;

	private String codesystem;

	private Long lateralidad;

	private int estado;

	private Long fechaprogramacion;

	private int horaprogramacion;

	private String motivo;

	/**
	 * Instantiates a new pruebas.
	 */
	public Pruebas() {

	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the peticion.
	 *
	 * @return the peticion
	 */
	public Long getPeticion() {
		return peticion;
	}

	/**
	 * Sets the peticion.
	 *
	 * @param peticion the new peticion
	 */
	public void setPeticion(Long peticion) {
		this.peticion = peticion;
	}

	/**
	 * Gets the catalogo.
	 *
	 * @return the catalogo
	 */
	public Long getCatalogo() {
		return catalogo;
	}

	/**
	 * Sets the catalogo.
	 *
	 * @param catalogo the new catalogo
	 */
	public void setCatalogo(Long catalogo) {
		this.catalogo = catalogo;
	}

	/**
	 * Gets the valor.
	 *
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * Sets the valor.
	 *
	 * @param valor the new valor
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Gets the catalogo valor.
	 *
	 * @return the catalogo valor
	 */
	public Long getCatalogo_valor() {
		return catalogo_valor;
	}

	/**
	 * Sets the catalogo valor.
	 *
	 * @param catalogo_valor the new catalogo valor
	 */
	public void setCatalogo_valor(Long catalogo_valor) {
		this.catalogo_valor = catalogo_valor;
	}

	/**
	 * Gets the grupo.
	 *
	 * @return the grupo
	 */
	public String getGrupo() {
		return grupo;
	}

	/**
	 * Sets the grupo.
	 *
	 * @param grupo the new grupo
	 */
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	/**
	 * Gets the catalogo grupo.
	 *
	 * @return the catalogo grupo
	 */
	public Long getCatalogo_grupo() {
		return Catalogo_grupo;
	}

	/**
	 * Sets the catalogo grupo.
	 *
	 * @param catalogo_grupo the new catalogo grupo
	 */
	public void setCatalogo_grupo(Long catalogo_grupo) {
		Catalogo_grupo = catalogo_grupo;
	}

	/**
	 * Gets the tipo opcion.
	 *
	 * @return the tipo opcion
	 */
	public Long getTipo_opcion() {
		return tipo_opcion;
	}

	/**
	 * Sets the tipo opcion.
	 *
	 * @param tipo_opcion the new tipo opcion
	 */
	public void setTipo_opcion(Long tipo_opcion) {
		this.tipo_opcion = tipo_opcion;
	}

	/**
	 * Gets the opcion.
	 *
	 * @return the opcion
	 */
	public String getOpcion() {
		return opcion;
	}

	/**
	 * Sets the opcion.
	 *
	 * @param opcion the new opcion
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the codesystem.
	 *
	 * @return the codesystem
	 */
	public String getCodesystem() {
		return codesystem;
	}

	/**
	 * Sets the codesystem.
	 *
	 * @param codesystem the new codesystem
	 */
	public void setCodesystem(String codesystem) {
		this.codesystem = codesystem;
	}

	/**
	 * Gets the lateralidad.
	 *
	 * @return the lateralidad
	 */
	public Long getLateralidad() {
		return lateralidad;
	}

	/**
	 * Sets the lateralidad.
	 *
	 * @param lateralidad the new lateralidad
	 */
	public void setLateralidad(Long lateralidad) {
		this.lateralidad = lateralidad;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public int getEstado() {
		return estado;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado the new estado
	 */
	public void setEstado(int estado) {
		this.estado = estado;
	}

	/**
	 * Gets the fechaprogramacion.
	 *
	 * @return the fechaprogramacion
	 */
	public Long getFechaprogramacion() {
		return fechaprogramacion;
	}

	/**
	 * Sets the fechaprogramacion.
	 *
	 * @param fechaprogramacion the new fechaprogramacion
	 */
	public void setFechaprogramacion(Long fechaprogramacion) {
		this.fechaprogramacion = fechaprogramacion;
	}

	/**
	 * Gets the horaprogramacion.
	 *
	 * @return the horaprogramacion
	 */
	public int getHoraprogramacion() {
		return horaprogramacion;
	}

	/**
	 * Sets the horaprogramacion.
	 *
	 * @param horaprogramacion the new horaprogramacion
	 */
	public void setHoraprogramacion(int horaprogramacion) {
		this.horaprogramacion = horaprogramacion;
	}

	/**
	 * Gets the motivo.
	 *
	 * @return the motivo
	 */
	public String getMotivo() {
		return motivo;
	}

	/**
	 * Sets the motivo.
	 *
	 * @param motivo the new motivo
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

}
