package com.jnieto.entity;

/**
 * The Class Variable. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class Variable {

	private String code;

	private String codesystem;

	private Long item;

	private String descripcion;

	private String valor;

	/**
	 * Instantiates a new variable.
	 */
	public Variable() {

	}

	/**
	 * Instantiates a new variable.
	 *
	 * @param code        the code
	 * @param codesystem  the codesystem
	 * @param item        the item
	 * @param descripcion the descripcion
	 */
	public Variable(String code, String codesystem, Long item, String descripcion) {
		this.code = code;
		this.codesystem = codesystem;
		this.item = item;
		this.descripcion = descripcion;
		this.valor = "";
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the codesystem.
	 *
	 * @return the codesystem
	 */
	public String getCodesystem() {
		return codesystem;
	}

	/**
	 * Sets the codesystem.
	 *
	 * @param codesystem the new codesystem
	 */
	public void setCodesystem(String codesystem) {
		this.codesystem = codesystem;
	}

	/**
	 * Gets the item.
	 *
	 * @return the item
	 */
	public Long getItem() {
		return item;
	}

	/**
	 * Sets the item.
	 *
	 * @param item the new item
	 */
	public void setItem(Long item) {
		this.item = item;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Gets the valor.
	 *
	 * @return the valor
	 */
	public String getValor() {
		if (valor != null)
			return valor;
		else {
			return "";
		}
	}

	/**
	 * Sets the valor.
	 *
	 * @param valor the new valor
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	public String toString() {
		return this.item.toString() + "=" + this.getItem() + "\n" + this.getDescripcion() + "=" + this.getValor()
				+ this.getValor() + "=" + this.getValor() + "\n";
	}
}
