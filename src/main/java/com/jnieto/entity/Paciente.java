package com.jnieto.entity;

import java.time.LocalDate;
import java.util.ArrayList;

import com.jnieto.utilidades.Utilidades;

/**
 * The Class Paciente. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class Paciente {

	private int numeroOrden;
	private Long id;
	private String nombre;
	private String apellido1;
	private String apellido2;
	private String numerohc;
	private String dni;
	private String direccion;
	private Provincia provincia;
	private Municipio municipio;
	private String codigopostal;
	private LocalDate fnac;
	private Integer sexo;
	private String telefono;
	private String movil;
	private ArrayList<Registro> listaRegistros = new ArrayList<>();

	/**
	 * Instantiates a new paciente.
	 */
	public Paciente() {
		this.id = new Long(0);
		valoresPorDefecto();
	};

	/**
	 * Instantiates a new paciente.
	 *
	 * @param id the id
	 */
	public Paciente(long id) {
		this.id = id;
		valoresPorDefecto();
	}

	/**
	 * Instantiates a new paciente.
	 *
	 * @param id        the id
	 * @param nombre    the nombre
	 * @param apellido1 the apellido 1
	 * @param apellido2 the apellido 2
	 */
	public Paciente(Long id, String nombre, String apellido1, String apellido2) {
		this.id = id;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		valoresPorDefecto();

	}

	/**
	 * Valores por defecto.
	 */
	public void valoresPorDefecto() {
		this.sexo = 0;
		this.numerohc = "";
		this.dni = "";
		this.provincia = Provincia.PROVINCIA_DEFECTO;
		this.municipio = new Municipio();
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Gets the apellido 1.
	 *
	 * @return the apellido 1
	 */
	public String getApellido1() {
		return apellido1;
	}

	/**
	 * Sets the apellido 1.
	 *
	 * @param apellido1 the new apellido 1
	 */
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	/**
	 * Gets the apellido 2.
	 *
	 * @return the apellido 2
	 */
	public String getApellido2() {
		return apellido2;
	}

	/**
	 * Sets the apellido 2.
	 *
	 * @param apellido2 the new apellido 2
	 */
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	/**
	 * Gets the numerohc.
	 *
	 * @return the numerohc
	 */
	public String getNumerohc() {
		return numerohc;
	}

	/**
	 * Sets the numerohc.
	 *
	 * @param numerohc the new numerohc
	 */
	public void setNumerohc(String numerohc) {
		this.numerohc = numerohc;
	}

	/**
	 * Gets the dni.
	 *
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}

	/**
	 * Sets the dni.
	 *
	 * @param dni the new dni
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}

	/**
	 * Gets the direccion.
	 *
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * Sets the direccion.
	 *
	 * @param direccion the new direccion
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * Gets the provincia.
	 *
	 * @return the provincia
	 */
	public Provincia getProvincia() {
		return provincia;
	}

	/**
	 * Sets the provincia.
	 *
	 * @param provincia the new provincia
	 */
	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	/**
	 * Gets the municipio.
	 *
	 * @return the municipio
	 */
	public Municipio getMunicipio() {
		return municipio;
	}

	/**
	 * Sets the municipio.
	 *
	 * @param municipio the new municipio
	 */
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	/**
	 * Gets the codigopostal.
	 *
	 * @return the codigopostal
	 */
	public String getCodigopostal() {
		return codigopostal;
	}

	/**
	 * Sets the codigopostal.
	 *
	 * @param codigopostal the new codigopostal
	 */
	public void setCodigopostal(String codigopostal) {
		this.codigopostal = codigopostal;
	}

	/**
	 * Gets the fnac.
	 *
	 * @return the fnac
	 */
	public LocalDate getFnac() {
		return fnac;
	}

	/**
	 * Sets the fnac.
	 *
	 * @param fnac the new fnac
	 */
	public void setFnac(LocalDate fnac) {
		this.fnac = fnac;
	}

	/**
	 * Gets the sexo.
	 *
	 * @return the sexo
	 */
	public Integer getSexo() {
		return sexo;
	}

	/**
	 * Sets the sexo.
	 *
	 * @param sexo the new sexo
	 */
	public void setSexo(Integer sexo) {
		this.sexo = sexo;
	}

	/**
	 * Sets the sexo setring.
	 *
	 * @param sexo the new sexo setring
	 */
	public void setSexoSetring(String sexo) {
		this.sexo = Utilidades.getSexoValor(sexo);
	}

	/**
	 * Gets the sexo string.
	 *
	 * @return the sexo string
	 */
	public String getSexoString() {
		return Utilidades.getSexoNombre(this.sexo);
	}

	/**
	 * Gets the telefono.
	 *
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * Sets the telefono.
	 *
	 * @param telefono the new telefono
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * Gets the movil.
	 *
	 * @return the movil
	 */
	public String getMovil() {
		return movil;
	}

	/**
	 * Sets the movil.
	 *
	 * @param movil the new movil
	 */
	public void setMovil(String movil) {
		this.movil = movil;
	}

	/**
	 * Gets the numero orden.
	 *
	 * @return the numero orden
	 */
	public int getNumeroOrden() {
		return numeroOrden;
	}

	/**
	 * Sets the numero orden.
	 *
	 * @param numeroOrden the new numero orden
	 */
	public void setNumeroOrden(int numeroOrden) {
		this.numeroOrden = numeroOrden;
	}

	/**
	 * Gets the apellidos nombre.
	 *
	 * @return the apellidos nombre
	 */
	public String getApellidosNombre() {
		String nombre = "";
		if (this.getApellido1() != null) {
			nombre = this.getApellido1().trim();
		}
		if (this.getApellido2() != null) {
			nombre = nombre.concat(" ").concat(this.getApellido2().trim());
		}

		if (this.getNombre() != null) {
			nombre = nombre.concat(", ").concat(this.getNombre().trim());
		}
		return nombre;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	public String toString() {
		return getId() + " " + getNombre() + " " + getApellido1() + " " + getApellido2() + "\n";
	}

	public ArrayList<Registro> getListaRegistros() {
		return listaRegistros;
	}

	public void setListaRegistros(ArrayList<Registro> listaRegistros) {
		this.listaRegistros = listaRegistros;
	}

}
