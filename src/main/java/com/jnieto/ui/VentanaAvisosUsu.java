package com.jnieto.ui;

import java.time.LocalDate;
import java.util.ArrayList;

import com.jnieto.dao.MensajesDAO;
import com.jnieto.entity.Mensaje;
import com.jnieto.utilidades.Utilidades;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBoxGroup;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

public class VentanaAvisosUsu extends Window {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5928415763822373627L;

	private Window subWindow = null;

	private FormLayout formulario = null;

	private UI ui = null;

	private CheckBoxGroup<Mensaje> avisosBoxGroup;

	public VentanaAvisosUsu(UI ui, ArrayList<Mensaje> mensajes) {
		this.ui = ui;
		subWindow = new Window();
		subWindow.setWidth("800");
		subWindow.setHeight("660");
		subWindow.center();
		subWindow.setModal(true);
		subWindow.isResizeLazy();
		subWindow.setCaption("Avisos del usuario ");

		formulario = new FormLayout();
		formulario.setMargin(true);
		formulario.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		formulario.setSizeFull();

		subWindow.setContent(formulario);
		avisosBoxGroup = new CheckBoxGroup<>();
		avisosBoxGroup.setItems(mensajes);
		avisosBoxGroup.setCaption(" Mensajes pendientes de revisar ");
		avisosBoxGroup.setItemDescriptionGenerator(Mensaje::getPacienteNombre);
		avisosBoxGroup.setItemCaptionGenerator(Mensaje::getPacienteNombre);
		avisosBoxGroup.addContextClickListener(a -> cc());
		Button aceptar = new ObjetosComunes().getBotonGrabar();
		aceptar.addClickListener(e -> aceptarClik());
		formulario.addComponents(avisosBoxGroup, aceptar);
		ui.addWindow(subWindow);
	}

	private void cc() {
		new NotificacionInfo(avisosBoxGroup.getValue().toString());
	}

	private void aceptarClik() {
		for (Mensaje mensaje : avisosBoxGroup.getSelectedItems()) {
			mensaje.setFecha_proceso(Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
			mensaje.setHora_proceso(Utilidades.getHoraNumeroAcual());
			mensaje.setEstado(Mensaje.MENSAJE__ENVIADO);
			new MensajesDAO().setMarcaEnviado(mensaje);
			subWindow.close();
		}
	}

}
