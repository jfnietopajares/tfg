package com.jnieto.ui;

import java.util.ArrayList;

import com.jnieto.dao.CentroDAO;
import com.jnieto.entity.Centro;
import com.jnieto.utilidades.Ayudas;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;

/**
 * The Class PantallaCentros. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class PantallaCentros extends PantallaMaster {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6150431393097876056L;

	/** The grid. */
	// grid
	Grid<Centro> grid = new Grid<>();

	/** The centro. */
	private Centro centro;

	/** The lista centros. */
	private ArrayList<Centro> listaCentros = null;

	/**
	 * Instantiates a new pantalla centros.
	 */
	public PantallaCentros() {
		super();

		grid.setCaption(" Lista de centros ");
		grid.addColumn(Centro::getId).setCaption("Id");
		grid.addColumn(Centro::getDescripcion).setCaption("Descripción");
		// grid.setItems(new CentroDao().getListaRegistros());
		grid.setWidthUndefined();
		grid.addItemClickListener(event -> selecciona());

		filtro.setPlaceholder(" nombre a buscar ");
		filtro.clear();

		paginacion = new CentroDAO().getPaginacionRegistros(filtro.getValue());
		setValoresGridBotones();
		contenedorGrid.addComponents(contenedorBotones, grid, filaPaginacion);
	}

	/**
	 * Buscar click.
	 */
	public void buscarClick() {
		paginacion = new CentroDAO().getPaginacionRegistros(filtro.getValue());
		setValoresGridBotones();
		if (listaCentros.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO + filtro.getValue());
			filtro.clear();
			filtro.focus();
		} else {
			grid.setItems(listaCentros);
			if (listaCentros.size() == 1) {
				doFormularioCentro(listaCentros.get(0));
			}
		}
	}

	/**
	 * Anadir click.
	 */
	public void anadirClick() {
		centro = new Centro();
		contenedorFormulario.setVisible(true);
		doFormularioCentro(centro);
	}

	/**
	 * Refrescar click.
	 */
	public void refrescarClick() {
		contenedorGrid.setEnabled(true);
		filtro.focus();
		filtro.clear();
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.setVisible(false);
		contenedorGrid.setEnabled(true);
		paginacion = new CentroDAO().getPaginacionRegistros(filtro.getValue());
		setValoresGridBotones();
	}

	/**
	 * Ayuda click.
	 */
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(PantallaCentros.AYUDA_PANTALLA_CENTROS));
	}

	/**
	 * Cerrar click.
	 */
	public void cerrarClick() {
		this.removeAllComponents();
	}

	/**
	 * Do formulario centro.
	 *
	 * @param centro the centro
	 */
	public void doFormularioCentro(Centro centro) {
		contenedorGrid.setEnabled(false);
		contenedorFormulario.setVisible(true);
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.addComponent(new FrmCentro(centro));
	}

	/**
	 * Selecciona.
	 */
	public void selecciona() {
		if (grid.getSelectedItems().size() == 1) {
			centro = new CentroDAO().getRegistroId(grid.getSelectedItems().iterator().next().getId());
			doFormularioCentro(centro);
		}
	}

	/**
	 * Sets the valores grid botones.
	 */
	public void setValoresGridBotones() {
		listaCentros = new CentroDAO().getListaRegistrosPaginados(filtro.getValue().toUpperCase(), paginacion);

		setValoresPgHeredados(listaCentros);
		if (listaCentros.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO + filtro.getValue());
		} else {
			grid.setItems(listaCentros);
			if (listaCentros.size() == 1) {
				centro = listaCentros.get(0);
				doFormularioCentro(listaCentros.get(0));
			}
		}
	}

	/**
	 * Sets the valores pg heredados.
	 *
	 * @param listaCentros the new valores pg heredados
	 */
	public void setValoresPgHeredados(ArrayList<Centro> listaCentros) {
		ArrayList<Integer> listaValoresArrayList = new ArrayList<>();
		for (Centro centro : listaCentros) {
			listaValoresArrayList.add(centro.getNumeroOrden());
		}
		setBotonesPaginacion(listaValoresArrayList);
	}

	/**
	 * Do control botones.
	 */
	@Override
	public void doControlBotones() {
		// TODO Auto-generated method stub

	}

	/** The Constant AYUDA_PANTALLA_CENTROS. */
	public static final String AYUDA_PANTALLA_CENTROS = " <b>Pantalla de revisión de centros  :\n  </b> <br>" + " <hr>"
			+ " Haciendo click en una fila de la tabla de centros se puede modificar y/o borrar  <br>"
			+ " El campo de buscar realiza búsquedas por código, id, nemónico o por descripción, <br> "
			+ " con cualquier tipo de coincidencia. <br>" + " <hr>" + Ayudas.AYUDA_BOTONES_BARCA;

}
