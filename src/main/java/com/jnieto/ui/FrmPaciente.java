package com.jnieto.ui;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vaadin.dialogs.ConfirmDialog;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.controlador.AccesoControlador;
import com.jnieto.dao.MunicipioDAO;
import com.jnieto.dao.PacienteDAO;
import com.jnieto.dao.ProvinciasDAO;
import com.jnieto.entity.Municipio;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Provincia;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.LlamadaExterna;
import com.jnieto.utilidades.Parametros;
import com.jnieto.utilidades.Utilidades;
import com.jnieto.validators.ValidatoDni;
import com.jnieto.validators.ValidatorNombre;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.server.UserError;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.LocalDateRenderer;

/**
 * The Class FrmPaciente. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class FrmPaciente extends PantallaPaginacion {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6812524658132734446L;

	private static final Logger logger = LogManager.getLogger(FrmPaciente.class);

	private TextField id = new TextField("Id");

	private TextField nombre = null;

	private TextField apellido1 = null;

	private TextField apellido2 = null;

	private TextField dni = null;

	private String dniAnteriorString = null;

	private TextField numerohc = null;

	private String numerohcAnteriorString = null;

	private DateField fnac = null;

	private RadioButtonGroup<String> sexo = null;

	private TextField telefono = null;

	private TextField movil = null;

	private TextField domicilio = null;

	private TextField codigopostal = null;

	private Provincia provincia = Provincia.PROVINCIA_DEFECTO;

	private ComboBox<Provincia> selectProvincia = creaComoboProvincias(provincia);

	private ComboBox<Municipio> selectMunicipio = creaComboMunicipio(provincia, null);

	private Button grabar, consultar, limpiar, borrar, ayuda, mama, colon, paliativos, oxigenoterapia, jimena;

	private Label labelAyuda = new Label();

	private HorizontalLayout fila1 = new HorizontalLayout();

	private HorizontalLayout fila2 = new HorizontalLayout();

	public HorizontalLayout fila4 = new HorizontalLayout();

	private VerticalLayout contenedorGridPaginacion = new VerticalLayout();

	private VerticalLayout contenedorCamposBotones = new VerticalLayout();

	private VerticalLayout contenedorTabSheet = new VerticalLayout();

	private TabSheet tabsheet = new TabSheet();

	private String apellidos = "";

	private Paciente paciente = new Paciente();

	private ArrayList<Paciente> listaPacientes = new ArrayList<>();

	private Grid<Paciente> grid = new Grid<>();

	private Binder<Paciente> binder = new Binder<>();

	DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");

	/**
	 * Instantiates a new frm paciente.
	 *
	 * @param paciente the paciente
	 */
	public FrmPaciente(Paciente paciente) {
		this.addComponent(tabsheet);
		tabsheet.setStyleName(MaterialTheme.TABSHEET_ICONS_ON_TOP);

		this.paciente = paciente;
		dniAnteriorString = paciente.getDni();
		numerohcAnteriorString = paciente.getNumerohc();

		this.setWidth("100%");
		this.setMargin(false);
		contenedorCamposBotones.setMargin(false);
		contenedorCamposBotones.setWidth("1100px");
		contenedorCamposBotones.addStyleName(MaterialTheme.CARD_HOVERABLE);

		fila1.setMargin(false);
		fila2.setMargin(false);
		fila4.setMargin(false);

		labelAyuda.setValue(getTextoDeAyuda());

		id.setPlaceholder("");
		id.setEnabled(false);
		id.setMaxLength(10);
		id.setWidth("80px");

		numerohc = new ObjetosComunes().getNumerohc("NHC", " nº historia ");

		binder.forField(numerohc).withValidator(numero -> numero.matches("[0-9]*"), " sólo números")
				.withValidator(
						numero -> (Integer.parseInt(numero) > 0 && Integer.parseInt(numero) < Integer
								.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_MAXIMO_NUMERO_HISTORIA))),
						" de 1 a  " + MyUI.objParametros.getProperty(Parametros.KEY_MAXIMO_NUMERO_HISTORIA))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(Paciente::getNumerohc, Paciente::setNumerohc);

		numerohc.addBlurListener(e -> saltaNumerohc());

		if (paciente.getId().equals(new Long(0))) {
			numerohc.focus();
		}

		nombre = new ObjetosComunes().getApeNombre("Nombre", " nombre paciente");
		if (!paciente.getId().equals(new Long(0))) {
			nombre.focus();
		}
		binder.forField(nombre).withValidator(new ValidatorNombre("Nombre  incorrecto "))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Paciente::getNombre, Paciente::setNombre);

		if (paciente.getId().equals(new Long(0))) {
			numerohc.focus();
		} else {
			nombre.focus();
		}

		apellido1 = new ObjetosComunes().getApeNombre("Apellido 1º", " primer apellido ");
		binder.forField(apellido1).withValidator(new ValidatorNombre("Apellido incorrecto "))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(Paciente::getApellido1, Paciente::setApellido1);

		apellido2 = new ObjetosComunes().getApeNombre("Apellido 2º", " segundo apellido ");
		binder.forField(apellido2).withValidator(new ValidatorNombre("Apellido incorrecto "))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(Paciente::getApellido2, Paciente::setApellido2);

		dni = new ObjetosComunes().getDni("DNI", " dni ");

		binder.forField(dni).withValidator(new ValidatoDni("DNI incorrecto ")).bind(Paciente::getDni, Paciente::setDni);
		dni.addBlurListener(e -> saltaDni());

		sexo = new ObjetosComunes().getSexo();
		binder.forField(sexo).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Paciente::getSexoString,
				Paciente::setSexoSetring);

		fila1.addComponents(numerohc, nombre, apellido1, apellido2, dni, sexo);

		fnac = new ObjetosComunes().getFecha("F.Nacimiento", "  nacimiento");
		binder.forField(fnac)
				.withValidator(returnDate -> !returnDate.isAfter(LocalDate.now()), " Fecha debe ser menor que hoy")
				.withValidator(returnDate -> !returnDate.isBefore(LocalDate.of(1910, 1, 1)), " Fecha mayo de 1910")
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Paciente::getFnac, Paciente::setFnac);
		//
		//
		telefono = new ObjetosComunes().getTelefono("Teléfono", " teléfono");
		telefono.addBlurListener(e -> saltaTf(telefono.getValue()));
		binder.forField(telefono).withValidator(returnTf -> {
			if (returnTf == null)
				return true;
			else
				return !returnTf.equals(movil.getValue());
		}, " Los telefónos  no pueden ser iguales.").withValidator(numero -> numero.matches("[0-9]{9}"), " 9 dígitos  ")
				.bind(Paciente::getTelefono, Paciente::setTelefono);

		movil = new ObjetosComunes().getMovil("Móvil", " tf móvil");
		movil.addBlurListener(e -> saltaTf(telefono.getValue()));

		binder.forField(movil).withValidator(returnMovile -> {
			if (returnMovile == null)
				return true;
			else
				return !returnMovile.equals(telefono.getValue());
		}, " Los teléfonos no pueden ser iguales.").withValidator(numero -> numero.matches("[0-9]{9}"), " 9 dígitos  ")
				.bind(Paciente::getMovil, Paciente::setMovil);

		domicilio = new ObjetosComunes().getDomicilio("Domicilio", " domicilio");
		binder.forField(domicilio).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Paciente::getDireccion,
				Paciente::setDireccion);

		selectProvincia = creaComoboProvincias(Provincia.PROVINCIA_DEFECTO);
		binder.forField(selectProvincia).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(Paciente::getProvincia, Paciente::setProvincia);

		selectMunicipio = creaComboMunicipio(selectProvincia.getValue(), null);
		binder.forField(selectMunicipio).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(Paciente::getMunicipio, Paciente::setMunicipio);

		codigopostal = new ObjetosComunes().getCodigoPostal("C.Postal", " cod postal");
		binder.forField(codigopostal).withValidator(numero -> numero.matches("[0-9]{5}"), " 5 dígitos  ")
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(Paciente::getCodigopostal, Paciente::setCodigopostal);

		fila2.addComponents(fnac, telefono, movil, domicilio, selectProvincia, selectMunicipio, codigopostal);

		consultar = new ObjetosComunes().getBotonBuscar();
		consultar.addClickListener(event -> consultarClick());

		grabar = new ObjetosComunes().getBotonGrabar();
		grabar.addClickListener(event -> grabarClick());

		limpiar = new ObjetosComunes().getBotonLimpiar();
		limpiar.addClickListener(event -> limpiarClick());

		borrar = new ObjetosComunes().getBotonBorrar();
		borrar.addClickListener(event -> borrarClick());
		borrar.setEnabled(false);

		ayuda = new ObjetosComunes().getBotonAyuda();
		ayuda.addClickListener(Event -> ayudaClick());

		mama = new ObjetosComunes().getBotonProcesoMama();
		mama.addClickListener(Event -> procesoMamaClick());
		mama.setEnabled(false);

		colon = new ObjetosComunes().getBotonProcesoColon();
		colon.addClickListener(Event -> ProcesoColonClick());
		colon.setEnabled(false);

		paliativos = new ObjetosComunes().getBotonProcesoPaliativos();
		paliativos.addClickListener(Event -> ProcesoPaliativosClick());
		paliativos.setEnabled(false);

		oxigenoterapia = new ObjetosComunes().getBotonProcesoOxigeno();
		oxigenoterapia.addClickListener(Event -> procesoOxigenoClick());
		oxigenoterapia.setEnabled(false);

		jimena = new ObjetosComunes().getBotonJimena();
		jimena.addClickListener(Event -> jimenaClick());
		jimena.setEnabled(false);

		paginacion.setNumeroRegistrosPagina(
				Integer.parseInt((String) MyUI.objParametros.get(Parametros.KEY_NUMERO_REGISTROS_PAGINACION)));
		grid.addColumn(Paciente::getNumerohc).setCaption("Nhc");
		grid.addColumn(Paciente::getFnac, new LocalDateRenderer("dd/MM/yyyy")).setCaption("F.Nac");
		grid.addColumn(Paciente::getNombre).setCaption("Nombre");
		grid.addColumn(Paciente::getApellido1).setCaption("Apellido1");
		grid.addColumn(Paciente::getApellido2).setCaption("Apellido2");
		grid.addColumn(Paciente::getDni).setCaption("DNI");
		grid.addItemClickListener(event -> selecciona());
		grid.asSingleSelect();
		grid.setWidth("1100px");
		grid.setHeight(Utilidades.getAltoGrid(paginacion.getNumeroRegistrosPagina()));

		fila4.addComponents(consultar, grabar, limpiar, borrar, mama, colon, paliativos, oxigenoterapia, jimena, ayuda);
		contenedorGridPaginacion.addComponents(grid, filaPaginacion);
		contenedorGridPaginacion.setMargin(false);
		contenedorGridPaginacion.setVisible(false);

		contenedorCamposBotones.addComponents(fila1, fila2, fila4);
		contenedorTabSheet.addComponents(contenedorCamposBotones, contenedorGridPaginacion);
		tabsheet.addTab(contenedorTabSheet, "Pacientes");

		binder.readBean(paciente);
		activaBotones(paciente);
	}

	/**
	 * Crea comobo provincias.
	 *
	 * @param provincia the provincia
	 * @return the combo box
	 */
	private ComboBox<Provincia> creaComoboProvincias(Provincia provincia) {
		ComboBox<Provincia> select = new ComboBox<Provincia>("Provincia");
		select.setWidth("150px");
		select.setItems(new ProvinciasDAO().getListaProvincias());
		select.setItemCaptionGenerator(Provincia::getDescripcion);
		select.setSelectedItem(provincia);
		select.setEmptySelectionAllowed(false);
		select.addSelectionListener(event -> elige());
		select.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return select;
	}

	/**
	 * Elige.
	 */
	private void elige() {
		ArrayList<Municipio> listaMunicipios = new MunicipioDAO().getListaMunicipios(selectProvincia.getValue());
		selectMunicipio.setItems(listaMunicipios);
	}

	/**
	 * Crea combo municipio.
	 *
	 * @param provicia  the provicia
	 * @param municipio the municipio
	 * @return the combo box
	 */
	private ComboBox<Municipio> creaComboMunicipio(Provincia provincia, Municipio municipio) {
		ComboBox<Municipio> selectm = new ComboBox<>("Municipio");
		ArrayList<Municipio> listaMunicipios = new MunicipioDAO().getListaMunicipios(provincia);
		selectm.setWidth("200px");
		selectm.setItemCaptionGenerator(Municipio::getDescripcion);
		selectm.setItems(listaMunicipios);
		selectm.setEmptySelectionAllowed(false);
		selectm.setPlaceholder("Elige municipio");
		if (municipio != null) {
			selectm.setSelectedItem(municipio);
		}
		selectm.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return selectm;
	}

	private void jimenaClick() {
		String urlString = new LlamadaExterna(numerohc.getValue(), "jimena").getUrl();
		this.getUI().getPage().open(urlString, "_blank");
	}

	/**
	 * Consultar click.
	 */
	private void consultarClick() {
		String ape1 = "";
		String ape2 = "";
		String nomb = "";

		if (!apellido1.getValue().isEmpty())
			ape1 = apellido1.getValue();

		if (!apellido2.getValue().isEmpty())
			ape2 = apellido2.getValue();

		if (!nombre.getValue().isEmpty())
			nomb = nombre.getValue();
		if (!numerohc.getValue().isEmpty()) {
			paginacion = new PacienteDAO().getPaginacionPaciente(numerohc.getValue(), new Long(0));
		} else if (!dni.getValue().isEmpty()) {
			paginacion = new PacienteDAO().getPaginacionPaciente(dni.getValue(), new Long(0));
		} else {

			paginacion = new PacienteDAO().getPaginacionPaciente(ape1, ape2, nomb, new Long(0));
		}
		paginacion.setNumeroRegistrosPagina(
				Integer.parseInt((String) MyUI.objParametros.get(Parametros.KEY_NUMERO_REGISTROS_PAGINACION)));
		if (paginacion.getRegistrosTotales() > 0) {
			setValoresGridBotones();

		} else {
			new NotificacionInfo(NotificacionInfo.PACIENTE_NO_ENCONTRADO + " Apellidos: " + apellidos);
		}
	}

	/**
	 * Grabar click.
	 */
	private void grabarClick() {
		try {
			binder.writeBean(paciente);
			if (doValidaFormulario() == true) {
				if (new PacienteDAO().grabaDatos(paciente)) {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
					limpiarClick();
				} else {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
				}
			}
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION + e.getMessage());
			logger.error("Validacion ", e);
		}
	}

	/**
	 * Limpiar click.
	 */
	private void limpiarClick() {
		apellidos = "";
		paciente = new Paciente();
		numerohcAnteriorString = paciente.getNumerohc();
		dniAnteriorString = paciente.getDni();
		selectProvincia.setValue(Provincia.PROVINCIA_DEFECTO);
		selectMunicipio.setValue(null);
		binder.readBean(paciente);
	}

	/**
	 * Borrar click.
	 */
	private void borrarClick() {
		ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, Constantes.CONFIRMACION_BORRADO_MENSAJE,
				Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 6169352858399108337L;

					public void onClose(ConfirmDialog dialog) {
						if (dialog.isConfirmed()) {
							borraElRegistro();

						}
					}
				});
	}

	/**
	 * Borra el registro.
	 */
	public void borraElRegistro() {
		boolean referencias = new PacienteDAO().getReferenciasExternas(paciente.getId());
		if (referencias == true) {
			if (new PacienteDAO().borraDatos(paciente)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_BORRADO);
				this.setValoresCampos(null);
				limpiarClick();
			}
		} else
			new NotificacionInfo(NotificacionInfo.FORMULARIO_ERROR_DATO_BORRADO);
	}

	/**
	 * Click ayuda.
	 */
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), labelAyuda);
	}

	/**
	 * Click procesos oxigeno.
	 */
	public void procesoOxigenoClick() {
		VerticalLayout contenedorTabLayout = new VerticalLayout();
		contenedorTabLayout.addComponent(new PantallaProcesos(paciente, Proceso.SUBAMBITO_OXIGENO, null));
		tabsheet.addTab(contenedorTabLayout, paciente.getApellidosNombre()).setClosable(true);
		tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));
	}

	/**
	 * Click proceso mama.
	 */
	public void procesoMamaClick() {
		if (paciente.getSexo() == 1) {
			doValidaSexo(paciente);
		} else {
			VerticalLayout contenedorTabLayout = new VerticalLayout();
			contenedorTabLayout.addComponent(new PantallaProcesos(paciente, Proceso.SUBAMBITO_MAMA, null));
			tabsheet.addTab(contenedorTabLayout, paciente.getApellidosNombre()).setClosable(true);
			tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));
		}
	}

	public void doValidaSexo(Paciente paciente) {
		ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, " Asignar proceso mama a un hombre ?",
				Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 6169352858399108337L;

					public void onClose(ConfirmDialog dialog) {
						if (dialog.isConfirmed()) {
							VerticalLayout contenedorTabLayout = new VerticalLayout();
							contenedorTabLayout
									.addComponent(new PantallaProcesos(paciente, Proceso.SUBAMBITO_MAMA, null));
							tabsheet.addTab(contenedorTabLayout, paciente.getApellidosNombre()).setClosable(true);
							tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));
						}
					}
				});
	}

	/**
	 * Click proceso colon.
	 */
	public void ProcesoColonClick() {
		VerticalLayout contenedorTabLayout = new VerticalLayout();
		contenedorTabLayout.addComponent(new PantallaProcesos(paciente, Proceso.SUBAMBITO_COLON, null));
		tabsheet.addTab(contenedorTabLayout, paciente.getApellidosNombre()).setClosable(true);
		tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));
	}

	/**
	 * Click proceso paliativos.
	 */
	public void ProcesoPaliativosClick() {
		VerticalLayout contenedorTabLayout = new VerticalLayout();
		contenedorTabLayout.addComponent(new PantallaProcesos(paciente, Proceso.SUBAMBITO_PALIATIVOS, null));
		tabsheet.addTab(contenedorTabLayout, paciente.getApellidosNombre()).setClosable(true);
		tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));
	}

	/**
	 * Salta numerohc.
	 */
	public void saltaNumerohc() {
		if (!numerohc.getValue().isEmpty()) {
			if (numerohcAnteriorString.isEmpty()) {
				ArrayList<Paciente> listaPacientes = new PacienteDAO().getListaPacientes(numerohc.getValue());
				if (listaPacientes.size() == 1 && paciente.getId().equals(new Long(0))) {
					paciente = listaPacientes.get(0);
					setValoresCampos(paciente);
					new AccesoControlador().doAccesoUsuarioPaciente(paciente);
					activaBotones(paciente);
				} else if (listaPacientes.size() > 1) {

					setValoresGridBotones();
				} else

				{
					// new NotificacionInfo(NotificacionInfo.PACIENTE_REPETIDO_NHC);
				}
			} else {
				if (!numerohc.getValue().equals(numerohcAnteriorString)) {
					ArrayList<Paciente> listaPacientes = new PacienteDAO().getListaPacientes(numerohc.getValue());
					if (listaPacientes.size() == 0) {
						// correcto el dni no existe
					} else {
						new NotificacionInfo(NotificacionInfo.PACIENTE_REPETIDO_NHC);
						numerohc.setValue(numerohcAnteriorString);
					}
				}
			}
		}
	}

	/**
	 * Salta dni.
	 */
	public void saltaDni() {
		if (!dni.getValue().isEmpty()) {
			if (dniAnteriorString.isEmpty()) {
				ArrayList<Paciente> listaPacientes = new PacienteDAO().getListaPacientes(dni.getValue());
				if (listaPacientes.size() > 0 && paciente.getId().equals(new Long(0))) {
					paciente = listaPacientes.get(0);
					setValoresCampos(paciente);
					new AccesoControlador().doAccesoUsuarioPaciente(paciente);
					activaBotones(paciente);
				} else if (listaPacientes.size() > 0) {
					new NotificacionInfo(NotificacionInfo.PACIENTE_REPETIDO_DNI + ":" + dni.getValue());
				}
			} else {
				if (!dni.getValue().equals(dniAnteriorString)) {
					ArrayList<Paciente> listaPacientes = new PacienteDAO().getListaPacientes(dni.getValue());
					if (listaPacientes.size() == 0) {
						// correcto el dni no existe
					} else {
						new NotificacionInfo(NotificacionInfo.PACIENTE_REPETIDO_DNI + "-" + dni.getValue());
						dni.setValue(dniAnteriorString);
					}
				}
			}
		}

	}

	public void saltaTf(String telefono) {
		if (!telefono.isEmpty()) {
			Paciente paci = new PacienteDAO().getPacientePorTf(telefono, paciente.getId());
			if (paci != null) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_TF_EXISTENTE + paci.getApellidosNombre());
			}
		}
	}

	/**
	 * Selecciona.
	 */
	public void selecciona() {
		if (grid.getSelectedItems().size() == 1) {
			paciente = grid.getSelectedItems().iterator().next();
			setValoresCampos(paciente);
			contenedorGridPaginacion.setVisible(false);
			new AccesoControlador().doAccesoUsuarioPaciente(paciente);
			contenedorCamposBotones.setEnabled(true);
			activaBotones(paciente);
		}
	}

	/**
	 * Valida datos formulario.
	 *
	 * @return true, if successful
	 */
	public boolean doValidaFormulario() {
		boolean datosValidos = true;
		if (selectMunicipio.getValue() == null) {
			datosValidos = false;
			selectMunicipio.setComponentError(new UserError(NotificacionInfo.FORMULARIOCAMPOREQUERIDO));
		} else {
			selectMunicipio.setComponentError(null);
		}

		if (selectMunicipio.getSelectedItem().isPresent()) {
			if (selectMunicipio.getValue().getId() == null) {
				datosValidos = false;
				selectMunicipio.setComponentError(new UserError(NotificacionInfo.FORMULARIOCAMPOREQUERIDO));
			} else {
				selectMunicipio.setComponentError(null);
			}
		}

		return datosValidos;
	}

	/**
	 * Sets the valores campos.
	 *
	 * @param paciente the new valores campos
	 */
	public void setValoresCampos(Paciente paciente) {
		binder.readBean(paciente);
		numerohcAnteriorString = paciente.getNumerohc();
		dniAnteriorString = paciente.getDni();
	}

	/**
	 * Limpia errores.
	 */
	public void limpiaErrores() {

		nombre.setComponentError(null);

		apellido1.setComponentError(null);

		apellido2.setComponentError(null);

		dni.setComponentError(null);

		numerohc.setComponentError(null);

		fnac.setComponentError(null);

		telefono.setComponentError(null);

		movil.setComponentError(null);

		domicilio.setComponentError(null);

		codigopostal.setComponentError(null);

		selectProvincia.setComponentError(null);

		selectMunicipio.setComponentError(null);

	}

	/**
	 * Asigna valores grid botones.
	 */
	@Override
	public void setValoresGridBotones() {
		String ape1 = "";
		String ape2 = "";
		String nomb = "";

		if (!apellido1.getValue().isEmpty())
			ape1 = apellido1.getValue();

		if (!apellido2.getValue().isEmpty())
			ape2 = apellido2.getValue();

		if (!nombre.getValue().isEmpty())
			nomb = nombre.getValue();

		if (!numerohc.getValue().isEmpty()) {
			listaPacientes = new PacienteDAO().getListaPacientes(numerohc.getValue(), paginacion, new Long(0));
		} else if (!dni.getValue().isEmpty()) {
			listaPacientes = new PacienteDAO().getListaPacientes(dni.getValue(), paginacion, new Long(0));
		} else {
			listaPacientes = new PacienteDAO().getListaPacientes(ape1, ape2, nomb, paginacion, new Long(0));
		}

		setValoresPgHeredados(listaPacientes);

		if (listaPacientes.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO + apellidos);
		} else {

			if (listaPacientes.size() == 1) {
				paciente = listaPacientes.get(0);
				setValoresCampos(paciente);
				contenedorCamposBotones.setEnabled(false);
				activaBotones(paciente);
				contenedorGridPaginacion.setVisible(false);
				contenedorCamposBotones.setEnabled(true);
			} else {
				grid.setItems(listaPacientes);
				contenedorGridPaginacion.setVisible(true);
				contenedorCamposBotones.setEnabled(false);
			}
		}
	}

	/**
	 * Activa botones.
	 *
	 * @param paciente the paciente
	 */
	public void activaBotones(Paciente paciente) {
		if (paciente != null) {
			if (paciente.getId().equals(new Long(0))) {
				borrar.setEnabled(false);
				mama.setEnabled(false);
				colon.setEnabled(false);
				oxigenoterapia.setEnabled(false);
				paliativos.setEnabled(false);
				jimena.setEnabled(false);
			} else {
				borrar.setEnabled(new PacienteDAO().getReferenciasExternas(paciente.getId()));
				mama.setEnabled(new UtilidadesUi().getVisibilidadBoton(mama));
				colon.setEnabled(new UtilidadesUi().getVisibilidadBoton(colon));
				paliativos.setEnabled(new UtilidadesUi().getVisibilidadBoton(paliativos));
				oxigenoterapia.setEnabled(new UtilidadesUi().getVisibilidadBoton(oxigenoterapia));
				jimena.setEnabled(true);
			}
		}
	}

	/**
	 * Sets the valores pg heredados.
	 *
	 * @param listaPacientes the new valores pg heredados
	 */
	public void setValoresPgHeredados(ArrayList<Paciente> listaPacientes) {
		ArrayList<Integer> listaValoresArrayList = new ArrayList<>();

		for (Paciente paciente : listaPacientes) {
			listaValoresArrayList.add(paciente.getNumeroOrden());
		}
		setBotonesPaginacion(listaValoresArrayList);
	}

	/**
	 * Gets the texto de ayuda.
	 *
	 * @return the texto de ayuda
	 */
	public String getTextoDeAyuda() {
		return " <b>Pantalla de mantenimiento de datos de pacientes:\n  </b> <br>" + "Descrición de botones: \n  <br>"
				+ "<ul> <li><b>Buscar</b>  Realiza consulta de datos por los campos marcados con  * en el siguiente orden:"
				+ "&nbsp;&nbsp;&nbsp; 1.- Número de historia,&nbsp; 2.-DNI, &nbsp; 3.-Apellidos primero y/o segundo o ambos"
				+ " </li>"
				+ "<li><b>Grabar</b>  Añade un nuevo paciente a la base de datos si el id está vacío o modifica los datos de paciente acutal. Los datos son validados antes de ser almacenados.</li>"
				+ "<li><b>Limpiar</b>  Borra los datos de los campos de la pantalla sin hacer alteraciones en la base de datos</li>"
				+ "<li><b>Borrar</b>  Borra la ficha actual de la base de datos.</li>"
				+ "<li><b>Ayuda</b>  Esta pantalla</li>" + "</ul>  <hr>" + "<lu>Campos validados"
				+ "<li><b>Nombre</b>  No puede estar en blanco</li>"
				+ "<li><b>Apellido1</b>  No puede estar en blanco</li>"
				+ "<li><b>Apellido2</b>  No puede estar en blanco</li>"
				+ "<li><b>DNI</b>  Debe un DNI correcto y no repetido que no exista en la base de datos</li>"
				+ "<li><b>Número de histora </b>Es un campo numérico obligatorio menor que "
				+ MyUI.objParametros.getProperty(Parametros.KEY_MAXIMO_NUMERO_HISTORIA)
				+ " y que no existe en la base de datos</li>"
				+ "<li><b>Fecha nacimiento</b>  Fecha menor o igual a hoy</li>"
				+ "<li><b>Teléfono </b>  Número de teléfono fijo válido no igual al móvil</li>"
				+ "<li><b>Móvil </b>  Número de teléfono móvil válido no igual al fijo</li>" + "</ul>  <hr>";
	}
}
