package com.jnieto.ui;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

import org.vaadin.dialogs.ConfirmDialog;

import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.GruposCatalogoDAO;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroOxi;
import com.jnieto.entity.Servicio;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Parametros;
import com.vaadin.server.UserError;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

/**
 * The Class FrmRegistroOxi. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public abstract class FrmRegistroOxi extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4726179888391826210L;

	protected HorizontalLayout filaCentrosServicio = new HorizontalLayout();

	// protected HorizontalLayout filaTerapiaModalidad = new HorizontalLayout();

	protected HorizontalLayout filaPruebaResultados = new HorizontalLayout();

	protected HorizontalLayout filaAerosolterapia = new HorizontalLayout();

	protected HorizontalLayout filaInicioTipoDuraciçon = new HorizontalLayout();

	protected HorizontalLayout filaObservaciones = new HorizontalLayout();

	protected HorizontalLayout fila3Interfase = new HorizontalLayout();

	protected HorizontalLayout filapulsio = new HorizontalLayout();

	protected HorizontalLayout filatos = new HorizontalLayout();

	protected HorizontalLayout filaBaja = new HorizontalLayout();

	protected ComboBox<Servicio> servicioSelect = new ComboBox<>();

	protected ComboBox<Centro> centroSelect = new ComboBox<>();
	protected ComboBox<String> modalidadesSelect;

	protected ComboBox<String> terapiasSelect;

	protected ComboBox<String> tipoPrescripcionSelest = new ComboBox<>("Tipo Prescipción");

	protected DateField fechaInicio = null;

	protected ComboBox<String> duracionSelect = new ComboBox<>("Duración del tratamiento");

	protected TextArea observaciones = new TextArea("Observaciones");

	protected DateField fechaBaja = null;

	protected TextField motivoBaja = new TextField("Motivo Baja");

	protected TextField interfase = new TextField("Interfase");

	protected TextField equipo = new TextField("Equipo");

	protected TextField farmaco = new TextField("Fármaco");

	protected TextField pruebaDiagnostica = new TextField("Prueba Diagnóstica");

	protected TextField resultadosPrueba = new TextField("Resultado Prueba");

	public FrmRegistroOxi() {
		this.setMargin(false);
		filaCentrosServicio.setMargin(false);
		filaPruebaResultados.setMargin(false);
		// filaTerapiaModalidad.setMargin(false);
		filaObservaciones.setMargin(false);
		filaPruebaResultados.setMargin(false);
		filaAerosolterapia.setMargin(false);
		filaInicioTipoDuraciçon.setMargin(false);
		fila3Interfase.setMargin(false);
		filapulsio.setMargin(false);
		filatos.setMargin(false);
		filaBaja.setMargin(false);

		/**
		 * Campos comunes a todos los registros tipo O2
		 */
		centroSelect = new ObjetosComunes().doCreaComboCentro(Centro.CENTRO_DEFECTO);
		servicioSelect = new ObjetosComunes().getComoboServicio(null,
				Proceso.getServicioDefecto(Proceso.SUBAMBITO_OXIGENO));

		pruebaDiagnostica.setWidth("300px");
		pruebaDiagnostica.setMaxLength(100);

		resultadosPrueba.setWidth("270px");
		pruebaDiagnostica.setMaxLength(100);

		tipoPrescripcionSelest = new ObjetosComunes().docreaComboTipoPrescripcion("");
		fechaInicio = new ObjetosComunes().getFecha("F.Inicio", "fecha inicio");
		duracionSelect = new ObjetosComunes().docreaComboDuracion("");
		observaciones.setWidth("375px");
		observaciones.setHeight("45px");
		observaciones.setMaxLength(400);
		fechaBaja = new ObjetosComunes().getFecha("F.Baja", "fecha fin");
		motivoBaja.setWidth("300px");
		motivoBaja.setMaxLength(100);

		filaPruebaResultados.addComponents(pruebaDiagnostica, resultadosPrueba);
		filaInicioTipoDuraciçon.addComponents(fechaInicio, tipoPrescripcionSelest, duracionSelect);

	}

	public void cambiaTipoPrescrip() {
		if (tipoPrescripcionSelest.getValue().equals(RegistroOxi.TIPO_PRES_DEFINITIVA)) {
			duracionSelect.setValue(RegistroOxi.TIPO_DURACION_DEFINITIVA);
			duracionSelect.setSelectedItem(RegistroOxi.TIPO_DURACION_DEFINITIVA);
			// duracionSelect.markAsDirty();
			// duracionSelect.attach();
			duracionSelect.getDataProvider().refreshAll();

		}
	}

	/**
	 * Crea combo terapias.
	 *
	 * @param terapia the terapia
	 * @return the combo box
	 */
	public ComboBox<String> creaComboTerapias(String terapia) {
		ComboBox<String> combo = new ComboBox<String>();
		combo.setCaption("Elige Terapia");
		combo.setWidth("205px");
		combo.setItems(new GruposCatalogoDAO().getValoresPorGrupo(RegistroOxi.GRUPOSC_O2_TERAPIA));
		if (terapia != null)
			combo.setSelectedItem(terapia);
		combo.setEmptySelectionAllowed(false);
		combo.addSelectionListener(event -> eligeModalidadTerapia(combo.getValue(), null));
		return combo;
	}

	/**
	 * Elige modalidad terapia.
	 *
	 * @param terapia  the terapia
	 * @param modaliad the modaliad
	 */
	public void eligeModalidadTerapia(String terapia, String modaliad) {
		this.modalidadesSelect = creaComboModalidades(terapia, modaliad);
	}

	/**
	 * Crea combo modalidades.
	 *
	 * @param terapia   the terapia
	 * @param modalidad the modalidad
	 * @return the combo box
	 */
	public ComboBox<String> creaComboModalidades(String terapia, String modalidad) {
		ComboBox<String> combo = new ComboBox<>();
		ArrayList<String> listaModalidades = new GruposCatalogoDAO().getMolalidadTerapia(terapia);
		combo.setCaption("Elige Modalidad");
		combo.setWidth("250px");
		combo.setItems(listaModalidades);
		combo.setEmptySelectionAllowed(false);
		if (modalidad != null)
			combo.setSelectedItem(modalidad);
		else if (listaModalidades.size() == 1)
			combo.setSelectedItem(listaModalidades.get(0));
		return combo;
	}

	@Override
	public void cerrarClick() {
		this.removeAllComponents();
		this.setVisible(false);
		((PantallaProcesos) this.getParent().getParent().getParent()).refrescarClick();
	}

	@Override
	public void ayudaClick() {
	}

	@Override
	public void grabarClick() {

	}

	/**
	 * Borrar click.
	 */
	@Override
	public void borrarClick() {
		ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, Constantes.CONFIRMACION_BORRADO_MENSAJE,
				Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 6169352858399108337L;

					public void onClose(ConfirmDialog dialog) {
						if (dialog.isConfirmed()) {
							borraElRegistro();
						}
					}
				});
		this.cerrarClick();
	}

	@Override
	public void borraElRegistro() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean doValidaFormulario() {
		boolean valido = true;
		/*
		 * if (!fechaBaja.isEmpty() && !fechaInicio.isEmpty()) { if
		 * (!fechaBaja.getValue().isAfter(fechaInicio.getValue())) { valido = false;
		 * fechaBaja.setComponentError( new
		 * UserError(NotificacionInfo.FORMULARIOFECHAMENOR + fechaInicio.getValue())); }
		 * } else { fechaBaja.setComponentError(null); }
		 */
		if (!motivoBaja.isEmpty() && fechaBaja.isEmpty()) {
			valido = false;
			fechaBaja.setComponentError(new UserError(Proceso.AVISO_PROCESO_MOTIVO_FECHA));
		} else {
			fechaBaja.setComponentError(null);
		}
		if (motivoBaja.isEmpty() && !fechaBaja.isEmpty()) {
			valido = false;
			motivoBaja.setComponentError(new UserError(Proceso.AVISO_PROCESO_MOTIVO_FECHA));
		} else {
			motivoBaja.setComponentError(null);
		}
		return valido;
	}

	@Override
	public void doActivaBotones(Registro registro) {
		if (registro.getProblema().getMotivo_baja() != null && registro.getProblema().getMotivo_baja() != "") {
			LocalDate date = LocalDate.now();
			long dias = ChronoUnit.DAYS.between(registro.getProblema().getFechafin(), date);

			if (dias > Integer.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
				new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
				contenedorCampos.setEnabled(false);
				borrar.setEnabled(false);
				grabar.setEnabled(false);
			}
		}
	}

	public String getAyudaHtml() {
		String ayudaHtml = " <b>Pantalla de mantenimiento de tratamietntos de oxigenoterapia :\n  </b> "
				+ terapiasSelect.getSelectedItem() + "<br>" + "<ul> "
				+ "<li><b>Centro:</b> Centro desde el que se realiza la prescripcion.</li>"
				+ "<li><b>Servicio:</b> Servicio que  realiza la prescripcion.</li>"
				+ "<li><b>Tipo:</b> Tipo de terapia.</li>" + "<li><b>Modalidad:</b> Modalidad de la terapia.</li>"
				+ "<li><b>Fecha de inicio:</b> Fecha de inicio del tratamiento.</li>"
				+ "<li><b>Tipo de prescripción:</b> Tipo de prescripción.</li>"
				+ "<li><b>Duraión:</b> Duración del tratamiento.</li>" + "<li><b>Prueba:</b> Prueba diagnóstica.</li>"
				+ "<li><b>Resultado:</b>Resultado de la prueba diagnóstica.</li>"
				+ "<li><b>Obsevaciones:</b>Un comentario si procede.</li>"
				+ "<li><b>Fecha Baja:</b>Fecha fin del tratamiento.</li>"
				+ "<li><b>Motivo de baja:</b>Motivo fin del tratamiento.</li>" + "</ul>";
		return ayudaHtml;
	}

}
