package com.jnieto.ui;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.entity.Paciente;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * The Class Pantalla_Master.
 *
 * @author JuanNieto *
 * @author Juan Nieto
 * @version 23.5.2018
 * 
 *          Clase asbtracta que gestiona los componentes comunes de las
 *          pantallas generales de gestión de datos.
 * 
 *          Basada en con componente GRID para buscar datos y selección de los
 *          mismos para edición o borrado.
 */
public abstract class PantallaMaster extends PantallaPaginacion {

	private static final long serialVersionUID = -6557096926480917251L;

	protected HorizontalLayout contenedorDatosPaciente = new HorizontalLayout();

	protected HorizontalLayout contenedorGridFrm = new HorizontalLayout();

	protected VerticalLayout contenedorGrid = new VerticalLayout();

	protected VerticalLayout contenedorFormulario = new VerticalLayout();

	protected HorizontalLayout contenedorBotones = new HorizontalLayout();

	protected Button buscar, anadir, refrescar, ayuda, cerrar;

	protected TextField filtro;

	public Paciente paciente = new Paciente();

	/**
	 * Name.
	 */
	protected void name() {

	}

	/**
	 * Instantiates a new pantalla master.
	 */
	public PantallaMaster() {
		doIniciaVentana();
	}

	/**
	 * Instantiates a new pantalla master.
	 *
	 * @param paciente the paciente
	 */
	public PantallaMaster(Paciente paciente) {
		this.paciente = paciente;
		doIniciaVentana();
		contenedorDatosPaciente.addComponent(new PanelPaciente(paciente));
		contenedorDatosPaciente.setVisible(true);
	}

	/**
	 * Do inicia ventana.
	 */
	public void doIniciaVentana() {
		this.setSizeFull();
		this.setMargin(false);

		contenedorDatosPaciente.setMargin(false);
		contenedorDatosPaciente.setVisible(false);
		// contenedorDatosPaciente.setWidth("600px");

		contenedorGridFrm.setMargin(false);
		contenedorBotones.setMargin(false);
		contenedorFormulario.setMargin(false);
		contenedorFormulario.setVisible(false);
		contenedorFormulario.setWidth("700px");

		contenedorGrid.setMargin(false);
		contenedorGrid.setWidth("550px");
		contenedorGrid.addStyleName(MaterialTheme.CARD_HOVERABLE);

		filtro = new ObjetosComunes().getFiltro();
		buscar = new ObjetosComunes().getBotonBuscar();
		buscar.addClickListener(e -> buscarClick());

		anadir = new ObjetosComunes().getBotonNuevo();
		anadir.addClickListener(e -> anadirClick());

		refrescar = new ObjetosComunes().getBotonRefrescar();
		refrescar.addClickListener(e -> refrescarClick());

		ayuda = new ObjetosComunes().getBotonAyuda();
		ayuda.addClickListener(e -> ayudaClick());

		cerrar = new ObjetosComunes().getBotonCerrar();
		cerrar.addClickListener(e -> cerrarClick());

		contenedorBotones.addComponents(filtro, buscar, anadir, refrescar, ayuda, cerrar);

		contenedorGridFrm.addComponents(contenedorGrid, contenedorFormulario);
		this.addComponents(contenedorDatosPaciente, contenedorGridFrm);
	}

	/**
	 * Buscar click.
	 */
	public abstract void buscarClick();

	/**
	 * Anadir click.
	 */
	public abstract void anadirClick();

	/**
	 * Refrescar click.
	 */
	public abstract void refrescarClick();

	/**
	 * Ayuda click.
	 */
	public abstract void ayudaClick();

	/**
	 * Cerrar click.
	 */
	public abstract void cerrarClick();

	/**
	 * Selecciona.
	 */
	public abstract void selecciona();

	/**
	 * Do control botones.
	 */
	public abstract void doControlBotones();

	/**
	 * Gets the contenedor grid.
	 *
	 * @return the contenedor grid
	 */
	public VerticalLayout getContenedorGrid() {
		return contenedorGrid;
	}

	/**
	 * Gets the contenedor formulario.
	 *
	 * @return the contenedor formulario
	 */
	public VerticalLayout getContenedorFormulario() {
		return contenedorFormulario;
	}

	/**
	 * Sets the contenedor formulario.
	 *
	 * @param contenedorFormulario the new contenedor formulario
	 */
	public void setContenedorFormulario(VerticalLayout contenedorFormulario) {
		this.contenedorFormulario = contenedorFormulario;
	}

}
