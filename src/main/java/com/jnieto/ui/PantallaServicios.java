package com.jnieto.ui;

import java.util.ArrayList;

import com.jnieto.dao.ServiciosDAO;
import com.jnieto.entity.Servicio;
import com.jnieto.utilidades.Ayudas;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;

/**
 * The Class PantallaServicios. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class PantallaServicios extends PantallaMaster {

	private static final long serialVersionUID = 6366480641067472647L;

	Grid<Servicio> grid = new Grid<>();

	private Servicio servicio;

	private ArrayList<Servicio> listaServicios = null;

	/**
	 * Instantiates a new pantalla servicios.
	 */
	public PantallaServicios() {
		super();
		grid.setCaption(" Listado  de servicios ");
		grid.addColumn(Servicio::getId).setCaption("Id");
		grid.addColumn(Servicio::getDescripcion).setCaption("Descripción");
		grid.addColumn(Servicio::getCodigo).setCaption("Cod");
		// grid.setItems(new ServiciosDAO().getListaRegistos());
		grid.setWidthUndefined();
		grid.addItemClickListener(event -> selecciona());

		filtro.setPlaceholder(" datos buscar ");
		paginacion = new ServiciosDAO().getPaginacionRegistros(filtro.getValue());
		setValoresGridBotones();
		contenedorGrid.addComponents(contenedorBotones, grid, filaPaginacion);
	}

	/**
	 * Buscar click.
	 */
	public void buscarClick() {
		paginacion = new ServiciosDAO().getPaginacionRegistros(filtro.getValue());
		setValoresGridBotones();
		if (listaServicios.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO + filtro.getValue());
			filtro.clear();
			filtro.focus();
		} else {
			grid.setItems(listaServicios);
			if (listaServicios.size() == 1) {
				servicio = new ServiciosDAO().getRegistroId(listaServicios.get(0).getId());
				doFormularioServicio(servicio);
			}
		}
	}

	/**
	 * Anadir click.
	 */
	public void anadirClick() {
		servicio = new Servicio();
		doFormularioServicio(servicio);
	}

	/**
	 * Refrescar click.
	 */
	public void refrescarClick() {
		filtro.clear();
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.setVisible(false);
		contenedorGrid.setEnabled(true);
		paginacion = new ServiciosDAO().getPaginacionRegistros(filtro.getValue());
		setValoresGridBotones();
	}

	/**
	 * Ayuda click.
	 */
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(PantallaServicios.AYUDA_PANTALLA_SERVICIOS));
	}

	/**
	 * Cerrar click.
	 */
	public void cerrarClick() {
		this.removeAllComponents();
	}

	/**
	 * Do formulario servicio.
	 *
	 * @param servicio the servicio
	 */
	public void doFormularioServicio(Servicio servicio) {
		contenedorGrid.setEnabled(false);
		contenedorFormulario.setVisible(true);
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.addComponent(new FrmServicio(servicio));
	}

	/**
	 * Selecciona.
	 */
	public void selecciona() {
		if (grid.getSelectedItems().size() == 1) {
			servicio = new ServiciosDAO().getRegistroId(grid.getSelectedItems().iterator().next().getId());
			doFormularioServicio(servicio);
		}
	}

	/**
	 * Sets the valores grid botones.
	 */
	public void setValoresGridBotones() {
		listaServicios = new ServiciosDAO().getListaRegistrosPaginados(filtro.getValue().toUpperCase(), paginacion);

		setValoresPgHeredados(listaServicios);
		if (listaServicios.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO + filtro.getValue());
		} else {
			grid.setItems(listaServicios);
			if (listaServicios.size() == 1) {
				servicio = listaServicios.get(0);
				doFormularioServicio(servicio);
			}
		}
	}

	/**
	 * Sets the valores pg heredados.
	 *
	 * @param listaServicios the new valores pg heredados
	 */
	public void setValoresPgHeredados(ArrayList<Servicio> listaServicios) {
		ArrayList<Integer> listaValoresArrayList = new ArrayList<>();
		for (Servicio servicio : listaServicios) {
			listaValoresArrayList.add(servicio.getNumeroOrden());
		}
		setBotonesPaginacion(listaValoresArrayList);
	}

	/**
	 * Do control botones.
	 */
	@Override
	public void doControlBotones() {
		// TODO Auto-generated method stub

	}

	/** The Constant AYUDA_PANTALLA_SERVICIOS. */
	public static final String AYUDA_PANTALLA_SERVICIOS = " <b>Pantalla de revisión de servicios  :\n  </b> <br>"
			+ " <hr>" + " Haciento click en una fila de la tabla de servicio se puede modificar y/o borrar"
			+ Ayudas.AYUDA_BOTONES_BARCA;

}
