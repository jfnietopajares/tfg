package com.jnieto.ui;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.PeticionesDao;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.RegistroOxiDomi;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Parametros;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

/**
 * The Class FrmRegistroOxiDomi. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class FrmRegistroOxiDomi extends FrmRegistroOxi {

	private static final Logger logger = LogManager.getLogger(FrmRegistroOxiDomi.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 323588328829317941L;

	private TextField flujo = new TextField("Flujo");
	private ComboBox<String> tipoFlujoSelect = new ComboBox<String>("Tipo flujo");
	private TextField horasDia = new TextField("Horas");
	private ComboBox<String> interfase = null;
//	protected ComboBox<String> terapiasSelect;

	private TextField presion = new TextField("Presión");
	private TextField saturacion = new TextField("Sat.O2%");

	private HorizontalLayout filaDomi = new HorizontalLayout();

	RegistroOxiDomi registroDomi = new RegistroOxiDomi();

	Binder<RegistroOxiDomi> binder = new Binder<RegistroOxiDomi>();

	public FrmRegistroOxiDomi(RegistroOxiDomi param) {
		super();
		this.registroDomi = param;
		filaDomi.setMargin(false);

		lbltitulo.setCaption(registroDomi.getVariableTerapia().getValor() + " " + registroDomi.getProblema().getId()
				+ "/" + registroDomi.getId());
		lbltitulo.setVisible(true);

		binder.forField(centroSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiDomi::getCentro, RegistroOxiDomi::setCentro);
		binder.forField(servicioSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiDomi::getServicio, RegistroOxiDomi::setServicio);
		modalidadesSelect = this.creaComboModalidades(registroDomi.getVariableTerapia().getValor(), "");
		binder.forField(modalidadesSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiDomi::getModalidadString, RegistroOxiDomi::setModalidad);
		binder.forField(pruebaDiagnostica).bind(RegistroOxiDomi::getPruebaDiagnosticaString,
				RegistroOxiDomi::setPruebaDiagnostica);
		binder.forField(resultadosPrueba).bind(RegistroOxiDomi::getResultadosPruebaString,
				RegistroOxiDomi::setResultadosPrueba);
		binder.forField(tipoPrescripcionSelest).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiDomi::getTipoPrescripcionString, RegistroOxiDomi::setTipoPrescripcion);
		tipoPrescripcionSelest.addBlurListener(e -> cambiaTipoPrescrip());
		binder.forField(fechaInicio).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiDomi::getFechaInicio, RegistroOxiDomi::setFechaInicio);
		binder.forField(duracionSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiDomi::getDuracionString, RegistroOxiDomi::setDuracion);
		observaciones.setWidth("600px");
		observaciones.setHeight("30px");
		binder.forField(observaciones).bind(RegistroOxiDomi::getObservacionesString, RegistroOxiDomi::setObservaciones);
		binder.forField(fechaBaja).withValidator(returnDate -> {
			if (returnDate == null)
				return true;
			else
				return !returnDate.isBefore(fechaInicio.getValue());
		}, "Mayor o igual que fecha inicio ").bind(RegistroOxiDomi::getFechaBaja, RegistroOxiDomi::setFechaBaja);
		binder.forField(motivoBaja).bind(RegistroOxiDomi::getMotivoBajaString, RegistroOxiDomi::setMotivoBaja);

		tipoFlujoSelect = new ObjetosComunes().docreaComboTipoFlujo("");
		binder.forField(tipoFlujoSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiDomi::getTipoFlujoString, RegistroOxiDomi::setTipoFlujo);

		flujo.setWidth("35px");

		binder.forField(flujo).withValidator(numero -> numero.matches("[0-9]*"), " sólo números")
				.withValidator(numero -> (Integer.parseInt(numero) > 0 && Integer.parseInt(numero) < 10), " de 1 a 10")
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiDomi::getFlujoString, RegistroOxiDomi::setFlujo);

		horasDia.setWidth("35px");
		binder.forField(horasDia).withValidator(numero -> numero.matches("[0-9]*"), " sólo números")
				.withValidator(numero -> (Integer.parseInt(numero) > 0 && Integer.parseInt(numero) < 25), " de 1 a 24")
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiDomi::getHorasDiaString, RegistroOxiDomi::setHorasDia);

		interfase = new ObjetosComunes().docreaComboInterfase(registroDomi.getInterfaseString());
		binder.forField(interfase).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiDomi::getInterfaseString, RegistroOxiDomi::setInterfase);

		presion.setWidth("35px");
		binder.forField(presion).withValidator(numero -> numero.matches("[0-9]*"), " sólo números")
				.withValidator(numero -> (Integer.parseInt(numero) > 0 && Integer.parseInt(numero) <= 100),
						" de 1 a 100 ")
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiDomi::getPresionString, RegistroOxiDomi::setPresion);

		saturacion.setWidth("35px");
		binder.forField(saturacion).withValidator(numero -> numero.matches("[0-9]*"), " sólo números")
				.withValidator(numero -> (Integer.parseInt(numero) > 0 && Integer.parseInt(numero) <= 100),
						" de 1 a 100 ")

				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiDomi::getSaturacionString, RegistroOxiDomi::setSaturacion);

		binder.readBean(registroDomi);

		filaCentrosServicio.addComponents(centroSelect, servicioSelect, modalidadesSelect);
		filaObservaciones.addComponents(observaciones);
		filaDomi.addComponents(tipoFlujoSelect, flujo, horasDia, interfase, presion, saturacion);
		filaBaja.addComponents(fechaBaja, motivoBaja);

		this.contenedorCampos.addComponents(filaCentrosServicio, filaPruebaResultados, filaInicioTipoDuraciçon,
				filaDomi, filaObservaciones, filaBaja);
		this.addComponents(contenedorBotones, contenedorCampos);
		doActivaBotones();
	}

	@Override
	public void doActivaBotones() {
		super.doActivaBotones(registroDomi);
		if (registroDomi.getMotivoBajaString() != null && registroDomi.getMotivoBajaString() != "") {
			LocalDate date = LocalDate.now();
			long dias = ChronoUnit.DAYS.between(registroDomi.getFechaBaja(), date);
			if (dias > Integer.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
				new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
				contenedorCampos.setEnabled(false);
				borrar.setEnabled(false);
				grabar.setEnabled(false);
			}
		}
	}

	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(this.getAyudaHtml()));
	}

	public void grabarClick() {
		if (doValidaFormulario() == true) {
			try {
				binder.writeBean(registroDomi);
				if (!new RegistroDAO().grabaDatos(registroDomi)) {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
					if (registroDomi.getIdPeticion().equals(new Long(0))) {
						new PeticionesDao().doEstadoProcesado(registroDomi.getIdPeticion());
					}
				} else {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
				}
				this.cerrarClick();
			} catch (ValidationException e) {
				new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
				logger.error(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION, e);
			}
		} else {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		}
	}

	/**
	 * Borra el registro.
	 */
	@Override
	public void borraElRegistro() {
		if (new RegistroDAO().doBorrarRegistro(registroDomi)) {
			registroDomi = null;
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_BORRADO);
		} else
			new NotificacionInfo(NotificacionInfo.FORMULARIO_ERROR_DATO_BORRADO);
		cerrarClick();
	}

	@Override
	public boolean doValidaFormulario() {
		boolean valido = true;
		if (!super.doValidaFormulario()) {
			valido = false;
		}

		return valido;
	}

	@Override
	public String getAyudaHtml() {
		String ayudaHtml = super.getAyudaHtml() + "<ul> " + "<li><b>Flujo :</b> .</li>" + "<li><b>Tipo flujo:</b>.</li>"
				+ "</ul>" + Ayudas.AYUDA_BOTONES_ABCA;
		return ayudaHtml;
	}
}
