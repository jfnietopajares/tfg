package com.jnieto.ui;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Registro;
import com.jnieto.utilidades.Parametros;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * The Class Frm_Master. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public abstract class FrmMaster extends VerticalLayout {

	private static final long serialVersionUID = 1814307515922589664L;

	protected HorizontalLayout contenedorBotones = new HorizontalLayout();
	protected VerticalLayout contenedorCampos = new VerticalLayout();

	protected Button grabar, borrar, cerrar, ayuda;

	protected Label lbltitulo = null;

	/**
	 * Instantiates a new frm master.
	 */
	public FrmMaster() {
		this.setSizeFull();
		this.setMargin(false);
		this.addStyleName(MaterialTheme.CARD_HOVERABLE);

		contenedorBotones.setMargin(false);
		contenedorCampos.setMargin(false);

		lbltitulo = new Label();
		lbltitulo.setContentMode(ContentMode.HTML);

		grabar = new ObjetosComunes().getBotonGrabar();
		grabar.addClickListener(event -> grabarClick());
		borrar = new ObjetosComunes().getBotonBorrar();
		borrar.addClickListener(event -> borrarClick());
		borrar.setEnabled(false);
		cerrar = new ObjetosComunes().getBotonCerrar();
		cerrar.addClickListener(event -> cerrarClick());
		ayuda = new ObjetosComunes().getBotonAyuda();
		ayuda.addClickListener(event -> ayudaClick());

		contenedorBotones.addComponents(lbltitulo, grabar, borrar, ayuda, cerrar);

	}

	/**
	 * Do control botones.
	 *
	 * @param id the id
	 */
	public void doActivaBotones(Long id) {
		if (id > 0) {
			borrar.setEnabled(true);
		} else {
			borrar.setEnabled(false);
		}
	}

	public void doActivaBotones(Registro registro) {
		if (registro.getId() > 0) {
			borrar.setEnabled(true);
		} else {
			borrar.setEnabled(false);
		}
		if (registro.getProblema().getMotivo_baja() != null && registro.getProblema().getMotivo_baja() != "") {
			LocalDate date = LocalDate.now();
			long dias = ChronoUnit.DAYS.between(registro.getProblema().getFechafin(), date);

			if (dias > Integer.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
				new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
				contenedorCampos.setEnabled(false);
				borrar.setEnabled(false);
				grabar.setEnabled(false);

			}
		}
	}

	public void doActivaBotones() {

	}

	/**
	 * Cerrar click.
	 */
	public abstract void cerrarClick();

	/**
	 * Ayuda click.
	 */
	public abstract void ayudaClick();

	/**
	 * Grabar click.
	 */
	public abstract void grabarClick();

	/**
	 * Borrar click.
	 */
	public abstract void borrarClick();

	/**
	 * Borra el registro.
	 */
	public abstract void borraElRegistro();

	/**
	 * Do valida formulario.
	 *
	 * @return true, if successful
	 */
	public abstract boolean doValidaFormulario();

	/**
	 * Gets the contenedor botones.
	 *
	 * @return the contenedor botones
	 */
	public HorizontalLayout getContenedorBotones() {
		return contenedorBotones;
	}

	/**
	 * Sets the contenedor botones.
	 *
	 * @param contenedorBotones the new contenedor botones
	 */
	public void setContenedorBotones(HorizontalLayout contenedorBotones) {
		this.contenedorBotones = contenedorBotones;
	}

	/**
	 * Gets the grabar.
	 *
	 * @return the grabar
	 */
	public Button getGrabar() {
		return grabar;
	}

	/**
	 * Sets the grabar.
	 *
	 * @param grabar the new grabar
	 */
	public void setGrabar(Button grabar) {
		this.grabar = grabar;
	}

	/**
	 * Gets the borrar.
	 *
	 * @return the borrar
	 */
	public Button getBorrar() {
		return borrar;
	}

	/**
	 * Sets the borrar.
	 *
	 * @param borrar the new borrar
	 */
	public void setBorrar(Button borrar) {
		this.borrar = borrar;
	}

	/**
	 * Gets the cerrar.
	 *
	 * @return the cerrar
	 */
	public Button getCerrar() {
		return cerrar;
	}

	/**
	 * Sets the cerrar.
	 *
	 * @param cerrar the new cerrar
	 */
	public void setCerrar(Button cerrar) {
		this.cerrar = cerrar;
	}

	/**
	 * Gets the ayuda.
	 *
	 * @return the ayuda
	 */
	public Button getAyuda() {
		return ayuda;
	}

	/**
	 * Sets the ayuda.
	 *
	 * @param ayuda the new ayuda
	 */
	public void setAyuda(Button ayuda) {
		this.ayuda = ayuda;
	}

	/**
	 * Gets the lbltitulo.
	 *
	 * @return the lbltitulo
	 */
	public Label getLbltitulo() {
		return lbltitulo;
	}

	/**
	 * Sets the lbltitulo.
	 *
	 * @param lbltitulo the new lbltitulo
	 */
	public void setLbltitulo(Label lbltitulo) {
		this.lbltitulo = lbltitulo;
	}

	public String getAyudaHtml() {
		return "";
	}
}
