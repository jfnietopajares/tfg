package com.jnieto.ui;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.vaadin.dialogs.ConfirmDialog;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.controlador.AccesoControlador;
import com.jnieto.dao.PacienteDAO;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Parametros;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.LocalDateRenderer;

/**
 * The Class PantallaBuscarPacientes.
 *
 * @author JuanNieto *
 * @author Juan Nieto
 * @version 23.5.2018
 * 
 *          Clase para las busquedas de pacientes de diferentes tipos
 */
public class PantallaBuscarPacientes extends PantallaPaginacion {

	private static final long serialVersionUID = 6802394604145200557L;

	private Grid<Paciente> grid = new Grid<>();

	public TextField filtro = new TextField();

	private HorizontalLayout filaBuscador = new HorizontalLayout();

	private Button buscar, nuevo, ayuda, cerrar;

	private ArrayList<Paciente> listaPacientes = new ArrayList<>();

	private TabSheet tabsheet = new TabSheet();

	private Long subambito = new Long(0);

	private Label lblDescripcionFiltro = new Label();

	DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");

	/**
	 * Instantiates a new pantalla buscar pacientes.
	 *
	 * @param subambito the subambito
	 */
	public PantallaBuscarPacientes(Long subambito) {
		this.subambito = subambito;
		if (subambito.equals(new Long(0)))
			lblDescripcionFiltro.setCaption("");
		else
			lblDescripcionFiltro
					.setCaption("Pacientes activos en el proceso de " + Proceso.getDescripcionSubambito(subambito));

		this.addComponent(tabsheet);
		tabsheet.setStyleName(MaterialTheme.TABSHEET_ICONS_ON_TOP);

		this.setMargin(false);
		this.setSizeFull();
		grid.addColumn(Paciente::getNumerohc).setCaption("Nhc");
		grid.addColumn(Paciente::getFnac, new LocalDateRenderer("dd/MM/yyyy")).setCaption("F.Nac");
		grid.addColumn(Paciente::getNombre).setCaption("Nombre");
		grid.addColumn(Paciente::getApellido1).setCaption("Apellido1");
		grid.addColumn(Paciente::getApellido2).setCaption("Apellido2");
		grid.addColumn(Paciente::getDni).setCaption("DNI");
		grid.addItemClickListener(event -> selecciona());
		grid.setSizeFull();
		grid.asSingleSelect();
		grid.setHeightByRows(10);
		grid.setHeightMode(HeightMode.ROW);

		filtro.setPlaceholder("filtro apellidos historia o dni ");
		filtro.setCursorPosition(0);
		filtro.setMaxLength(100);
		filtro.setWidth("250px");

		buscar = new ObjetosComunes().getBotonBuscar();
		buscar.addClickListener(event -> clickBuscar());
		buscar.setClickShortcut(KeyCode.ENTER);

		nuevo = new ObjetosComunes().getBotonNuevo();
		nuevo.addClickListener(event -> clickNuevo());

		ayuda = new ObjetosComunes().getBotonAyuda();
		ayuda.addClickListener(event -> clickAyuda());

		cerrar = new ObjetosComunes().getBotonCerrar();
		cerrar.addClickListener(event -> clickCerrar());

		filaBuscador.setMargin(false);
		filaBuscador.addComponents(filtro, buscar, nuevo, ayuda, cerrar, lblDescripcionFiltro);

		VerticalLayout vtLayout = new VerticalLayout();
		vtLayout.addComponents(filaBuscador, grid, filaPaginacion);

		paginacion = new PacienteDAO().getPaginacionPaciente(filtro.getValue().toUpperCase(), subambito);
		paginacion.setNumeroRegistrosPagina(Integer
				.parseInt((String) MyUI.objParametros.get(Parametros.KEY_NUMERO_REGISTROS_PAGINACION_PACIENTES)));
		setValoresGridBotones();

		tabsheet.addTab(vtLayout, "Pacientes");
	}

	/**
	 * Click buscar.
	 */
	public void clickBuscar() {
		paginacion = new PacienteDAO().getPaginacionPaciente(filtro.getValue().toUpperCase(), subambito);
		paginacion.setNumeroRegistrosPagina(Integer
				.parseInt((String) MyUI.objParametros.get(Parametros.KEY_NUMERO_REGISTROS_PAGINACION_PACIENTES)));

		setValoresGridBotones();
	}

	/**
	 * Click nuevo.
	 */
	public void clickNuevo() {
		((MyUI) getUI()).actualiza(new FrmPaciente(new Paciente()));
	}

	/**
	 * Click ayuda.
	 */
	public void clickAyuda() {
		new VentanaHtml(this.getUI(), new Label(PantallaBuscarPacientes.AYUDA_PANTALLA_BUSCAPACIENTE));

	}

	/**
	 * Click cerrar.
	 */
	public void clickCerrar() {
		this.removeAllComponents();
	}

	/**
	 * Selecciona.
	 * 
	 * Cada paciente abre una nueva solapa
	 */
	public void selecciona() {
		if (grid.getSelectedItems().size() == 1) {
			Paciente paciente = grid.getSelectedItems().iterator().next();
			if (subambito > 0) {
				if (subambito == Proceso.SUBAMBITO_MAMA && paciente.getSexo() == 1) {
					doValidaSexo(paciente);
				} else {
					VerticalLayout contenedorTabLayout = new VerticalLayout();
					new AccesoControlador().doAccesoUsuarioPaciente(paciente);
					contenedorTabLayout.addComponent(new PantallaProcesos(paciente, subambito, null));
					tabsheet.addTab(contenedorTabLayout, paciente.getApellidosNombre()).setClosable(true);
					tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));
				}
			} else {
				((MyUI) getUI()).actualiza(new FrmPaciente(paciente));
			}
		}
	}

	public void doValidaSexo(Paciente paciente) {
		ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, " Asignar proceso mama a un hombre ?",
				Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 6169352858399108337L;

					public void onClose(ConfirmDialog dialog) {
						if (dialog.isConfirmed()) {
							VerticalLayout contenedorTabLayout = new VerticalLayout();
							new AccesoControlador().doAccesoUsuarioPaciente(paciente);
							contenedorTabLayout.addComponent(new PantallaProcesos(paciente, subambito, null));
							tabsheet.addTab(contenedorTabLayout, paciente.getApellidosNombre()).setClosable(true);
							tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));

						}
					}
				});
	}

	/**
	 * Sets the valores grid botones.
	 */
	public void setValoresGridBotones() {
		listaPacientes = new PacienteDAO().getListaPacientes(filtro.getValue().toUpperCase(), paginacion, subambito);
		setValoresPgHeredados(listaPacientes);
		if (listaPacientes.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO + filtro.getValue());
		} else {
			grid.setItems(listaPacientes);
			if (listaPacientes.size() == 1) {
			}
		}
	}

	/**
	 * Sets the valores pg heredados.
	 *
	 * @param listaPacientes the new valores pg heredados
	 */
	public void setValoresPgHeredados(ArrayList<Paciente> listaPacientes) {
		ArrayList<Integer> listaValoresArrayList = new ArrayList<>();

		for (Paciente paciente : listaPacientes) {
			listaValoresArrayList.add(paciente.getNumeroOrden());
		}
		setBotonesPaginacion(listaValoresArrayList);
	}

	/**
	 * Gets the fila buscador.
	 *
	 * @return the fila buscador
	 */
	public HorizontalLayout getFilaBuscador() {
		return filaBuscador;
	}

	/**
	 * Gets the filtro.
	 *
	 * @return the filtro
	 */
	public TextField getFiltro() {
		return filtro;
	}

	/**
	 * Sets the filtro.
	 *
	 * @param filtro the new filtro
	 */
	public void setFiltro(TextField filtro) {
		this.filtro = filtro;
	}

	/** The Constant AYUDA_FRM_CENTROS. */
	public static final String AYUDA_PANTALLA_BUSCAPACIENTE = "<b>Pantalla de  búsqueda de pacientes. \n  </b> <br>"
			+ " Si hay un proceso seleccionado, permite buscar pacientes con procesos activos.<br>"
			+ " Si noy hay un proceso seleccionado, realiza una búsqued general de pacientes.<br>"
			+ " Seleccionando una paciente se accede a la pantalla de edición del procesos y los <br>"
			+ " registros asociados al proceso. <br>"
			+ " El formulario de cada paciente se abre en una nueva solapa lo que permite  trabajar. <br>"
			+ " con varios pacientes de forma simultánea <br>" + "<hr>" + "<ul> "
			+ "<li><b>Filtro:</b>  Es posible buscar por nombre, uno o dos apellidos, dni o historia.</li>"
			+ "<li>        Las búsquedas se realizan por combinaciones de todos los campos.</li>"
			+ "<li><b>Buscar:</b>Botón para buscar.</li>"
			+ "<li><b>+</b> Abre a ficha completa de datos del paciente	</li>" + "<li><b>?</b> Esta pantall</li>"
			+ "<li><b>x</b> Cierra la ventana </li>" + "</ul>  <hr> ";

}
