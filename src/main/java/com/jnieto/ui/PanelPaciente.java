package com.jnieto.ui;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.entity.Edad;
import com.jnieto.entity.Paciente;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

// TODO: Auto-generated Javadoc
/**
 * The Class PanelPaciente.
 */
public class PanelPaciente extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3497603835747985208L;

	/** The paciente. */
	private Paciente paciente = null;

	private Label pacientelbl = new Label();

	/**
	 * Instantiates a new panel paciente.
	 *
	 * @param paciente the paciente
	 */
	public PanelPaciente(Paciente paciente) {
		this.paciente = paciente;

		this.setMargin(false);
		this.setSizeUndefined();
		this.setWidth("550px");
		this.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		this.addStyleName(MaterialTheme.CARD_HOVERABLE);

		pacientelbl.setContentMode(ContentMode.HTML);
		pacientelbl.setContentMode(ContentMode.HTML);
		pacientelbl.setSizeFull();
		this.addComponent(pacientelbl);
		if (paciente != null) {

			String contenidoString;

			contenidoString = "<h3><hr><b>Paciente:</b> " + paciente.getApellidosNombre();
			if (paciente.getFnac() != null) {
				contenidoString = contenidoString.concat(" (" + new Edad(paciente.getFnac()).getEdadUnidades() + ")");
			}
			contenidoString = contenidoString.concat("<br><hr> ");
			contenidoString = contenidoString.concat("<b>Id: </b>" + paciente.getId() + " <br> ");
			contenidoString = contenidoString.concat("<b>Historia: </b>" + paciente.getNumerohc() + " <br> ");
			contenidoString = contenidoString.concat("<b>Teléfono: </b>" + paciente.getTelefono() + " <br>  ");
			contenidoString = contenidoString.concat("<b>Móvil: </b>" + paciente.getMovil() + " <br>  ");
			contenidoString = contenidoString.concat("<b>Dni: </b>" + paciente.getDni() + " <br>  ");
			contenidoString = contenidoString.concat("<b>Naci: </b>" + paciente.getFnac() + " <br>  ");
			contenidoString = contenidoString.concat("<b> </b>" + "" + " <br> <hr> ");
			contenidoString = contenidoString.concat("</h3> ");
			pacientelbl.setValue(contenidoString);

		}

	}

}
