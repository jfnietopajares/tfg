package com.jnieto.ui;

import java.util.ArrayList;

import com.jnieto.dao.ParametroDAO;
import com.jnieto.entity.ParametBBDD;
import com.jnieto.utilidades.Ayudas;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;

/**
 * The Class PantallaPatrametros. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class PantallaPatrametros extends PantallaMaster {

	private static final long serialVersionUID = 7020754381604032105L;

	Grid<ParametBBDD> grid = new Grid<>();

	private ParametBBDD parametro;

	private ArrayList<ParametBBDD> listaParametros = null;

	/**
	 * Instantiates a new pantalla patrametros.
	 */
	public PantallaPatrametros() {
		super();
		grid.setCaption(" Listado  de parametros ");
		grid.addColumn(ParametBBDD::getCodigo).setCaption("Código");
		grid.addColumn(ParametBBDD::getDescripcion).setCaption("Descripción");
		grid.setItems(new ParametroDAO().getListaRegistros(null));
		grid.setWidthUndefined();
		grid.addItemClickListener(event -> selecciona());

		filtro.setPlaceholder(" datos buscar ");
		paginacion = new ParametroDAO().getPaginacionRegistros(filtro.getValue());
		setValoresGridBotones();
		contenedorGrid.addComponents(contenedorBotones, grid, filaPaginacion);
	}

	/**
	 * Buscar click.
	 */
	public void buscarClick() {
		listaParametros = new ParametroDAO().getListaRegistros(filtro.getValue());
		if (listaParametros.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.GRID_DATOS_NO_ENCONTRADOS_FILTRO + " " + filtro.getValue());
			filtro.clear();
			filtro.focus();
		} else {
			grid.setItems(listaParametros);
			if (listaParametros.size() == 1) {
				parametro = listaParametros.get(0);
				doFormularioParametro(parametro);
			}
		}

	}

	/**
	 * Anadir click.
	 */
	public void anadirClick() {
		parametro = new ParametBBDD();
		doFormularioParametro(parametro);
	}

	/**
	 * Refrescar click.
	 */
	public void refrescarClick() {
		filtro.clear();
		filtro.focus();
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.setVisible(false);
		contenedorGrid.setEnabled(true);
		paginacion = new ParametroDAO().getPaginacionRegistros(filtro.getValue());
		setValoresGridBotones();
	}

	/**
	 * Ayuda click.
	 */
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(PantallaPatrametros.AYUDA_PANTALLA_PARAMETROS));
	}

	/**
	 * Cerrar click.
	 */
	public void cerrarClick() {
		this.removeAllComponents();
	}

	/**
	 * Do formulario parametro.
	 *
	 * @param parametro the parametro
	 */
	public void doFormularioParametro(ParametBBDD parametro) {
		contenedorGrid.setEnabled(false);
		contenedorFormulario.setVisible(true);
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.addComponent(new FrmParametro(parametro));
	}

	/**
	 * Selecciona.
	 */
	public void selecciona() {
		if (grid.getSelectedItems().size() == 1) {
			parametro = grid.getSelectedItems().iterator().next();
			doFormularioParametro(parametro);
		}
	}

	/**
	 * Sets the valores grid botones.
	 */
	public void setValoresGridBotones() {
		listaParametros = new ParametroDAO().getListaRegistrosPaginados(filtro.getValue().toUpperCase(), paginacion);

		setValoresPgHeredados(listaParametros);
		if (listaParametros.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO + filtro.getValue());
		} else {
			grid.setItems(listaParametros);
			if (listaParametros.size() == 1) {
				parametro = listaParametros.get(0);
				doFormularioParametro(parametro);
			}
		}
	}

	/**
	 * Sets the valores pg heredados.
	 *
	 * @param listaParametros the new valores pg heredados
	 */
	public void setValoresPgHeredados(ArrayList<ParametBBDD> listaParametros) {
		ArrayList<Integer> listaValoresArrayList = new ArrayList<>();
		for (ParametBBDD parametro : listaParametros) {
			listaValoresArrayList.add(parametro.getNumeroOrden());
		}
		setBotonesPaginacion(listaValoresArrayList);
	}

	/**
	 * Do control botones.
	 */
	@Override
	public void doControlBotones() {

	}

	/** The Constant AYUDA_PANTALLA_PARAMETROS. */
	public static final String AYUDA_PANTALLA_PARAMETROS = " <b>Pantalla de revisión  de parámetros  :\n  </b> <br>"
			+ " Permite buscar y seleccionar entre los parámetros actuales <br>"
			+ " para modificar sus valores o añadir nuevos parámetros.<br><hr>" + Ayudas.AYUDA_BOTONES_BARCA;
}
