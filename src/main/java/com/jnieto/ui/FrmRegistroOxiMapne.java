package com.jnieto.ui;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.vaadin.dialogs.ConfirmDialog;

import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.RegistroOxiMapnea;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Parametros;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

/**
 * The Class FrmRegistroOxiMapne. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class FrmRegistroOxiMapne extends FrmRegistroOxi {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5126203907478578638L;

//	private TextField equipo = new TextField("Equipo");

	private TextField tiempoRampa = new TextField("Tiempo rampa");

	private TextField presionRampa = new TextField("Presion rampa");

	private TextField presion = new TextField("Presión");

	private HorizontalLayout filaMapnea = new HorizontalLayout();

	RegistroOxiMapnea registroMapnea = new RegistroOxiMapnea();

	Binder<RegistroOxiMapnea> binder = new Binder<RegistroOxiMapnea>();

	public FrmRegistroOxiMapne(RegistroOxiMapnea paraMapnea) {
		super();
		this.setMargin(false);
		this.registroMapnea = paraMapnea;
		filaCentrosServicio.setMargin(false);
		filaMapnea.setMargin(false);

		lbltitulo.setCaption(registroMapnea.getVariableTerapia().getValor() + " " + registroMapnea.getProblema().getId()
				+ "/" + registroMapnea.getId());
		lbltitulo.setVisible(true);

		binder.forField(centroSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiMapnea::getCentro, RegistroOxiMapnea::setCentro);
		binder.forField(servicioSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiMapnea::getServicio, RegistroOxiMapnea::setServicio);
		modalidadesSelect = this.creaComboModalidades(registroMapnea.getVariableTerapia().getValor(), "");
		binder.forField(modalidadesSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiMapnea::getModalidadString, RegistroOxiMapnea::setModalidad);
		binder.forField(pruebaDiagnostica).bind(RegistroOxiMapnea::getPruebaDiagnosticaString,
				RegistroOxiMapnea::setPruebaDiagnostica);
		binder.forField(resultadosPrueba).bind(RegistroOxiMapnea::getResultadosPruebaString,
				RegistroOxiMapnea::setResultadosPrueba);
		binder.forField(tipoPrescripcionSelest).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiMapnea::getTipoPrescripcionString, RegistroOxiMapnea::setTipoPrescripcion);
		tipoPrescripcionSelest.addBlurListener(e -> cambiaTipoPrescrip());
		binder.forField(fechaInicio).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiMapnea::getFechaInicio, RegistroOxiMapnea::setFechaInicio);
		binder.forField(duracionSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiMapnea::getDuracionString, RegistroOxiMapnea::setDuracion);
		observaciones.setWidth("600px");
		observaciones.setHeight("30px");
		binder.forField(observaciones).bind(RegistroOxiMapnea::getObservacionesString,
				RegistroOxiMapnea::setObservaciones);
		binder.forField(fechaBaja).withValidator(returnDate -> {
			if (returnDate == null)
				return true;
			else
				return !returnDate.isBefore(fechaInicio.getValue());
		}, "Mayor o igual que fecha inicio ").bind(RegistroOxiMapnea::getFechaBaja, RegistroOxiMapnea::setFechaBaja);
		binder.forField(motivoBaja).bind(RegistroOxiMapnea::getMotivoBajaString, RegistroOxiMapnea::setMotivoBaja);

		tiempoRampa.setMaxLength(4);
		tiempoRampa.setWidth("60px");
		binder.forField(tiempoRampa).withValidator(numero -> numero.matches("[0-9]*"), " sólo números")
				.withValidator(numero -> (Integer.parseInt(numero) >= 0 && Integer.parseInt(numero) < 31),
						" de 0 a 30 ")
				.bind(RegistroOxiMapnea::getTiempoRampaString, RegistroOxiMapnea::setTiempoRampa);

		presionRampa.setMaxLength(4);
		presionRampa.setWidth("60px");
		binder.forField(presionRampa).withValidator(numero -> numero.matches("[0-9]*"), " sólo números")
				.withValidator(numero -> (Integer.parseInt(numero) >= 0 && Integer.parseInt(numero) < 31),
						" de 0 a 30 ")
				.bind(RegistroOxiMapnea::getPresionRampaString, RegistroOxiMapnea::setPresionRampa);

		presion.setMaxLength(8);
		presion.setWidth("60px");
		binder.forField(presion).withValidator(numero -> numero.matches("[0-9]*"), " sólo números")
				.withValidator(numero -> (Integer.parseInt(numero) >= 0 && Integer.parseInt(numero) < 31),
						" de 0 a 30 ")
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiMapnea::getPresionString, RegistroOxiMapnea::setPresion);

		binder.readBean(registroMapnea);

		filaCentrosServicio.addComponents(centroSelect, servicioSelect, modalidadesSelect);
		filaMapnea.addComponents(tiempoRampa, presionRampa, presion);
		filaObservaciones.addComponents(observaciones);

		filaBaja.addComponents(fechaBaja, motivoBaja);

		this.contenedorCampos.addComponents(filaCentrosServicio, filaPruebaResultados, filaInicioTipoDuraciçon,
				filaMapnea, filaObservaciones, filaBaja);
		this.addComponents(contenedorBotones, contenedorCampos);
		doActivaBotones();
	}

	@Override
	public void doActivaBotones() {
		super.doActivaBotones(registroMapnea);
		if (registroMapnea.getMotivoBajaString() != null && registroMapnea.getMotivoBajaString() != "") {
			LocalDate date = LocalDate.now();
			long dias = ChronoUnit.DAYS.between(registroMapnea.getFechaBaja(), date);
			if (dias > Integer.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
				new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
				contenedorCampos.setEnabled(false);
				borrar.setEnabled(false);
				grabar.setEnabled(false);
			}
		}
	}

	@Override
	public void cerrarClick() {
		this.removeAllComponents();
		this.setVisible(false);
		((PantallaProcesos) this.getParent().getParent().getParent()).refrescarClick();
	}

	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(this.getAyudaHtml()));
	}

	public void grabarClick() {
		if (doValidaFormulario() == true) {
			try {
				binder.writeBean(registroMapnea);
				if (!new RegistroDAO().grabaDatos(registroMapnea)) {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);

				} else {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
				}
				this.cerrarClick();
			} catch (ValidationException e) {
				new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
			}
		} else {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		}
	}

	/**
	 * Borrar click.
	 */
	@Override
	public void borrarClick() {
		ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, Constantes.CONFIRMACION_BORRADO_MENSAJE,
				Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 6169352858399108337L;

					public void onClose(ConfirmDialog dialog) {
						if (dialog.isConfirmed()) {
							borraElRegistro();
						}
					}
				});
		this.cerrarClick();
	}

	/**
	 * Borra el registro.
	 */
	@Override
	public void borraElRegistro() {
		if (new RegistroDAO().doBorrarRegistro(registroMapnea)) {
			registroMapnea = null;
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_BORRADO);
		} else
			new NotificacionInfo(NotificacionInfo.FORMULARIO_ERROR_DATO_BORRADO);
		cerrarClick();
	}

	@Override
	public boolean doValidaFormulario() {
		boolean valido = true;
		if (!super.doValidaFormulario()) {
			valido = false;
		}

		return valido;
	}

	@Override
	public String getAyudaHtml() {
		String ayudaHtml = super.getAyudaHtml() + "<ul> " + "<li><b>Equipo :</b> Número del equipo.</li>"
				+ "<li><b>Número de días:</b>Número de días.</li>" + "</ul>" + Ayudas.AYUDA_BOTONES_ABCA;
		return ayudaHtml;
	}
}
