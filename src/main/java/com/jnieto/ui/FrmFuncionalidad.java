package com.jnieto.ui;

import org.vaadin.dialogs.ConfirmDialog;

import com.jnieto.dao.FuncionalidadDAO;
import com.jnieto.entity.Funcionalidad;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Constantes;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

/**
 * The Class FrmFuncionalidad. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class FrmFuncionalidad extends FrmMaster {

	private static final long serialVersionUID = 1061789637013079285L;

	private TextField id = null;

	private TextField descripcion = null;

	Binder<Funcionalidad> binder = new Binder<>();

	private Funcionalidad funcionalidad = null;

	/**
	 * Instantiates a new frm funcionalidad.
	 *
	 * @param funcionalidad the funcionalidad
	 */
	public FrmFuncionalidad(Funcionalidad funcionalidad) {
		this.funcionalidad = funcionalidad;
		lbltitulo.setCaption("Datos de funcionalidades");

		id = new TextField("Id");
		id.setEnabled(false);
		id.setWidth("100px");
		binder.forField(id).bind(Funcionalidad::getIdString, Funcionalidad::setIdString);

		descripcion = new TextField("Descripcion");
		descripcion.setWidth("300px");
		binder.forField(descripcion).withValidator(new StringLengthValidator(" De 0 a 50 ", 0, 50))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(Funcionalidad::getDescripcion, Funcionalidad::setDescripcion);

		binder.readBean(funcionalidad);
		doActivaBotones(funcionalidad.getId());
		this.addComponents(contenedorBotones, id, descripcion);
	}

	/**
	 * Control de botones de acción Las funcionalidades no se pueden borrar. La
	 * ligica de la aplición la usa para el control de menús de los usuarios
	 */

	@Override
	public void cerrarClick() {
		this.removeAllComponents();
		((PantallaFuncionalidades) this.getParent().getParent().getParent()).getContenedorGrid().setEnabled(true);
		((PantallaFuncionalidades) this.getParent().getParent().getParent()).refrescarClick();
	}

	/**
	 * Ayuda click.
	 */
	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(FrmFuncionalidad.AYUDA_FRM_FUNCIONALIDAD));
	}

	/**
	 * Grabar click.
	 */
	@Override
	public void grabarClick() {
		try {
			binder.writeBean(funcionalidad);
			if (!new FuncionalidadDAO().grabaDatos(funcionalidad)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			cerrarClick();

		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		}

	}

	/**
	 * Borrar click.
	 */
	@Override
	public void borrarClick() {
		ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, Constantes.CONFIRMACION_BORRADO_MENSAJE,
				Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 6169352858399108337L;

					public void onClose(ConfirmDialog dialog) {
						if (dialog.isConfirmed()) {
							borraElRegistro();
						}
					}
				});

	}

	/**
	 * Borra el registro.
	 */
	@Override
	public void borraElRegistro() {
		if (new FuncionalidadDAO().borraDatos(funcionalidad)) {
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_BORRADO);
		} else
			new NotificacionInfo(NotificacionInfo.FORMULARIO_ERROR_DATO_BORRADO);

		cerrarClick();
	}

	/**
	 * Do control botones.
	 *
	 * @param id the id
	 */
	@Override
	public void doActivaBotones(Long id) {
		if (id.equals(new Long(0))) {
			borrar.setEnabled(false);
		} else if (new FuncionalidadDAO().getReferenciasExternas(id)) {
			borrar.setEnabled(false);
			borrar.setDescription(NotificacionInfo.SQLREFERENCIASEXTERNAS);
		} else {
			borrar.setEnabled(true);
		}
	}

	/**
	 * Do valida formulario.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean doValidaFormulario() {
		return true;
	}

	/** The Constant AYUDA_FRM_FUNCIONALIDAD. */
	public static final String AYUDA_FRM_FUNCIONALIDAD = " <b>Pantalla de mantenimiento de funcionalidades  :\n  </b> <br>"
			+ "Descripción de campos:" + "<ul> <li><b>Id:</b>  Campo de identificación interna no modificable.</li>"
			+ "<li><b>Descripción:</b> Descripción de la funcionalidad. </li>" + "</ul> "
			+ "<b> No se pueden  Borrar </b> <hr>" + Ayudas.AYUDA_BOTONES_ABCA;
}
