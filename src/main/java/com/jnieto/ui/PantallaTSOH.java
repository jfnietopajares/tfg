package com.jnieto.ui;

import java.time.LocalDate;
import java.util.ArrayList;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.dao.ProcesoDAO;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.dao.ResultadosTsohDAO;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.RegistroColon;
import com.jnieto.entity.ResultadoTsoh;
import com.jnieto.utilidades.Utilidades;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

/**
 * @author Juan Nieto
 * @version 23.5.2018
 * 
 *          The Class PantallaTSOH.
 * 
 *          Revisa los resultados de las analíticas del laboratorio
 */
public class PantallaTSOH extends PantallaPaginacion {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5718607311870956569L;

	private VerticalLayout columnaResultados = new VerticalLayout();

	private HorizontalLayout filafechas = new HorizontalLayout();

	private DateField desde = null;

	private DateField hasta = null;

	private com.vaadin.ui.Button boton = null;

	private Grid<ResultadoTsoh> grid = new Grid<>();

//	private ResultadoTsoh resultado = new ResultadoTsoh();

	ArrayList<ResultadoTsoh> listaResultadosTsohs = new ArrayList<>();

	TabSheet tabsheet = new TabSheet();

	/**
	 * Instantiates a new pantalla TSOH.
	 */
	public PantallaTSOH() {
		super();
		this.addComponent(tabsheet);
		tabsheet.setStyleName(MaterialTheme.TABSHEET_ICONS_ON_TOP);

		this.setMargin(false);
		columnaResultados.setMargin(false);
		filafechas.setMargin(false);

		desde = new ObjetosComunes().getFecha("Desde", " desde fecha");
		desde.setValue(LocalDate.now());

		hasta = new ObjetosComunes().getFecha("Hasta", " hasta fecha");
		hasta.setValue(LocalDate.now());

		boton = new ObjetosComunes().getBotonGrabar();

		boton.addClickListener(event -> procesarClick());
		filafechas.addComponents(desde, hasta, boton);
		grid.setCaption(" Lista de peticiones pendientes   ");
		grid.addColumn(ResultadoTsoh::getId).setCaption("Id");
		grid.addColumn(ResultadoTsoh::getNombrePaciente).setCaption("Paciente");
		grid.addColumn(ResultadoTsoh::getResultado).setCaption("Resultado");
		grid.addColumn(ResultadoTsoh::getComentario).setCaption("Comentario");

		columnaResultados.addComponents(filafechas, grid);
		tabsheet.addTab(columnaResultados, "TSOH");
	}

	public void procesarClick() {
		listaResultadosTsohs = new ResultadosTsohDAO().getResultadosTsho(desde.getValue(), hasta.getValue());
		for (ResultadoTsoh r : listaResultadosTsohs) {
			r.setComentario(procesa(r));
			grid.setItems(listaResultadosTsohs);
		}
	}

	public String procesa(ResultadoTsoh r) {
		String resultado = r.getResultado();
		String valorString = "?";
		if (Utilidades.isNumeric(resultado) == true) {
			if (new ProcesoDAO().getProcesoPaciSub(r.getPaciente().getId(), Proceso.SUBAMBITO_COLON) == null) {
				Proceso proceso = new Proceso();
				proceso.setPaciente(r.getPaciente());
				proceso.setServicio(Proceso.getServicioDefecto(Proceso.SUBAMBITO_COLON));
				proceso.setSubambito(Proceso.SUBAMBITO_COLON);
				proceso.setMotivo("screeneng colon");
				proceso.setFechaini(r.getFechaDate());
				Long id = new ProcesoDAO().getInsertaDatos(proceso);
				if (id != null) {
					proceso.setId(id);
					RegistroColon rcColon = new RegistroColon();
					rcColon.setFecha(r.getFechaDate());
					rcColon.setPaciente(r.getPaciente());
					rcColon.setProblema(proceso);
					rcColon.setAnalitica(r.getFechaDate());
					rcColon.setResultado(r.getResultado());
					if (new RegistroDAO().actualizaDatos(rcColon)) {
						valorString = "Procesado";
					} else {
						valorString = " No inserta registro";
					}
				} else {
					valorString = "No inserta proceso";
				}
			}
		}
		// }
		return valorString;
	}

	/**
	 * Asigna valores grid botones.
	 */
	@Override
	public void setValoresGridBotones() {

	}

}
