package com.jnieto.ui;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.vaadin.dialogs.ConfirmDialog;

import com.jnieto.continuidad.MyUI;
import com.jnieto.controlador.MensajeCtrlInserta;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.RegistroMama;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Parametros;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

/**
 * The Class FrmRegistroMama. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class FrmRegistroMama extends FrmMaster {

	private static final long serialVersionUID = -1854278228585067954L;

	private RegistroMama registromama = new RegistroMama();

	private TextField id = new TextField();

	private TextField idPcam = new TextField();

	private DateField fechaap = null;

	private TextField tnm = new TextField();

	private TextField birads = new TextField();

	private TextField biradscorregido = new TextField();

	private DateField fechagine = null;

	private DateField fechacomite = null;

	private DateField fechaprueba = null;

	private DateField fechatratamiento = null;

	protected TextArea observaciones = new TextArea();

	private Button informe = null;

	private Binder<RegistroMama> binder = new Binder<>();

	private HorizontalLayout fila1 = new HorizontalLayout();

	private HorizontalLayout fila2 = new HorizontalLayout();

	private HorizontalLayout fila3 = new HorizontalLayout();

	private HorizontalLayout fila4 = new HorizontalLayout();

	/**
	 * Instantiates a new frm registro mama.
	 *
	 * @param registromama the registromama
	 */
	public FrmRegistroMama(RegistroMama registromama) {
		super();
		this.registromama = registromama;
		if (registromama.getId().equals(new Long(0))) {
			registromama.setFechaap(LocalDate.now());
		}

		lbltitulo.setCaption("Formulario Mama " + registromama.getProblema().getId() + "/" + registromama.getId());
		if (registromama.getPaciente() == null) {
			new NotificacionInfo(NotificacionInfo.ERROR_PACIENTE_ES_NUlO, true);
		} else if (registromama.getPaciente().getId() == 0) {
			new NotificacionInfo(NotificacionInfo.ERROR_PACIENTE_ES_NUlO, true);
		} else if (registromama.getProblema() == null) {
			new NotificacionInfo(NotificacionInfo.ERROR_PROCESO_ES_NULO, true);
		} else if (registromama.getProblema().getId() == 0) {
			new NotificacionInfo(NotificacionInfo.ERROR_PROCESO_ES_NULO, true);
		} else {

			fila1.setMargin(false);
			fila2.setMargin(false);
			fila3.setMargin(false);
			fila4.setMargin(false);

			binder.forField(id).bind(RegistroMama::getIdString, RegistroMama::setId);
			/**
			 * .withValidator(returnDate -> !returnDate.isAfter(LocalDate.now()), " Fecha
			 * debe ser menor que hoy") .withValidator(returnDate ->
			 * !returnDate.isBefore(LocalDate.of(1910, 1, 1)), " Fecha mayo de 1910")
			 */
			fechaap = new ObjetosComunes().getFecha("Fecha A.Primaria", " fecha atención primaria");
			binder.forField(fechaap)
					.withValidator(returnDate -> !returnDate.isBefore(registromama.getProblema().getFechaini()),
							" Mayor inicio proceso")
					.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
					.bind(RegistroMama::getFechaapValue, RegistroMama::setFechaap);

			tnm.setCaption("TNM");
			tnm.setPlaceholder(" tnm ");
			tnm.setMaxLength(10);
			tnm.setWidth("50px");
			binder.forField(tnm).bind(RegistroMama::getTnmValue, RegistroMama::setTnm);

			idPcam.setCaption("IdPcam");
			idPcam.setPlaceholder(" id pacam ");
			idPcam.setMaxLength(10);
			idPcam.setWidth("50px");
			binder.forField(idPcam).bind(RegistroMama::getIdpcanValue, RegistroMama::setIdpcan);

			informe = new ObjetosComunes().getBotonVerInforme();
			informe.addClickListener(e -> clickVerInformePdf());

			if (registromama.getProblema().getIdInformeAsociado() > 0) {
				informe.setVisible(true);
			} else {
				informe.setVisible(false);
			}

			birads.setCaption("Birads");
			birads.setPlaceholder(" Birads ");
			birads.setMaxLength(10);
			birads.setWidth("75px");
			binder.forField(birads).bind(RegistroMama::getBiradsValue, RegistroMama::setBirads);

			biradscorregido.setWidth("75px");
			biradscorregido.setCaption("Birads 2 corregido");
			biradscorregido.setPlaceholder(" Birads 2 corregido ");
			biradscorregido.setMaxLength(10);
			binder.forField(biradscorregido).bind(RegistroMama::getBiradscorregidoValue,
					RegistroMama::setBiradscorregidoValue);

			fechaprueba = new ObjetosComunes().getFecha("Fecha Prueba RX", " Fecha prueba radiología oncológico");
			binder.forField(fechaprueba).withValidator(returnDate -> {
				if (returnDate == null)
					return true;
				else
					return !returnDate.isBefore(fechaap.getValue());
			}, "Mayor o igual que fecha ap").bind(RegistroMama::getFechapruebaValue, RegistroMama::setFechaprueba);

			/*
			 * Validator<LocalDate> fechamayor = new Validator<LocalDate>() {
			 * 
			 * @Override public ValidationResult apply(LocalDate fecha, ValueContext
			 * valueContext) { if (fecha != null) { if (fecha.isBefore((ChronoLocalDate)
			 * fechaprueba)) { return ValidationResult.error("fecha menor"); } else { return
			 * ValidationResult.ok(); } } else { return ValidationResult.ok(); } } };
			 */
			fechagine = new ObjetosComunes().getFecha("Fecha Ginecología", " Fecha atención ginecología");

			/**
			 * binder.forField(fechagine).withValidator(returnDate -> { if (returnDate !=
			 * null) if (!returnDate.isBefore(fechaprueba.getValue())) return
			 * ValidationResult.error(); else return ValidationResult.ok(); else return
			 * ValidationResult.ok(); }).bind(RegistroMama::getFechagineValue,
			 * RegistroMama::setFechagine);
			 */
			/**
			 * binder.forField(fechagine).withValidator( returnDate -> (returnDate != null
			 * && !returnDate.isBefore(fechaprueba.getValue())), "Mayor o igual que
			 * fecha").bind(RegistroMama::getFechagineValue, RegistroMama::setFechagine);
			 */

			binder.forField(fechagine).withValidator(returnDate -> {
				if (returnDate == null)
					return true;
				else
					return !returnDate.isBefore(fechaprueba.getValue());
			}, "Mayor o igual que fecha").bind(RegistroMama::getFechagineValue, RegistroMama::setFechagine);

			fechacomite = new ObjetosComunes().getFecha("Fecha Comité", " Fecha comite oncológico");
			binder.forField(fechacomite).withValidator(returnDate -> {
				if (returnDate == null)
					return true;
				else
					return !returnDate.isBefore(fechaprueba.getValue());
			}, "Mayor o igual que fecha gine").bind(RegistroMama::getFechacomiteValue, RegistroMama::setFechacomite);

			fechatratamiento = new ObjetosComunes().getFecha("Fecha Tratamiento", " Fecha tratamiento");
			binder.forField(fechatratamiento).withValidator(returnDate -> {
				if (returnDate == null)
					return true;
				else
					return !returnDate.isBefore(fechaprueba.getValue());
			}, "Mayor o igual que fecha comité").bind(RegistroMama::getFechatratamientoValue,
					RegistroMama::setFechatratamiento);

			observaciones.setCaption("Observaciones");
			observaciones.setWidth("400px");
			observaciones.setHeight("100px");
			observaciones.setMaxLength(400);
			observaciones.setPlaceholder(" observaciones al registro de mama");
			observaciones.setMaxLength(200);
			binder.forField(observaciones).bind(RegistroMama::getObservacionesValue, RegistroMama::setObservaciones);

			binder.readBean(registromama);
			doActivaBotones(registromama.getId());
			fila1.addComponents(fechaap, birads, tnm, idPcam, informe);
			fila2.addComponents(fechaprueba, biradscorregido);
			fila3.addComponents(fechagine, fechacomite, fechatratamiento);
			fila4.addComponents(observaciones);
			this.addComponents(contenedorBotones, fila1, fila2, fila3, fila4);
		}
	}

	public void clickVerInformePdf() {
		new VentanaVerPdf(this.getUI(), registromama.getProblema().getIdInformeAsociado(),
				VentanaVerPdf.TIPO_VERPDF_INFORME);
	}

	/**
	 * Cerrar click.
	 */
	@Override
	public void cerrarClick() {
		this.removeAllComponents();
		this.setVisible(false);
		((PantallaProcesos) this.getParent().getParent().getParent()).refrescarClick();
	}

	/**
	 * Ayuda click.
	 */
	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(this.getAyudaHtml()));
	}

	/**
	 * Grabar click.
	 */
	@Override
	public void grabarClick() {
		if (doValidaFormulario() == true) {
			try {
				binder.writeBean(registromama);
				if (!new RegistroDAO().grabaDatos(registromama)) {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);

				} else {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
					new MensajeCtrlInserta().doInsertaMsgMama(registromama);
				}
				this.cerrarClick();

			} catch (ValidationException e) {
				new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
			} finally {

			}
		}
	}

	/**
	 * Borrar click.
	 */
	@Override
	public void borrarClick() {
		ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, Constantes.CONFIRMACION_BORRADO_MENSAJE,
				Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 6169352858399108337L;

					public void onClose(ConfirmDialog dialog) {
						if (dialog.isConfirmed()) {
							borraElRegistro();
						}
					}
				});
		this.cerrarClick();
	}

	/**
	 * Borra el registro.
	 */
	@Override
	public void borraElRegistro() {
		if (new RegistroDAO().doBorrarRegistro(registromama)) {
			registromama = null;
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_BORRADO);
		} else
			new NotificacionInfo(NotificacionInfo.FORMULARIO_ERROR_DATO_BORRADO);

		cerrarClick();
	}

	/**
	 * Do valida formulario.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean doValidaFormulario() {
		boolean valido = true;
		/*
		 * if (fechaap.isEmpty()) { valido = false; fechaap.setComponentError(new
		 * UserError(Mensajes.FORMULARIOCAMPOREQUERIDO + fechaap.getCaption())); } else
		 * { fechaprueba.setComponentError(null); } if (!fechaprueba.isEmpty() &&
		 * !fechaap.isEmpty()) { if
		 * (!fechaprueba.getValue().isAfter(fechaap.getValue())) { valido = false;
		 * fechaprueba.setComponentError(new UserError(Mensajes.FORMULARIOFECHAMENOR +
		 * fechaap.getValue())); } } else { fechaprueba.setComponentError(null); } if
		 * (!fechagine.isEmpty() && !fechaprueba.isEmpty()) { if
		 * (!fechagine.getValue().isAfter(fechaprueba.getValue())) { valido = false;
		 * fechagine.setComponentError(new UserError(Mensajes.FORMULARIOFECHAMENOR +
		 * fechaprueba.getValue())); } } else { fechagine.setComponentError(null); } if
		 * (!fechacomite.isEmpty() && !fechagine.isEmpty()) { if
		 * (!fechacomite.getValue().isAfter(fechagine.getValue())) { valido = false;
		 * fechacomite.setComponentError(new UserError(Mensajes.FORMULARIOFECHAMENOR +
		 * fechaprueba.getValue())); } } else { fechacomite.setComponentError(null); }
		 * if (!fechatratamiento.isEmpty() && !fechacomite.isEmpty()) { if
		 * (!fechatratamiento.getValue().isAfter(fechacomite.getValue())) { valido =
		 * false; fechatratamiento .setComponentError(new
		 * UserError(Mensajes.FORMULARIOFECHAMENOR + fechacomite.getValue())); } } else
		 * { fechatratamiento.setComponentError(null); }
		 */
		return valido;
	}

	/**
	 * Do control botones.
	 *
	 * @param id the id
	 */
	@Override
	public void doActivaBotones(Long id) {
		if (id.compareTo(new Long(0)) > 0) {
			borrar.setEnabled(true);
		} else {
			borrar.setEnabled(false);
		}
		if (birads.getValue().isEmpty()) {
			birads.focus();
		}
		// comprueba que tenga fecha de tratamiento
		if (registromama.getFechatratamientoValue() != null) {
			LocalDate date = LocalDate.now();
			long dias = ChronoUnit.DAYS.between(registromama.getFechatratamientoValue(), date);

			if (dias > Integer.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
				new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
				fila1.setEnabled(false);
				fila2.setEnabled(false);
				fila3.setEnabled(false);
				fila4.setEnabled(false);
				borrar.setEnabled(false);
				grabar.setEnabled(false);
			}
		}
		// comprueba que el procesos asociado no esté cerrado
		if (registromama.getProblema().getFechafin() != null) {
			LocalDate date = LocalDate.now();
			long dias = ChronoUnit.DAYS.between(registromama.getProblema().getFechafin(), date);

			if (dias > Integer.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
				new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
				fila1.setEnabled(false);
				fila2.setEnabled(false);
				fila3.setEnabled(false);
				fila4.setEnabled(false);
				borrar.setEnabled(false);
				grabar.setEnabled(false);

			}
		}
	}

	public String getAyudaHtml() {
		String textoHtml = " <b>Pantalla de mantenimiento de casos de mama  :\n  </b> <br>" + "<ul> "
				+ "<li><b>FechaAp:</b>  Fecha de llegada informe atención primaria. Resultado mamografía.</li>"
				+ "<li><b>Birads:</b> Birads del informe de la mamografía  </li>"
				+ "<li><b>TNM:</b> Clasificación TNM de la lesión  </li>"
				+ "<li><b>Fecha prueba RX:</b> Fecha de atención en el servicio de radiodiagnóstico   </li>"
				+ "<li><b>Birads corregido:</b> Birads corregido tras la atención en el servicio de radiodiagnóstico.    </li>"
				+ "<li><b>Fecha atención ginecología :</b> Fecha de atención en el servicio de ginecología.   </li>"
				+ "<li><b>Fecha comité  :</b> Fecha de revisión caso por el comité de tumores.   </li>"
				+ "<li><b>Fecha tratamiento  :</b> Fecha de realización de primer tratamiento.   </li>" + "</ul> "
				+ " <hr> " + Ayudas.AYUDA_BOTONES_ABCA;
		return textoHtml;
	}

}
