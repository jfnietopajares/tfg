package com.jnieto.ui;

import java.io.File;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.controlador.AccesoControlador;
import com.jnieto.controlador.AuthService;
import com.jnieto.dao.UsuarioDAO;
import com.jnieto.entity.Usuario;
import com.jnieto.excepciones.LoginException;
import com.jnieto.excepciones.PasswordException;
import com.jnieto.excepciones.UsuarioBajaException;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Parametros;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

/**
 * The Class PantallaLogin.
 *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class PantallaLogin extends VerticalLayout {

	private static final long serialVersionUID = -4257018428198327487L;

	private Button buttonConectar;

	private TextField username;

	private PasswordField password;

	private CheckBox rememberMe;

	private Usuario usuario = null;

	private int intentos = 0;

	Binder<Usuario> binder = new Binder<>();

	public final static String LOGIN_DATOS_OBLIGATORIOS = " Es necesario registar usuario y clave ";

	public final static String LOGIN_USUARIO_NOENCONTRADO = "Usuario no encontrado ";

	public final static String LOGIN_USUARIO_NOACTIVO = "Usuario no activo en la base de datos ";

	public final static String LOGIN_CONTRASEÑAINCORRECTA = "Constraseña incorrecta ";

	public final static String LOGIN_ERRORAUT = "Error en la autenticacion ";

	public final static String DNI_INCORRECTO = "DNI incorrecto  ";

	public final static String LOGIN_CUENTA_BLOQUEADA = "La cuenta ha sido desactivada. Contacte con el administrador.  ";

	public final static String LOGIN_OK = "Login correcto";

	private final String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();

	/**
	 * Instantiates a new pantalla login.
	 */
	public PantallaLogin() {

		usuario = new Usuario();
		Label titulo = new Label(Constantes.APLICACION_TITULO_PROGRAMA);
		titulo.setContentMode(ContentMode.HTML);
		titulo.setHeight("15px");
		titulo.setSizeUndefined();
		// titulo.setWidth("600px");
		FileResource resourceLogin = new FileResource(new File(basepath + "/WEB-INF/images/login.jpg"));
		Image image = new Image("", resourceLogin);
		/*
		 * Label subtitulo = new Label(Constantes.APLICACION_SUBTITULO_PROGRAMA);
		 * subtitulo.setContentMode(ContentMode.HTML); subtitulo.setIcon(resourceLogin);
		 */
		Label pie = new Label(Constantes.APLICACION_PIE);
		pie.setContentMode(ContentMode.HTML);

		username = new ObjetosComunes().getUserid();
		// username.addBlurListener(event -> doValidaCampo(username.getValue()));
		// username.addBlurListener(event -> doActivaBotones(username.getValue()));
		binder.forField(username).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Usuario::getUserid,
				Usuario::setUserid);

		password = new ObjetosComunes().getPassword();
		binder.forField(password).withValidator(new StringLengthValidator(" De 6 a 50 ", 6, 50))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Usuario::getPassword, Usuario::setPassword);

		rememberMe = new CheckBox("Recordarme ");
		rememberMe.setValue(true);
		rememberMe.setIcon(VaadinIcons.RECYCLE);

		buttonConectar = new ObjetosComunes().getBotonConectar();
		buttonConectar.setEnabled(true);
		buttonConectar.setCaption("Conectar");
		buttonConectar.addClickListener(this::conectarClic);
		HorizontalLayout filaBoton = new HorizontalLayout();
		filaBoton.setMargin(false);
		filaBoton.addComponent(buttonConectar);
		filaBoton.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		FormLayout formLayout = new FormLayout(username, password, rememberMe);
		formLayout.setSizeUndefined();
		formLayout.setMargin(false);
		formLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

		VerticalLayout layout = new VerticalLayout();
		layout.setSizeUndefined();
		layout.setMargin(true);
		layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		layout.addComponents(titulo, image, formLayout, filaBoton, pie);
		layout.addStyleName(MaterialTheme.CARD_HOVERABLE);

		this.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		this.addComponent(layout);

		// setCompositionRoot(layout);
	}

	/**
	 * Do valida campo.
	 *
	 * @param usuario the usuario
	 */

	/*
	 * private void doValidaCampo(String usuario) { try { new
	 * UsuarioDAO().getUsuarioLogin(usuario); } catch (LoginException e) { new
	 * NotificacionInfo(LOGIN_USUARIO_NOENCONTRADO); doLimpiaFormulario(); } catch
	 * (UsuarioBajaException e) { new NotificacionInfo(LOGIN_CUENTA_BLOQUEADA,
	 * true); doLimpiaFormulario(); } }
	 */
	/**
	 * Do activa botones.
	 *
	 * @param usuario the usuario
	 */

	/*
	 * private void doActivaBotones(String usuario) {
	 * buttonConectar.setEnabled(true); }
	 */
	/**
	 * Conectar clic.
	 *
	 * @param event the event
	 */
	private void conectarClic(ClickEvent event) {
		intentos++;
		try {
			// if (intentos <= Usuario.LOGIN_INTENTOS) {
			if (intentos <= Integer.parseInt((String) MyUI.objParametros.get(Parametros.KEY_INTENTOS_LOGIN))) {
				binder.writeBean(usuario);
				doLogin(usuario, rememberMe.getValue());
			} else {
				if (new UsuarioDAO().doBloqueaCuenta(usuario)) {
					new NotificacionInfo(PantallaLogin.LOGIN_CUENTA_BLOQUEADA, true);
					intentos = 0;
					doLimpiaFormulario();
				}
			}
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_NOVALIDOS);
		}
	}

	public void doLimpiaFormulario() {
		username.clear();
		username.focus();
		username.setComponentError(null);
		password.clear();
		password.setComponentError(null);
	}

	/**
	 * Do login.
	 *
	 * @param usuario    the usuario
	 * @param rememberMe the remember me
	 */
	private void doLogin(Usuario usuario, boolean rememberMe) {
		try {
			AuthService.login(usuario.getUserid(), usuario.getPassword(), rememberMe);
			new AccesoControlador().doAccesoUsuarioLoginOk(usuario);
			MyUI ui = (MyUI) UI.getCurrent();
			ui.showPrivateComponent((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME),
					null);
		} catch (LoginException e) {
			new NotificacionInfo(LOGIN_USUARIO_NOENCONTRADO);
			new AccesoControlador().doAccesoUsuarioNoAutorizado(usuario.getUserid());
			doLimpiaFormulario();
		} catch (UsuarioBajaException e) {
			new NotificacionInfo(PantallaLogin.LOGIN_CUENTA_BLOQUEADA, true);
			new AccesoControlador().doAccesoUsuarioDeBaja(usuario);
			doLimpiaFormulario();
		} catch (PasswordException e) {
			new NotificacionInfo(Usuario.LONGIN_CLAVE_INCORRECTA);
			new AccesoControlador().doAccesoUsuarioClaveIncorrecta(usuario);
			password.clear();
			password.setComponentError(null);
			password.focus();
		}
	}
}