package com.jnieto.ui;

import org.apache.commons.codec.digest.DigestUtils;
import org.vaadin.dialogs.ConfirmDialog;

import com.jnieto.dao.FuncionalidadDAO;
import com.jnieto.dao.UsuarioDAO;
import com.jnieto.entity.Usuario;
import com.jnieto.excepciones.LoginException;
import com.jnieto.excepciones.UsuarioBajaException;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Constantes;
import com.jnieto.validators.ValidatorMovil;
import com.jnieto.validators.ValidatorNombre;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.server.UserError;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;

/**
 * The Class FrmUsuario. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class FrmUsuario extends FrmMaster {

	private static final long serialVersionUID = -643756539940518382L;

	private HorizontalLayout fila1 = new HorizontalLayout();

	private HorizontalLayout fila2 = new HorizontalLayout();

	private HorizontalLayout fila3 = new HorizontalLayout();

	private HorizontalLayout fila4 = new HorizontalLayout();

	private Usuario usuario = new Usuario();

	private TextField userid = null;

	private TextField apellido1 = null;

	private TextField apellido2 = null;

	private TextField nombre = null;

	private TextField mail = null;

	private PasswordField password = null;

	private CheckBox activo = null;

	private TextField telefono = null;

	private String mailAnterior = null;

	private String telefonoAnteriorString = null;

	private Binder<Usuario> binder = new Binder<>();

	/**
	 * The funcionalidad usuario.
	 *
	 */
	private TwinColSelect<String> funcionalidadUsuario = null;

	/**
	 * Instantiates a new frm usuario.
	 *
	 * @param usuario the usuario
	 */
	public FrmUsuario(Usuario usuario) {
		this.usuario = usuario;

		lbltitulo.setCaption("Formulario de usuarios");

		fila1.setMargin(false);
		fila2.setMargin(false);
		fila3.setMargin(false);
		fila4.setMargin(false);

		userid = new ObjetosComunes().getUserid();
		binder.forField(userid).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Usuario::getUserid,
				Usuario::setUserid);
		userid.addBlurListener(e -> saltaUserid());

		activo = new ObjetosComunes().getUsuarioActivo();
		activo.setValue(true);
		binder.forField(activo).bind(Usuario::getEstado, Usuario::setEstado);

		apellido1 = new ObjetosComunes().getApeNombre("Apellido 1", " apellido 1");
		apellido1.setMaxLength(60);
		binder.forField(apellido1).withValidator(new ValidatorNombre("Apellido  incorrecto "))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(Usuario::getApellido1, Usuario::setApellido1);

		apellido2 = new ObjetosComunes().getApeNombre("Apellido 2", " apellido 2");
		apellido2.setMaxLength(60);
		binder.forField(apellido2).withValidator(new ValidatorNombre("Apellido  incorrecto "))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(Usuario::getApellido2, Usuario::setApellido2);

		nombre = new ObjetosComunes().getApeNombre("Nombre ", " nombre ");
		binder.forField(nombre).withValidator(new ValidatorNombre("Nombre  incorrecto "))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Usuario::getNombre, Usuario::setNombre);

		mail = new ObjetosComunes().getMail();
		mail.addBlurListener(e -> saltaMail());
		binder.forField(mail).withValidator(new EmailValidator("Dirección de correo incorrecta "))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Usuario::getMail, Usuario::setMail);

		telefono = new ObjetosComunes().getMovil("Móvil", " tf movil");
		telefono.addBlurListener(e -> saltaTelefono());
		binder.forField(telefono).withValidator(new ValidatorMovil("Movil  incorrecto "))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Usuario::getTelefono, Usuario::setTelefono);

		funcionalidadUsuario = new TwinColSelect<>("Funcionalidades");
		funcionalidadUsuario.setItems(new FuncionalidadDAO().getListaRegistrosSoloNombre());
		funcionalidadUsuario.setRows(funcionalidadUsuario.getRows());
		funcionalidadUsuario.setLeftColumnCaption("Disponibles");
		funcionalidadUsuario.setRightColumnCaption("Elegidas");
		if (usuario.getNombreFuncionaliades() != null)
			funcionalidadUsuario.setValue((usuario.getNombreFuncionaliades()));

		password = new ObjetosComunes().getPassword();
		binder.forField(password).bind(Usuario::getPassword, Usuario::setPassword);

		binder.readBean(usuario);
		mailAnterior = usuario.getMail();
		telefonoAnteriorString = usuario.getTelefono();
		doControlBotones(usuario);
		fila1.addComponents(userid, password, activo);
		fila2.addComponents(apellido1, apellido2);
		fila3.addComponents(nombre, mail, telefono);
		fila4.addComponent(funcionalidadUsuario);

		this.addComponents(contenedorBotones, fila1, fila2, fila3, fila4);
	}

	/**
	 * Salta userid.
	 */
	public void saltaUserid() {
		if (!userid.getValue().isEmpty()) {
			Usuario usu;
			try {
				usu = new UsuarioDAO().getUsuarioLogin(userid.getValue());
				usuario = usu;
				binder.readBean(usuario);
				userid.setEnabled(false);
				funcionalidadUsuario.setValue((usuario.getNombreFuncionaliades()));
			} catch (LoginException e) {
				// new NotificacionInfo(PantallaLogin.LOGIN_USUARIO_NOENCONTRADO);
			} catch (UsuarioBajaException e) {
				// new NotificacionInfo(PantallaLogin.LOGIN_USUARIO_NOACTIVO);
			}
		}
	}

	/**
	 * Salta mail. Comprueba que el mail registrado en la pantala no esté repetido
	 * en la base de datos.
	 */
	public void saltaMail() {
		if (new UsuarioDAO().getUsuarioMail(mail.getValue(), usuario.getUserid()) != null) {
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_REPETIDO);
			mail.clear();
			mail.focus();
			if (mailAnterior != null)
				mail.setValue(mailAnterior);
		}

	}

	/**
	 * Salta telefono. Comprueba en la base de datos que el teléfono no esté
	 * repetido.
	 */
	public void saltaTelefono() {
		if (new UsuarioDAO().getUsuarioTelefono(telefono.getValue(), usuario.getUserid()) != null) {
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_REPETIDO);
			telefono.clear();
			telefono.focus();
			if (telefonoAnteriorString != null)
				telefono.setValue(telefonoAnteriorString);
		}

	}

	/**
	 * Ayuda click.
	 */
	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(FrmUsuario.AYUDA_FRM_USUARIOS));
	}

	/**
	 * Cerrar click.
	 */
	@Override
	public void cerrarClick() {
		this.removeAllComponents();
		((PantallaUsuarios) this.getParent().getParent().getParent()).getContenedorGrid().setEnabled(true);
		((PantallaUsuarios) this.getParent().getParent().getParent()).refrescarClick();
	}

	/**
	 * Borrar click.
	 */
	@Override
	public void borrarClick() {
		if (usuario != null) {
			if (new UsuarioDAO().getDatosenBBDD(usuario) == 0) {
				ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO,
						Constantes.CONFIRMACION_BORRADO_MENSAJE, Constantes.CONFIRMACION_BOTONSI,
						Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {

							private static final long serialVersionUID = 6169352858399108337L;

							public void onClose(ConfirmDialog dialog) {
								if (dialog.isConfirmed()) {
									borraElRegistro();
								}
							}
						});

			} else {
				new NotificacionInfo(NotificacionInfo.SQLREGISTROSERRORBORRADO);
			}
			cerrarClick();
		}
	}

	/**
	 * Borra el registro.
	 */
	public void borraElRegistro() {
		if (new UsuarioDAO().borraDatos(usuario)) {
			new NotificacionInfo(NotificacionInfo.SQLREGISTROSERRORBORRADO);
		}
		cerrarClick();
	}

	/**
	 * Grabar click.
	 */
	@Override
	public void grabarClick() {
		if (this.doValidaFormulario() == true) {
			try {
				binder.writeBean(usuario);
				usuario.setFuncionalidadNombre(funcionalidadUsuario.getSelectedItems());
				if (!usuario.getPassword().isEmpty()) {
					usuario.setPasswordhash(DigestUtils.sha1Hex(usuario.getPassword()));
				}
				UsuarioDAO usuarioDAO = new UsuarioDAO();
				if (usuarioDAO.grabaDatos(usuario) == true) {
					new NotificacionInfo(NotificacionInfo.DATO_GRABADO);
					cerrarClick();
				} else {
					new NotificacionInfo(NotificacionInfo.DATO_ERROR);
				}
			} catch (ValidationException e) {
				new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
			}
		}
	}

	/**
	 * Do valida formulario.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean doValidaFormulario() {
		boolean valido = true;
		if (password.isEmpty() && usuario.getPasswordhash() == null) {
			valido = false;
		} else if (!password.isEmpty()) {
			if (!password.getValue().matches("^(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{6,16}$")) {
				valido = false;
				password.setComponentError(new UserError(NotificacionInfo.AVISO_DATO_NO_VALIDO));
			} else {
				password.setComponentError(null);
			}
		}
		if (funcionalidadUsuario.getSelectedItems().size() == 0) {
			funcionalidadUsuario.setComponentError(new UserError(Usuario.FUNCIONALIDADES_VACIAS));
			valido = false;
		} else {
			funcionalidadUsuario.setComponentError(null);
		}
		return valido;
	}

	/**
	 * Do control botones.
	 *
	 * @param usuario the usuario
	 */
	public void doControlBotones(Usuario usuario) {
		if (usuario.getUserid() != null)
			borrar.setEnabled(true);
		else
			borrar.setEnabled(false);

		if (usuario.getUserid() != null) {
			if (usuario.getUserid().isEmpty()) {
				userid.setEnabled(true);
				borrar.setEnabled(false);
			} else
				userid.setEnabled(false);
		} else
			userid.setEnabled(true);

		if (!new UsuarioDAO().getReferenciasExternas(usuario.getUserid())) {
			borrar.setEnabled(false);
			borrar.setDescription(NotificacionInfo.SQLREFERENCIASEXTERNAS);
		} else {
			borrar.setEnabled(true);
		}
	}

	/** The Constant AYUDA_FRM_USUARIOS. */
	public static final String AYUDA_FRM_USUARIOS = "<b>Pantalla de mantenimiento de usuarios  :\n  </b> <br>"
			+ Ayudas.AYUDA_BOTONES_ABCA
			+ "<ul> <li><b>Usuario:</b>  Campo de identificación única de cada usuario.</li>"
			+ "<li><b>Nombre:</b> Nombre del usuario.</li>" + "<li><b>Apellido1:</b> Primer apellido.</li>"
			+ "<li><b>Apellido2:</b> Segundo apellido.</li>" + "<li><b>Mail:</b> Código electrónico.</li>"
			+ "<li><b>Funcionalidades:</b> Lista de funcionalidades permitidas .</li>" + "</ul> "
			+ "Los campos con *, son obligatorios. " + " <hr>";
}
