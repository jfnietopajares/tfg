package com.jnieto.ui;

import java.time.LocalDate;
import java.util.ArrayList;

import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.MensajesDAO;
import com.jnieto.entity.Mensaje;
import com.jnieto.entity.MensajesEstados;
import com.jnieto.entity.MensajesTipos;
import com.jnieto.utilidades.Parametros;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * The Class PantallaVerMensajes. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class PantallaVerMensajes extends PantallaPaginacion {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1379899727725660284L;

	private Grid<Mensaje> grid = new Grid<>();

	public TextField usuario = null;
	public TextField numerohc = null;
	public DateField desde = null;
	public DateField hasta = null;
	public ComboBox<MensajesEstados> estado = null;
	public ComboBox<MensajesTipos> tipo = null;

	private HorizontalLayout filaBuscador = new HorizontalLayout();
	private VerticalLayout contenedorGrid = new VerticalLayout();

	private Button buscar, ayuda, cerrar;

	private ArrayList<Mensaje> listaMensajes = new ArrayList<>();

	private Mensaje mensaje = null;

	public PantallaVerMensajes() {
		this.setMargin(false);
		this.setSizeFull();

		usuario = new ObjetosComunes().getUserid();
		usuario.addBlurListener(e -> clickBuscar());

		numerohc = new ObjetosComunes().getNumerohc("Historia", " historia ");
		numerohc.addBlurListener(e -> clickBuscar());

		desde = new ObjetosComunes().getFecha("Desde", " desde ");
		desde.setValue(LocalDate.now());

		hasta = new ObjetosComunes().getFecha("Hasta", " hasta ");
		hasta.setValue(LocalDate.now());

		estado = new ObjetosComunes().getComboMensajesEstados(null);
		estado.addBlurListener(e -> clickBuscar());

		tipo = new ObjetosComunes().getComboMensajesTipos(null);
		tipo.addBlurListener(e -> clickBuscar());

		buscar = new ObjetosComunes().getBotonBuscar();
		buscar.addClickListener(e -> clickBuscar());

		ayuda = new ObjetosComunes().getBotonAyuda();
		ayuda.addClickListener(e -> clickAyuda());

		cerrar = new ObjetosComunes().getBotonCerrar();
		cerrar.addClickListener(e -> clickCerrar());

		paginacion = new MensajesDAO().getPaginacionRegistros(desde.getValue(), hasta.getValue(), "", "", 0, 0);
		paginacion.setNumeroRegistrosPagina(Integer
				.parseInt((String) MyUI.objParametros.get(Parametros.KEY_NUMERO_REGISTROS_PAGINACION_PACIENTES)));

		setValoresGridBotones();
		// grid.addColumn(Mensaje::getId).setCaption("Id");
		grid.addColumn(Mensaje::getFechaHora).setCaption("Fecha");
		grid.addColumn(Mensaje::getEstadoString).setCaption("Estado");
		// grid.addColumn(Mensaje::getEstadoInt).setCaption("Estado");
		grid.addColumn(Mensaje::getTipoString).setCaption("Tipo");
		grid.addColumn(Mensaje::getUserid_destinoCorto).setCaption("Userid");
		grid.addColumn(Mensaje::getPacienteNhc).setCaption("Paciente");
		grid.addColumn(Mensaje::getContenidoCorto).setCaption("Contenido");
		grid.addColumn(Mensaje::getError).setCaption("Error");
		grid.addItemClickListener(event -> selecciona());
		grid.setSizeFull();
		grid.asSingleSelect();
		grid.setHeightByRows(10);
		grid.setHeightMode(HeightMode.ROW);
		grid.setItems(listaMensajes);
		filaBuscador.addComponents(usuario, numerohc, desde, hasta, estado, tipo, buscar, ayuda, cerrar);
		contenedorGrid.addComponents(grid, filaPaginacion);
		this.addComponents(filaBuscador, contenedorGrid);

	}

	public void clickBuscar() {
		String nhc = "", usr = "";
		int estadop = 0, tipop = 0;
		if (!numerohc.getValue().isEmpty()) {
			nhc = numerohc.getValue();
		}
		if (!usuario.getValue().isEmpty()) {
			usr = usuario.getValue();
		}
		if (estado.getSelectedItem().isPresent()) {
			estadop = estado.getValue().getEstado();
		}
		if (tipo.getSelectedItem().isPresent()) {
			tipop = tipo.getValue().getTipo();
		}
		paginacion = new MensajesDAO().getPaginacionRegistros(desde.getValue(), hasta.getValue(), usr, nhc, estadop,
				tipop);
		paginacion.setNumeroRegistrosPagina(Integer
				.parseInt((String) MyUI.objParametros.get(Parametros.KEY_NUMERO_REGISTROS_PAGINACION_PACIENTES)));

		setValoresGridBotones();
	}

	public void clickAyuda() {
		new VentanaHtml(this.getUI(), new Label(PantallaVerMensajes.AYUDA_PANTALLA_VERMENSAJES));
	}

	public void clickCerrar() {
		this.removeAllComponents();
	}

	public void selecciona() {
		if (grid.getSelectedItems().size() == 1) {
			mensaje = grid.getSelectedItems().iterator().next();
			new VentanaHtml(this.getUI(), new Label(mensaje.getHtml()), "Datos del mensaje");
		}
	}

	@Override
	public void setValoresGridBotones() {
		String nhc = "", usr = "";
		int estadop = 0, tipop = 0;
		if (!numerohc.getValue().isEmpty()) {
			nhc = numerohc.getValue();
		}
		if (!usuario.getValue().isEmpty()) {
			usr = usuario.getValue();
		}
		if (estado.getSelectedItem().isPresent()) {
			estadop = estado.getValue().getEstado();
		}
		if (tipo.getSelectedItem().isPresent()) {
			tipop = tipo.getValue().getTipo();
		}
		listaMensajes = new MensajesDAO().getListaMensajes(desde.getValue(), hasta.getValue(), usr, nhc, estadop, tipop,
				paginacion);
		setValoresPgHeredados(listaMensajes);
		grid.setItems(listaMensajes);
		if (listaMensajes.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO);
		}
	}

	public void setValoresPgHeredados(ArrayList<Mensaje> listaMensajes) {
		ArrayList<Integer> listaValoresArrayList = new ArrayList<>();
		for (Mensaje mensaje : listaMensajes) {
			listaValoresArrayList.add(mensaje.getNumeroOrden());
		}
		setBotonesPaginacion(listaValoresArrayList);
	}

	public static final String AYUDA_PANTALLA_VERMENSAJES = "<b>Pantalla de consulta de mensajes. \n  </b> <br>"
			+ "Esta pantalla permite revisar los datos de la tabla de mensajes.<br>"
			+ "Haciendo click en un fila se muestra una ventana con todos los datos del mensaje.<br>"
			+ " Se pueden consultar por diferentes criterios:.<br>"
			+ "<ul> <li><b>Desde:</b> Desde la fecha de generación del mensaje.</li>"
			+ "<li><b>Hasta:</b> Hasta la fecha de generación del mensaje.</li>"
			+ "<li><b>Usuario:</b> Consulta por el campo mail de los destinatarios. Cualquier ocurrencia </li>"
			+ "<li><b>Historia:</b> Filtra los mensajes con ese número de historia .</li>"
			+ "<li><b>Estado:</b> Filtra los mensajes con el estado elegido .</li>"
			+ "<li><b>Tipo:</b> Filtra los mensajes con el tipo elegido .</li>" + "</ul>  <hr> "
			+ " Descripción de botones:<br>" + "<ul> <li><b>Buscar:</b> Ejecuta la búsqueda según los criterios.</li>"
			+ "<li><b>?</b> Esta pantalla.</li>" + "<li><b>x</b> Cierra la ventana.</li>"
			+ "<li>Mediantes los botones de paginación puedes desplazar para ver diferentes registros.</li>"
			+ "</ul>  <hr> ";

}
