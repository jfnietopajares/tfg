package com.jnieto.ui;

import java.util.ArrayList;
import java.util.Iterator;

import com.jnieto.dao.GruposCatalogoDAO;
import com.jnieto.entity.Funcionalidad;
import com.jnieto.entity.Indicador;
import com.jnieto.entity.ProcesoOxigeno;
import com.jnieto.entity.RegistroO2Incidencia;
import com.jnieto.entity.Usuario;
import com.jnieto.utilidades.Constantes;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;

/**
 * The Class UtilidadesUi. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class UtilidadesUi {

	/**
	 * Instantiates a new utilidades ui.
	 */
	public UtilidadesUi() {

	}

	/**
	 * Do crea combo centro.
	 *
	 * @param centro the centro
	 * @return the combo box
	 */
	/*
	 * public ComboBox<Servicio> doCreaComboServicio(Servicio servicio) {
	 * ComboBox<Servicio> combo = new ComboBox<>(); ArrayList<Servicio>
	 * listaServicios = new ServiciosDAO().getListaServicios();
	 * combo.setCaption("Servicio"); combo.setItems(listaServicios);
	 * combo.setItemCaptionGenerator(Servicio::getDescripcion); if (servicio !=
	 * null) combo.setSelectedItem(servicio); else if (listaServicios.size() == 1)
	 * combo.setSelectedItem(listaServicios.get(0)); else
	 * combo.setSelectedItem(Constantes.SERVICIO_DEFECTO);
	 * 
	 * combo.setEmptySelectionAllowed(false); return combo; }
	 */

	/**
	 * Docrea combo tipo flujo.
	 *
	 * @param oxiTipoFluo the oxi tipo fluo
	 * @return the combo box
	 */

	/**
	 * Docrea combo tipo prescripcion.
	 *
	 * @param tipo the tipo
	 * @return the combo box
	 */

	/**
	 * Docrea combo duracion.
	 *
	 * @param duracion the duracion
	 * @return the combo box
	 */

	/**
	 * Do valida campo numerico.
	 *
	 * @param campo    the campo
	 * @param inferior the inferior
	 * @param superior the superior
	 * @return true, if successful
	 */
	/*
	 * public boolean doValidaCampoNumerico(TextField campo, Long inferior, Long
	 * superior) { boolean valido = true; if (campo.isEmpty()) { valido = false;
	 * campo.setComponentError(new UserError(Constantes.AVISO_DATO_OBIGATORIO)); }
	 * else if (!Utilidades.isNumeric(campo.getValue())) { valido = false;
	 * campo.setComponentError(new UserError(Constantes.DATO_FUERA_DE_RANGO + " " +
	 * inferior + " " + superior)); } else if (Long.parseLong(campo.getValue()) <
	 * inferior || Long.parseLong(campo.getValue()) > superior) { valido = false;
	 * campo.setComponentError(new UserError(Constantes.DATO_FUERA_DE_RANGO + " " +
	 * inferior + " " + superior)); } else campo.setComponentError(null); return
	 * valido; }
	 */

	/**
	 * Do valida campo combo.
	 *
	 * @param combo the combo
	 * @return true, if successful
	 */
	public boolean doValidaCampoCombo(ComboBox<String> combo) {
		boolean valido = true;
		if (combo.isEmpty()) {
			valido = false;
			combo.setComponentError(new UserError(NotificacionInfo.FORMULARIOCAMPOREQUERIDO));
		} else
			combo.setComponentError(null);
		return valido;
	}

	/**
	 * Do valida campo fecha.
	 *
	 * @param fecha the fecha
	 * @return true, if successful
	 */
	public boolean doValidaCampoFecha(DateField fecha) {
		boolean valido = true;
		if (fecha.isEmpty()) {
			valido = false;
			fecha.setComponentError(new UserError(NotificacionInfo.FORMULARIOCAMPOREQUERIDO));
		} else
			fecha.setComponentError(null);
		return valido;
	}

	/**
	 * Docrea combo tipo incidencia.
	 *
	 * @param tipo the tipo
	 * @return the combo box
	 */
	public ComboBox<String> getComboTipoIncidencia(String tipo) {
		ComboBox<String> combo = new ComboBox<String>();
		combo.setCaption("Elige Tipo Incidencia");
		combo.setWidth("300px");
		combo.setItems(new GruposCatalogoDAO().getValoresPorGrupo(RegistroO2Incidencia.GRUPOSC_O2_TIPOS_INCIDENCIA));
		if (tipo == null)
			combo.setValue(RegistroO2Incidencia.TIPO_INCIDENCIA_DEFECTO);
		else
			combo.setValue(tipo);
		combo.setEmptySelectionAllowed(false);
		return combo;
	}

	public String getTablaHtml(ArrayList<ProcesoOxigeno> listadProcesos, String cabecera) {
		String htmlTabla;
		htmlTabla = "<table>";
		htmlTabla = htmlTabla.concat("<tr><td colspan=\"5\">").concat(cabecera).concat("</td></tr>");
		htmlTabla = htmlTabla
				.concat("<tr><th>Historia</th><th>Paciente</th><th>Motivo</th><th>Inicio</th><th>Fin</th></tr>");
		Iterator<ProcesoOxigeno> it = listadProcesos.iterator();
		while (it.hasNext()) {
			String fila = it.next().toHtmlFila();
			htmlTabla = htmlTabla.concat(fila);
		}
		htmlTabla = htmlTabla.concat("</table>");
		return htmlTabla;
	}

	/**
	 * Gets the tabla html completa.
	 *
	 * @param listadProcesos the listad procesos
	 * @param cabecera       the cabecera
	 * @return the tabla html completa
	 */
	public String getTablaHtmlCompleta(ArrayList<ProcesoOxigeno> listadProcesos, String cabecera) {
		String htmlTabla;
		htmlTabla = "<table>";
		htmlTabla = htmlTabla.concat("<tr><td colspan=\"5\">").concat(cabecera).concat("</td></tr>");
		htmlTabla = htmlTabla
				.concat("<tr><th>Historia</th><th>Paciente</th><th>Motivo</th><th>Inicio</th><th>Fin</th></tr>");
		Iterator<ProcesoOxigeno> it = listadProcesos.iterator();
		while (it.hasNext()) {
			ProcesoOxigeno proceso = it.next();
			String fila = proceso.toHtmlFila();
			htmlTabla = htmlTabla.concat(fila);
			/*
			 * ArrayList<RegistroOxi> lista = new RegistroTerapiaDao()
			 * .getListaTerapiasPacienteProceso(proceso.getPaciente().getId(),
			 * proceso.getId()); Iterator<RegistroOxi> it1 = lista.iterator(); while
			 * (it1.hasNext()) { String filaterapia = it1.next().toHtmlFila(); htmlTabla =
			 * htmlTabla.concat(filaterapia); }
			 */
		}
		htmlTabla = htmlTabla.concat("</table>");
		return htmlTabla;
	}

	/**
	 * Gets the tabla html estadistica.
	 *
	 * @param lista    the lista
	 * @param cabecera the cabecera
	 * @return the tabla html estadistica
	 */
	public String getTablaHtmlEstadistica(ArrayList<Indicador> lista, String cabecera) {
		String htmlTabla;
		htmlTabla = "<table>";
		htmlTabla = htmlTabla.concat("<tr><td colspan=\"4\">").concat(cabecera).concat("</td></tr>");
		htmlTabla = htmlTabla.concat("<tr><th>Año</th><th>Mes</th><th>Alta</th><th>Bajas</th></tr>");
		Iterator<Indicador> it = lista.iterator();
		while (it.hasNext()) {
			Indicador indicador = it.next();
			htmlTabla = htmlTabla.concat("<tr>").concat("<td>" + indicador.getAno() + "</td>")
					.concat("<td>" + indicador.getMes() + "</td>").concat("<td>" + indicador.getAltas() + "</td>")
					.concat("<td>" + indicador.getBajas() + "</td>").concat("</tr>");
		}
		htmlTabla = htmlTabla.concat("</table>");
		return htmlTabla;
	}

	public boolean getVisibilidadBoton(Button boton) {
		boolean visible = false;
		Usuario usuario = (Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME);
		ArrayList<Funcionalidad> funcionalisadesUsu = usuario.getFuncionaliades();
		for (Funcionalidad f : funcionalisadesUsu) {
			if (new Long(f.getId()).equals(MenuPrincipal.MENU_COLON_FUNCIONALIDAD)
					&& boton.getCaption().equals(ObjetosComunes.NOMBRE_BOTON_COLON)) {
				return true;
			}
			if (new Long(f.getId()).equals(MenuPrincipal.MENU_MAMA_FUNCIONALIDAD)
					&& boton.getCaption().equals(ObjetosComunes.NOMBRE_BOTON_MAMA)) {
				return true;
			}
			if (new Long(f.getId()).equals(MenuPrincipal.MENU_OXIGENO_FUNCIONALIDAD)
					&& boton.getCaption().equals(ObjetosComunes.NOMBRE_BOTON_OXIGENOTERAPIA)) {
				return true;
			}
			if (new Long(f.getId()).equals(MenuPrincipal.MENU_PALIATIVOS_FUNCIONALIDAD)
					&& boton.getCaption().equals(ObjetosComunes.NOMBRE_BOTON_PALIATIVOS)) {
				return true;
			}
		}
		return visible;
	}
}
