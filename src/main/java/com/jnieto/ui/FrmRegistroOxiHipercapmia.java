package com.jnieto.ui;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.vaadin.dialogs.ConfirmDialog;

import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.RegistroOxiHipercapnia;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Parametros;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

/**
 * The Class FrmRegistroOxiHipercapmia. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class FrmRegistroOxiHipercapmia extends FrmRegistroOxi {

	/**
	 * 
	 */
	private static final long serialVersionUID = 945598549209664129L;

	private TextField presioninspiracion = new TextField("Presión Inspiración.");

	private TextField presionesspiracion = new TextField("Presión Espiración.");

	private HorizontalLayout filaEapoyo1 = new HorizontalLayout();

	private HorizontalLayout filaEapoyo2 = new HorizontalLayout();

	RegistroOxiHipercapnia registroHipercapmia = new RegistroOxiHipercapnia();

	Binder<RegistroOxiHipercapnia> binder = new Binder<RegistroOxiHipercapnia>();

	public FrmRegistroOxiHipercapmia(RegistroOxiHipercapnia param) {
		super();
		this.setMargin(false);
		this.registroHipercapmia = param;
		filaCentrosServicio.setMargin(false);
		filaEapoyo1.setMargin(false);
		filaEapoyo2.setMargin(false);

		lbltitulo.setCaption(registroHipercapmia.getVariableTerapia().getValor() + " "
				+ registroHipercapmia.getProblema().getId() + "/" + registroHipercapmia.getId());
		lbltitulo.setVisible(true);

		binder.forField(centroSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiHipercapnia::getCentro, RegistroOxiHipercapnia::setCentro);
		binder.forField(servicioSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiHipercapnia::getServicio, RegistroOxiHipercapnia::setServicio);
		modalidadesSelect = this.creaComboModalidades(registroHipercapmia.getVariableTerapia().getValor(), "");
		binder.forField(modalidadesSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiHipercapnia::getModalidadString, RegistroOxiHipercapnia::setModalidad);
		binder.forField(pruebaDiagnostica).bind(RegistroOxiHipercapnia::getPruebaDiagnosticaString,
				RegistroOxiHipercapnia::setPruebaDiagnostica);
		binder.forField(resultadosPrueba).bind(RegistroOxiHipercapnia::getResultadosPruebaString,
				RegistroOxiHipercapnia::setResultadosPrueba);
		binder.forField(tipoPrescripcionSelest).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiHipercapnia::getTipoPrescripcionString, RegistroOxiHipercapnia::setTipoPrescripcion);
		tipoPrescripcionSelest.addBlurListener(e -> cambiaTipoPrescrip());
		binder.forField(fechaInicio).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiHipercapnia::getFechaInicio, RegistroOxiHipercapnia::setFechaInicio);
		binder.forField(duracionSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiHipercapnia::getDuracionString, RegistroOxiHipercapnia::setDuracion);
		observaciones.setWidth("600px");
		observaciones.setHeight("30px");
		binder.forField(observaciones).bind(RegistroOxiHipercapnia::getObservacionesString,
				RegistroOxiHipercapnia::setObservaciones);
		binder.forField(fechaBaja).withValidator(returnDate -> {
			if (returnDate == null)
				return true;
			else
				return !returnDate.isBefore(fechaInicio.getValue());
		}, "Mayor o igual que fecha inicio ").bind(RegistroOxiHipercapnia::getFechaBaja,
				RegistroOxiHipercapnia::setFechaBaja);
		binder.forField(motivoBaja).bind(RegistroOxiHipercapnia::getMotivoBajaString,
				RegistroOxiHipercapnia::setMotivoBaja);

		presioninspiracion.setWidth("50px");
		presioninspiracion.setMaxLength(50);
		binder.forField(presioninspiracion).withValidator(numero -> numero.matches("[0-9]*"), " sólo números")
				.withValidator(numero -> (Integer.parseInt(numero) > 0 && Integer.parseInt(numero) < 31), " de 1 a 31 ")
				.bind(RegistroOxiHipercapnia::getPresiespiracionString, RegistroOxiHipercapnia::setPresiinspiracion);

		presionesspiracion.setWidth("50px");
		presionesspiracion.setMaxLength(50);
		binder.forField(presionesspiracion).withValidator(numero -> numero.matches("[0-9]*"), " sólo números")
				.withValidator(numero -> (Integer.parseInt(numero) > 0 && Integer.parseInt(numero) < 31), " de 1 a 31 ")
				.bind(RegistroOxiHipercapnia::getPresiespiracionString, RegistroOxiHipercapnia::setPresiespiracion);

		binder.readBean(registroHipercapmia);

		filaCentrosServicio.addComponents(centroSelect, servicioSelect, modalidadesSelect);
		filaEapoyo1.addComponents(presioninspiracion, presionesspiracion);

		filaObservaciones.addComponents(observaciones);

		filaBaja.addComponents(fechaBaja, motivoBaja);

		this.contenedorCampos.addComponents(filaCentrosServicio, filaPruebaResultados, filaInicioTipoDuraciçon,
				filaEapoyo1, filaEapoyo2, filaObservaciones, filaBaja);
		this.addComponents(contenedorBotones, contenedorCampos);

		doActivaBotones();
	}

	@Override
	public void doActivaBotones() {
		super.doActivaBotones(registroHipercapmia);
		if (registroHipercapmia.getMotivoBajaString() != null && registroHipercapmia.getMotivoBajaString() != "") {
			LocalDate date = LocalDate.now();
			long dias = ChronoUnit.DAYS.between(registroHipercapmia.getFechaBaja(), date);
			if (dias > Integer.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
				new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
				contenedorCampos.setEnabled(false);
				borrar.setEnabled(false);
				grabar.setEnabled(false);
			}
		}
	}

	@Override
	public void cerrarClick() {
		this.removeAllComponents();
		this.setVisible(false);
		((PantallaProcesos) this.getParent().getParent().getParent()).refrescarClick();
	}

	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(this.getAyudaHtml()));
	}

	public void grabarClick() {
		if (doValidaFormulario() == true) {
			try {
				binder.writeBean(registroHipercapmia);
				if (!new RegistroDAO().grabaDatos(registroHipercapmia)) {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);

				} else {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
				}
				this.cerrarClick();
			} catch (ValidationException e) {
				new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
			}
		} else {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		}
	}

	@Override
	public void borrarClick() {
		ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, Constantes.CONFIRMACION_BORRADO_MENSAJE,
				Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 6169352858399108337L;

					public void onClose(ConfirmDialog dialog) {
						if (dialog.isConfirmed()) {
							borraElRegistro();
						}
					}
				});
		this.cerrarClick();
	}

	@Override
	public void borraElRegistro() {
		if (new RegistroDAO().doBorrarRegistro(registroHipercapmia)) {
			registroHipercapmia = null;
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_BORRADO);
		} else
			new NotificacionInfo(NotificacionInfo.FORMULARIO_ERROR_DATO_BORRADO);
		cerrarClick();
	}

	@Override
	public boolean doValidaFormulario() {
		boolean valido = true;
		if (!super.doValidaFormulario()) {
			valido = false;
		}

		return valido;
	}

	@Override
	public String getAyudaHtml() {
		String ayudaHtml = super.getAyudaHtml() + "<ul> " + "<li><b>?? :</b> ???.</li>" + "<li><b>....:</b>....</li>"
				+ "</ul>" + Ayudas.AYUDA_BOTONES_ABCA;
		return ayudaHtml;
	}
}
