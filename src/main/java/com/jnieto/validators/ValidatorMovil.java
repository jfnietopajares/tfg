package com.jnieto.validators;

import com.vaadin.data.validator.RegexpValidator;

@SuppressWarnings("serial")
public class ValidatorMovil extends RegexpValidator {

	private static final String PATTERN = "[0-9]{1,9}";

	public ValidatorMovil(String errorMessage) {
		 super(errorMessage, PATTERN, true);
	}
}
