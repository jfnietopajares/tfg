package com.jnieto.validators;

import com.vaadin.data.validator.RegexpValidator;

@SuppressWarnings("serial")
public class ValidatoDni extends RegexpValidator {
	private static final String PATTERN = "(\\d{1,8})([TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke])";
	public ValidatoDni(String errorMessage) {
		
		super(errorMessage, PATTERN, true);
	}
}
