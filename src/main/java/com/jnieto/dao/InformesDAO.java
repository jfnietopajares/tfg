package com.jnieto.dao;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.entity.Informe;
import com.jnieto.entity.Paciente;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Utilidades;

/**
 * The Class InformesDAO.
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class InformesDAO extends ConexionDAO implements InterfaceDAO {

	private static final Logger logger = LogManager.getLogger(InformesDAO.class);

	private Informe informe = null;

	private String sql;

	public InformesDAO() {
	}

	@Override
	public boolean getReferenciasExternas(Long id) {
		return false;
	}

	@Override
	public boolean grabaDatos(Object object) {
		this.informe = (Informe) object;
		boolean actualizado = false;
		if (informe.getId() == 0) {
			actualizado = this.insertaDatos(informe);
		} else {
			actualizado = this.actualizaDatos(informe);
		}
		return actualizado;
	}

	@Override
	public boolean actualizaDatos(Object mensajeparam) {
		Connection connection = null;
		informe = (Informe) mensajeparam;
		boolean actualizado = false;
		try {
			connection = super.getConexionBBDD();
			sql = " UPDATE informes SET  descripcion=?,  fecha=? , hora =? WHERE id=? ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, informe.getDescripcion());
			statement.setLong(2, informe.getFecha());
			statement.setLong(3, informe.getHora());
			statement.setLong(4, informe.getId());
			actualizado = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(" UPDATE informes SET  descripcion= '" + informe.getDescripcion() + "',  fecha="
					+ informe.getFecha() + " , hora =" + informe.getHora() + " WHERE id= " + informe.getId());
		} catch (SQLException e) {
			logger.error(" UPDATE informes SET  descripcion= '" + informe.getDescripcion() + "',  fecha="
					+ informe.getFecha() + " , hora =" + informe.getHora() + " WHERE id= " + informe.getId());
			logger.error(CentroDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return actualizado;
	}

	@Override
	public boolean insertaDatos(Object mensajeparam) {
		Connection connection = null;
		informe = (Informe) mensajeparam;
		Long id = null;
		boolean insertado = false;
		FileInputStream is = null;

		try {
			connection = super.getConexionBBDD();
			id = new UtilidadesDAO().getSiguienteId("informes");
			informe.setId(id);
			is = new FileInputStream(informe.getFicheroInformeFile());
			sql = " INSERT INTO informes (id,centro, estado ,tipoxml ,tipobin,canal,flag,version,ultimoguardado"
					+ ",bloqueado,ambito ,descripcion,fecha,hora,servicio,docubin,paciente,tipo_documento,problema)"
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)  ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, informe.getId());
			statement.setLong(2, informe.getCentro().getId());
			statement.setInt(3, informe.getEstado());
			statement.setInt(4, informe.getTipoxml());
			statement.setInt(5, informe.getTipobin());
			statement.setLong(6, informe.getCana());
			statement.setInt(7, informe.getFlag());
			statement.setInt(8, informe.getVersion());
			statement.setLong(9, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
			statement.setInt(10, informe.getBloqueado());
			statement.setLong(11, informe.getAmbito());
			statement.setString(12, informe.getDescripcion());
			statement.setLong(13, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
			statement.setLong(14, Utilidades.getHoraNumeroAcual());
			statement.setLong(15, informe.getServicio().getId());
			statement.setBlob(16, is);
			statement.setLong(17, informe.getPaciente().getId());
			statement.setLong(18, informe.getTipo_documento());
			statement.setLong(19, informe.getProblema().getId());
			insertado = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(" INSERT INTO informes (id,centro, estado ,tipoxml ,tipobin,canal,flag,version,ultimoguardado"
					+ ",bloqueado,ambito ,descripcion,fecha,hora,servicio,docubin,paciente,tipo_documento,problema)"
					+ " VALUES (" + informe.getId() + "," + informe.getEstado() + "," + informe.getTipoxml() + ","
					+ informe.getTipobin() + "," + informe.getCana() + "," + informe.getFlag() + ","
					+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + "," + informe.getBloqueado() + ","
					+ informe.getAmbito() + ",'" + informe.getDescripcion() + "',"
					+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + "," + Utilidades.getHoraNumeroAcual()
					+ "," + is + "," + informe.getPaciente().getId() + "," + informe.getTipo_documento() + ","
					+ informe.getProblema().getId() + ")  ");
		} catch (SQLException e) {
			logger.error(" INSERT INTO informes (id,centro, estado ,tipoxml ,tipobin,canal,flag,version,ultimoguardado"
					+ ",bloqueado,ambito ,descripcion,fecha,hora,servicio,docubin,paciente,tipo_documento,problema)"
					+ " VALUES (" + informe.getId() + "," + informe.getEstado() + "," + informe.getTipoxml() + ","
					+ informe.getTipobin() + "," + informe.getCana() + "," + informe.getFlag() + ","
					+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + "," + informe.getBloqueado() + ","
					+ informe.getAmbito() + ",'" + informe.getDescripcion() + "',"
					+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + "," + Utilidades.getHoraNumeroAcual()
					+ "," + is + "," + informe.getPaciente().getId() + "," + informe.getTipo_documento() + ","
					+ informe.getProblema().getId() + ")  ");
			logger.error(CentroDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return insertado;
	}

	@Override
	public Object getRegistroResulset(ResultSet rs) {
		return null;
	}

	@Override
	public Object getRegistroId(Long id) {
		return null;
	}

	@Override
	public String getSqlWhere(String cadena) {
		return null;
	}

	@Override
	public boolean borraDatos(Object objeto) {
		return false;
	}

	/**
	 * Gets the id informe des tipo. Recupera el id del informe para ese paciente,
	 * descripcion y tipo de documento. Este método se usa para no repetir informes
	 * en los procesos que tienen que ser únicos; por ejemplo en el de screening de
	 * mama donde el informe que hace la empresa es único
	 *
	 * @param paciente    un paciente
	 * @param descrString la descripcion del informe
	 * @param tipo        el tipo de documento
	 * @return el id del infome si existe , 0 si no existe.
	 */
	public Long getIdInformeDesTipo(Paciente paciente, String descrString, Long tipo, Long proceso) {
		Connection connection = null;
		Long id = new Long(0);
		informe = null;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT id FROM informes  WHERE paciente=?  AND trim(descripcion)= '" + descrString.trim()
					+ "' AND tipo_documento=?  AND problema=?";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, paciente.getId());
			statement.setLong(2, tipo);
			statement.setLong(3, proceso);
			ResultSet resulSet = statement.executeQuery();
			if (resulSet.next()) {
				id = resulSet.getLong("id");
			}
			statement.close();
			logger.debug(
					sql = "SELECT id FROM informes  WHERE paciente=" + paciente.getId() + "  AND trim(descripcion)= '"
							+ descrString.trim() + "' AND tipo_documento=" + tipo + "  AND problema=" + proceso + "");
		} catch (SQLException e) {
			logger.error(
					sql = "SELECT id FROM informes  WHERE paciente=" + paciente.getId() + "  AND trim(descripcion)= '"
							+ descrString.trim() + "' AND tipo_documento=" + tipo + "  AND problema=" + proceso + "");
			logger.error(CentroDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return id;
	}

	public Long getIdInformeProceso(Paciente paciente, Long proceso) {

		Connection connection = null;
		Long id = new Long(0);
		if (paciente == null)
			return id;
		informe = null;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT id FROM informes  WHERE paciente=?  AND problema=?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, paciente.getId());
			statement.setLong(2, proceso);
			ResultSet resulSet = statement.executeQuery();
			if (resulSet.next()) {
				id = resulSet.getLong("id");
			}
			statement.close();
			logger.debug(
					"SELECT id FROM informes  WHERE paciente=" + paciente.getId() + "  AND problema=" + proceso + "");
		} catch (SQLException e) {
			logger.error(
					"SELECT id FROM informes  WHERE paciente=" + paciente.getId() + "  AND problema=" + proceso + "");
			logger.error(CentroDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return id;
	}

	public boolean actualizaFichero(Object mensajeparam) {
		Connection connection = null;
		informe = (Informe) mensajeparam;
		boolean actualizado = false;
		FileInputStream is = null;
		try {
			connection = super.getConexionBBDD();
			is = new FileInputStream(informe.getFicheroInformeFile());
			sql = " UPDATE informes SET  docubin=?,  fecha=? , hora =? WHERE id=? ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setBlob(1, is);
			statement.setLong(2, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
			statement.setLong(3, Utilidades.getHoraNumeroAcual());
			statement.setLong(4, informe.getId());
			actualizado = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(sql = " UPDATE informes SET  docubin=" + is + ",  fecha="
					+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + " , hora ="
					+ Utilidades.getHoraNumeroAcual() + " WHERE id=" + informe.getId());
			actualizado = true;
		} catch (SQLException e) {
			logger.error(sql = " UPDATE informes SET  docubin=" + is + ",  fecha="
					+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + " , hora ="
					+ Utilidades.getHoraNumeroAcual() + " WHERE id=" + informe.getId());
			logger.error(CentroDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return actualizado;
	}

	public java.sql.Blob getPdfId(Long id) {
		Connection connection = null;
		java.sql.Blob pdf = null;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT docubin  FROM informes  WHERE id=? ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, id);
			ResultSet resulSet = statement.executeQuery();
			if (resulSet.next()) {
				pdf = resulSet.getBlob("docubin");
			}
			statement.close();
			logger.debug("SELECT docubin  FROM informes  WHERE id= " + id);
		} catch (SQLException e) {
			logger.error("SELECT docubin  FROM informes  WHERE id= " + id);
			logger.error(CentroDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return pdf;
	}
}
