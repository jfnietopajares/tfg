
package com.jnieto.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.entity.Campos_r;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroColon;
import com.jnieto.entity.RegistroMama;
import com.jnieto.entity.RegistroO2Incidencia;
import com.jnieto.entity.RegistroOxi;
import com.jnieto.entity.RegistroOxiAerosol;
import com.jnieto.entity.RegistroOxiDomi;
import com.jnieto.entity.RegistroOxiHipercapnia;
import com.jnieto.entity.RegistroOxiMapnea;
import com.jnieto.entity.Usuario;
import com.jnieto.entity.Variable;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;
import com.vaadin.server.VaadinSession;

/**
 * The Class RegistroDAO.
 * 
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class RegistroDAO extends ConexionDAO implements InterfaceDAO {

	protected String sql;

	private static final Logger logger = LogManager.getLogger(RegistroDAO.class);

	/**
	 * Instantiates a new registro DAO.
	 */
	public RegistroDAO() {
		super();
	}

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	public Connection getConnection() {
		return super.getConexionBBDD();
	}

	/**
	 * Graba datos.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean grabaDatos(Registro registro) {
		boolean actualizado = false;
		if (registro.getId() == 0) {
			actualizado = this.insertaDatos(registro);
		} else {
			actualizado = this.actualizaDatos(registro);
		}
		return actualizado;
	}

	/**
	 * Actualiza datos.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean actualizaDatos(Registro registro) {
		boolean actualizado = false;
		try {
			// actualizado = doActualizaRegistro(registro);
			doActualizaEstadoRegistro(registro);
			actualizado = this.insertaDatos(registro);
			if (actualizado == true) {
				// poner en campos_r actualizado=5
				if (this.doCambiaEstadoCamposR(registro) == true) {
					doInsertaCamposR(registro);
				} // campos_R estado=5
			} // ok de insercion de registro
		} catch (Exception e) {
			logger.error(NotificacionInfo.MSG_EXCEPTION, e);
		}
		return actualizado;
	}

	/**
	 * Inserta datos.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean insertaDatos(Registro registro) {
		boolean insertado = false;
		Long idRegistro;
		try {
			idRegistro = new UtilidadesDAO().getSiguienteId("registros");
			registro.setId(idRegistro);
			insertado = doInsertaRegistro(registro);
			if (insertado == true) {
				// campos comunes a los registros
				if (!doInsertaCamposR(registro))
					return false;
			} // ok de insercion de registro
		} catch (Exception e) {
			logger.error(NotificacionInfo.MSG_EXCEPTION, e);
		}
		return insertado;
	}

	/**
	 * Haz inserta registro.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean doInsertaRegistro(Registro registro) {
		Connection connection = null;
		boolean rowInserted = false;
		PreparedStatement statement;
		try {
			connection = super.getConexionBBDD();
			if (registro.getEpisodio() == null) {
				sql = " INSERT INTO registros  (id, descripcion, paciente ,  centro,fecha,hora,estado,servicio,userid"
						+ ",plantilla_editor,canal,tiporegistro,problema, useridredactor)  "
						+ "            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)  ";
				statement = connection.prepareStatement(sql);
				statement.setLong(1, registro.getId());
				statement.setString(2, registro.getDescripcion());
				statement.setLong(3, registro.getPaciente().getId());
				statement.setLong(4, registro.getCentro().getId());
				statement.setLong(5, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
				statement.setLong(6, Utilidades.getHoraNumeroAcual());
				statement.setInt(7, registro.getEstado());
				statement.setLong(8, registro.getServicio().getId());
				statement.setString(9, registro.getUserid().getUserid());
				statement.setLong(10, registro.getPlantilla_editor());
				statement.setLong(11, registro.getCanal());
				statement.setLong(12, registro.getTiporegistro());
				statement.setLong(13, registro.getProblema().getId());
				statement.setString(14, registro.getUseridredactor().getUserid());
			} else {
				sql = " INSERT INTO registros  (id, descripcion, paciente ,  centro,fecha,hora,estado,episodio,servicio,userid"
						+ ",plantilla_editor,canal,tiporegistro,problema,useridredactor )  "
						+ "            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)  ";
				statement = connection.prepareStatement(sql);
				statement.setLong(1, registro.getId());
				statement.setString(2, registro.getDescripcion());
				statement.setLong(3, registro.getPaciente().getId());
				statement.setLong(4, registro.getCentro().getId());
				statement.setLong(5, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
				statement.setLong(6, Utilidades.getHoraNumeroAcual());
				statement.setInt(7, registro.getEstado());
				statement.setLong(8, registro.getEpisodio()); //
				statement.setLong(9, registro.getServicio().getId());
				statement.setString(10, registro.getUserid().getUserid());
				statement.setLong(11, registro.getPlantilla_editor());
				statement.setLong(12, registro.getCanal());
				statement.setLong(13, registro.getTiporegistro());
				statement.setLong(14, registro.getProblema().getId());
				statement.setString(15, registro.getUseridredactor().getUserid());
			}
			rowInserted = statement.executeUpdate() > 0;
			statement.close();
			logger.debug("INSERT INTO registros  (id, descripcion, paciente ,  centro,fecha,hora,estado,servicio,userid"
					+ ",plantilla_editor,canal,tiporegistro,problema, useridredactor)  " + "            VALUES ("
					+ registro.getId() + ",'" + registro.getDescripcion() + "'," + registro.getPaciente().getId() + ","
					+ registro.getCentro().getId() + "," + Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now())
					+ "," + Utilidades.getHoraNumeroAcual() + "," + registro.getEstado() + "," + registro.getEpisodio()
					+ "," + registro.getServicio().getId() + ",'" + registro.getUserid().getUserid() + "',"
					+ registro.getPlantilla_editor() + "," + registro.getCanal() + "," + registro.getTiporegistro()
					+ "," + registro.getProblema().getId() + ")  ");
		} catch (SQLException e) {
			logger.error("INSERT INTO registros  (id, descripcion, paciente ,  centro,fecha,hora,estado,servicio,userid"
					+ ",plantilla_editor,canal,tiporegistro,problema, useridredactor)  " + "            VALUES ("
					+ registro.getId() + ",'" + registro.getDescripcion() + "'," + registro.getPaciente().getId() + ","
					+ registro.getCentro().getId() + "," + Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now())
					+ "," + Utilidades.getHoraNumeroAcual() + "," + registro.getEstado() + "," + registro.getEpisodio()
					+ "," + registro.getServicio().getId() + ",'" + registro.getUserid().getUserid() + "',"
					+ registro.getPlantilla_editor() + "," + registro.getCanal() + "," + registro.getTiporegistro()
					+ "," + registro.getProblema().getId() + ")  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return rowInserted;
	}

	/**
	 * Haz borrar registro.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean doBorrarRegistro(Registro registro) {
		Connection connection = null;
		boolean rowInserted = false;
		try {
			connection = super.getConexionBBDD();
			sql = " DELETE FROM campos_r WHERE registro=?   ";
			logger.info(" Borra campos_r  " + sql);
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, registro.getId());
			rowInserted = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(" DELETE FROM campos_r WHERE registro= " + registro.getId());
			sql = " DELETE FROM registros WHERE id =?   ";
			PreparedStatement statement1 = connection.prepareStatement(sql);
			statement1.setLong(1, registro.getId());
			rowInserted = statement1.executeUpdate() > 0;
			statement.close();
			logger.debug(" DELETE FROM registros WHERE id =?   " + registro.getId());
		} catch (SQLException e) {
			logger.error(" DELETE FROM campos_r WHERE registro= " + registro.getId());
			logger.error(" DELETE FROM registros WHERE id =?   " + registro.getId());
			logger.error(ConexionDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return rowInserted;
	}

	/**
	 * Haz inserta campos R.
	 *
	 * @param registro the registro
	 * @param variable the variable
	 * @param valor    the valor
	 * @return true, if successful
	 */
	public boolean doInsertaCamposR(Registro registro, Variable variable, String valor) {
		Connection connection = null;
		boolean insertado = false;
		Long idCamposr = null;
		try {

			idCamposr = new UtilidadesDAO().getSiguienteId("campos_r");
			connection = super.getConexionBBDD();
			sql = " INSERT INTO campos_r   (id, registro,item,userid,fecha,hora,descripcion,orden,estado,dato,canal )  "
					+ "            VALUES (?,?,?,?,?,?,?,?,?,?,?)  ";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, idCamposr);
			statement.setLong(2, registro.getId());
			statement.setLong(3, variable.getItem());
			statement.setString(4, registro.getUserid().getUserid());
			statement.setLong(5, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
			statement.setLong(6, Utilidades.getHoraNumeroAcual());
			statement.setString(7, variable.getDescripcion());
			statement.setLong(8, Registro.ORDEN_DEFECTO); //
			statement.setLong(9, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setString(10, valor);
			statement.setLong(11, Registro.CANAL_DEFECTO);
			insertado = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(
					" INSERT INTO campos_r   (id, registro,item,userid,fecha,hora,descripcion,orden,estado,dato,canal )  "
							+ "            VALUES (" + idCamposr + "," + registro.getId() + "," + variable.getItem()
							+ ",'" + registro.getUserid().getUserid() + "',"
							+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + ","
							+ Utilidades.getHoraNumeroAcual() + ",'" + variable.getDescripcion() + "',"
							+ Registro.ORDEN_DEFECTO + "," + Registro.VAR_RESGISTRO_ESTADO_NORMAL + ",'" + valor + "',"
							+ Registro.CANAL_DEFECTO + ")  ");
		} catch (SQLException e) {
			logger.error(
					" INSERT INTO campos_r   (id, registro,item,userid,fecha,hora,descripcion,orden,estado,dato,canal )  "
							+ "            VALUES (" + idCamposr + "," + registro.getId() + "," + variable.getItem()
							+ ",'" + registro.getUserid().getUserid() + "',"
							+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + ","
							+ Utilidades.getHoraNumeroAcual() + ",'" + variable.getDescripcion() + "',"
							+ Registro.ORDEN_DEFECTO + "," + Registro.VAR_RESGISTRO_ESTADO_NORMAL + ",'" + valor + "',"
							+ Registro.CANAL_DEFECTO + ")  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return insertado;
	}

	/**
	 * Haz actualiza registro.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean doActualizaRegistro(Registro registro) {
		Connection connection = null;
		boolean actualizado = false;
		try {
			connection = super.getConexionBBDD();
			sql = " UPDATE   registros  SET  centro=?,servicio=?,userid=? , fecha=? , hora=?  WHERE id =?   ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, registro.getCentro().getId());
			statement.setLong(2, registro.getServicio().getId());
			statement.setString(3, registro.getUserid().getUserid());
			statement.setLong(4, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
			statement.setLong(5, Utilidades.getHoraNumeroAcual());
			statement.setLong(6, registro.getId());
			actualizado = statement.executeUpdate() > 0;
			statement.close();

		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return actualizado;
	}

	public boolean doActualizaEstadoRegistro(Registro registro) {
		Connection connection = null;
		boolean actualizado = false;
		try {
			connection = super.getConexionBBDD();
			sql = " UPDATE   registros  SET  estado=? WHERE id =?   ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(2, registro.getId());
			statement.setLong(1, Registro.VAR_RESGISTRO_ESTADO_SUSTITUIDO);
			actualizado = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(" UPDATE   registros  SET  estado=" + Registro.VAR_RESGISTRO_ESTADO_SUSTITUIDO + " WHERE id = "
					+ registro.getId());
		} catch (SQLException e) {
			logger.error(" UPDATE   registros  SET  estado=" + Registro.VAR_RESGISTRO_ESTADO_SUSTITUIDO + " WHERE id = "
					+ registro.getId());
			logger.error(ConexionDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return actualizado;
	}

	/**
	 * Haz cambia estado campos R.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean doCambiaEstadoCamposR(Registro registro) {
		Connection connection = null;
		boolean rowInserted = false;
		try {
			connection = super.getConexionBBDD();
			sql = " UPDATE   campos_r SET estado=? WHERE registro=? AND estado=?  ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, Registro.VAR_RESGISTRO_ESTADO_SUSTITUIDO);
			statement.setLong(2, registro.getId());
			statement.setInt(3, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			rowInserted = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(" UPDATE   campos_r SET estado= " + Registro.VAR_RESGISTRO_ESTADO_SUSTITUIDO
					+ " WHERE registro=" + registro.getId() + " AND estado=  " + Registro.VAR_RESGISTRO_ESTADO_NORMAL);
		} catch (SQLException e) {
			logger.error(" UPDATE   campos_r SET estado= " + Registro.VAR_RESGISTRO_ESTADO_SUSTITUIDO
					+ " WHERE registro=" + registro.getId() + " AND estado=  " + Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return rowInserted;
	}

	/**
	 * Haz inserta campos R.
	 *
	 * @param registro the registro. Recibe como parámetro un objeto tipo registro.
	 *                 Todos las clases tipo registro extienden de Registro y puede
	 *                 ser un subtipo de mama, colon, oxigenoterapia, incidencias.
	 * 
	 *                 Los objetos Bean de estos tipos tienen definido un método que
	 *                 comienza con el profijo getVariableNombreAtributo que
	 *                 devuelve un dato tipo Variable correspondiente a ese
	 *                 atributo.
	 * 
	 *                 Este método recorre los métodos del objeto y los que su
	 *                 prefijo coinciden con el patrón "getVariable", invoca al
	 *                 método y hace casting al tipo Variable del objeto devuelto.
	 * 
	 * @return true, if successful
	 */
	public boolean doInsertaCamposR(Object registro) {

		boolean insertado = true;
		Method[] metodos = registro.getClass().getMethods();
		for (Method m : metodos) {
			if (m.getName().length() > 10) {
				if (m.getName().substring(0, 11).equals("getVariable")) {
					try {
						if (m.invoke(registro, null) instanceof Variable) {
							Variable variable = (Variable) m.invoke(registro, null);
							if (variable != null) {
								if (variable.getValor() != null) {
									if (!variable.getValor().isEmpty()) {
										insertado = doInsertaCamposRBBDD((Registro) registro, variable);
									}
								}
							}
						}
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
					} catch (Exception e) {
						logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
					}
				}
			}
		}
		return insertado;
	}

	/**
	 * Haz inserta campos RBBDD.
	 *
	 * @param registro the registro
	 * @param variable the variable
	 * @return true, if successful
	 */
	public boolean doInsertaCamposRBBDD(Registro registro, Variable variable) {
		Connection connection = null;
		boolean insertado = false;
		Long idCamposr = null;
		try {

			idCamposr = new UtilidadesDAO().getSiguienteId("campos_r");
			connection = super.getConexionBBDD();
			sql = " INSERT INTO campos_r   (id, registro,item,userid,fecha,hora,descripcion,orden,estado,dato,canal )  "
					+ "            VALUES (?,?,?,?,?,?,?,?,?,?,?)  ";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, idCamposr);
			statement.setLong(2, registro.getId());
			statement.setLong(3, variable.getItem());
			statement.setString(4,
					((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME)).getUserid());
			statement.setLong(5, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
			statement.setLong(6, Utilidades.getHoraNumeroAcual());
			statement.setString(7, variable.getDescripcion());
			statement.setLong(8, Registro.ORDEN_DEFECTO); //
			statement.setLong(9, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setString(10, variable.getValor());
			statement.setLong(11, Registro.CANAL_DEFECTO);
			insertado = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(
					" INSERT INTO campos_r   (id, registro,item,userid,fecha,hora,descripcion,orden,estado,dato,canal )  "
							+ "            VALUES (" + idCamposr + "," + registro.getId() + "," + variable.getItem()
							+ ",'"
							+ ((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME))
									.getUserid()
							+ "'," + Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + ","
							+ Utilidades.getHoraNumeroAcual() + ",'" + variable.getDescripcion() + "',"
							+ Registro.ORDEN_DEFECTO + "," + Registro.VAR_RESGISTRO_ESTADO_NORMAL + ",'"
							+ variable.getValor() + "'," + Registro.CANAL_DEFECTO + ")  ");
		} catch (SQLException e) {
			logger.error(
					" INSERT INTO campos_r   (id, registro,item,userid,fecha,hora,descripcion,orden,estado,dato,canal )  "
							+ "            VALUES (" + idCamposr + "," + registro.getId() + "," + variable.getItem()
							+ ",'"
							+ ((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME))
									.getUserid()
							+ "'," + Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + ","
							+ Utilidades.getHoraNumeroAcual() + ",'" + variable.getDescripcion() + "',"
							+ Registro.ORDEN_DEFECTO + "," + Registro.VAR_RESGISTRO_ESTADO_NORMAL + ",'"
							+ variable.getValor() + "'," + Registro.CANAL_DEFECTO + ")  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return insertado;
	}

	/**
	 * Gets the registro por id.
	 *
	 * @param id the id
	 * @return the registro por id
	 */
	public Registro getRegistroPorId(Long id) {
		Connection connection = null;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT r.* FROM registros r  WHERE r.id = ?  AND r.estado=?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, id);
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			ResultSet resuSulset = statement.executeQuery();
			if (resuSulset.next()) {
				return getRegistroResulset(resuSulset);
			}
			statement.close();
			logger.debug("SELECT r.* FROM registros r  WHERE r.id = " + id + "  AND r.estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL);
		} catch (SQLException e) {
			logger.error("SELECT r.* FROM registros r  WHERE r.id = " + id + "  AND r.estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return null;
	}

	/**
	 * Gets the registro resulset.
	 *
	 * @param resuSulset the resu sulset
	 * @return the registro resulset
	 */

	public Registro getRegistroResulset(ResultSet resuSulset) {
		Long id;
		try {
			id = resuSulset.getLong("plantilla_editor");

			if (id.equals(RegistroMama.PLANTILLLA_EDITOR_REGISTROMAMA)) {
				RegistroMama registro = new RegistroMama();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid")));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema")));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				return registro;
			} else if (id.equals(RegistroColon.PLANTILLLA_EDITOR_REGISTROCOLON)) {
				RegistroColon registro = new RegistroColon();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid")));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema")));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				return registro;
			} else if (id.equals(RegistroOxiAerosol.PLANTILLLA_EDITOR_REGISTROAEROSOL)) {
				RegistroOxiAerosol registro = new RegistroOxiAerosol();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid")));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema")));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				return registro;
			} else if (id.equals(RegistroOxiMapnea.PLANTILLLA_EDITOR_REGISTROMAPNEA)) {
				RegistroOxiMapnea registro = new RegistroOxiMapnea();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid")));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema")));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				return registro;
			} else if (id.equals(RegistroOxiDomi.PLANTILLLA_EDITOR_REGISTRODOMI)) {
				RegistroOxiDomi registro = new RegistroOxiDomi();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid")));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema")));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				return registro;
			} else if (id.equals(RegistroOxiHipercapnia.PLANTILLLA_EDITOR_REGISTROEQPOYO)) {
				RegistroOxiHipercapnia registro = new RegistroOxiHipercapnia();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid")));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema")));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				RegistroOxiHipercapnia registroOxiEapoyo = new RegistroOxiHipercapmiaDAO().getValoresCamposR(registro);
				return registroOxiEapoyo;
			} else if (id.equals(RegistroO2Incidencia.PLANTILLLA_EDITOR_REGISTROINCIDENCIA)) {
				RegistroO2Incidencia registro = new RegistroO2Incidencia();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid")));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema")));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				return registro;
			}
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		return null;
	}

	/**
	 * Gets the registro paciente proceso.
	 *
	 * @param paciente         the paciente
	 * @param proceso          the proceso
	 * @param plantilla_editor the plantilla editor
	 * @return the registro paciente proceso
	 */
	public Long getRegistroPacienteProceso(Paciente paciente, Proceso proceso, Long plantilla_editor) {
		Connection connection = null;
		Long id = new Long(0);
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT id FROM registros  WHERE paciente = ?  AND estado=? AND problema= ? AND plantilla_editor=?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, paciente.getId());
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(3, proceso.getId());
			statement.setLong(4, plantilla_editor);
			ResultSet resuSulset = statement.executeQuery();
			if (resuSulset.next()) {
				id = resuSulset.getLong("id");
			}
			statement.close();
			logger.debug("SELECT id FROM registros  WHERE paciente = " + paciente.getId() + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND problema= " + proceso.getId()
					+ " AND plantilla_editor=plantilla_editor");
		} catch (SQLException e) {
			logger.error("SELECT id FROM registros  WHERE paciente = " + paciente.getId() + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND problema= " + proceso.getId()
					+ " AND plantilla_editor=plantilla_editor");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return id;
	}

	/**
	 * Gets the registro paciente proceso O 2. Recupera un registro de tipo
	 * oxigenoterapia del subtipo plantilla_editor que no este cerrado, es decir que
	 * no tenga valor en campos_r la variable fecha fin
	 *
	 * @param paciente         the paciente
	 * @param proceso          the proceso
	 * @param plantilla_editor the plantilla editor
	 * @return the registro paciente proceso O 2
	 */
	public Long getRegistroPacienteProcesoO2(Paciente paciente, Proceso proceso, Long plantilla_editor) {
		Connection connection = null;
		Long id = new Long(0);
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT r.id ,c.dato FROM registros  r   "
					+ " LEFT JOIN campos_r c ON r.id=c.registro AND c.estado=? AND c.item=?"
					+ "	WHERE r.paciente = ?  AND r.estado=? AND r.problema= ? " + " AND plantilla_editor=?"
					+ " AND ( dato=0 OR dato  IS null )";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(2, RegistroOxi.ITEM_FECHAFIN);
			statement.setLong(3, paciente.getId());
			statement.setInt(4, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(5, proceso.getId());
			statement.setLong(6, plantilla_editor);
			ResultSet resuSulset = statement.executeQuery();
			if (resuSulset.next()) {
				id = resuSulset.getLong("id");
			}
			statement.close();
			logger.debug("SELECT r.id ,c.dato FROM registros  r   "
					+ " LEFT JOIN campos_r c ON r.id=c.registro AND c.estado=" + Registro.VAR_RESGISTRO_ESTADO_NORMAL
					+ " AND c.item=" + RegistroOxi.ITEM_FECHAFIN + "" + "	WHERE r.paciente = " + paciente.getId()
					+ "  AND r.estado=? AND r.problema= " + proceso.getId() + " " + " AND plantilla_editor="
					+ plantilla_editor + "" + " AND ( dato=0 OR dato  IS null )");
		} catch (SQLException e) {
			logger.error("SELECT r.id ,c.dato FROM registros  r   "
					+ " LEFT JOIN campos_r c ON r.id=c.registro AND c.estado=" + Registro.VAR_RESGISTRO_ESTADO_NORMAL
					+ " AND c.item=" + RegistroOxi.ITEM_FECHAFIN + "" + "	WHERE r.paciente = " + paciente.getId()
					+ "  AND r.estado=? AND r.problema= " + proceso.getId() + " " + " AND plantilla_editor="
					+ plantilla_editor + "" + " AND ( dato=0 OR dato  IS null )");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return id;
	}

	/**
	 * Gets the lista campos registro.
	 *
	 * @param id the id
	 * @return the lista campos registro
	 */
	public ArrayList<Campos_r> getListaCamposRegistro(Long id) {
		Connection connection = null;
		ArrayList<Campos_r> listaCampos = new ArrayList<>();
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT *  FROM campos_r  WHERE registro = ?  AND estado=?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, id);
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			ResultSet resuSulset = statement.executeQuery();
			while (resuSulset.next()) {
				Campos_r campo_r = getCamposRResulSet(resuSulset);
				listaCampos.add(campo_r);
			}
			statement.close();
			logger.debug("SELECT *  FROM campos_r  WHERE registro = " + id + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL);
		} catch (SQLException e) {
			logger.error("SELECT *  FROM campos_r  WHERE registro = " + id + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaCampos;
	}

	/**
	 * Gets the registro resul set.
	 *
	 * @param rsRegistro the rs registro
	 * @return the registro resul set
	 */
	public Registro getRegistroResulSet(ResultSet rsRegistro) {

		Long id = null;
		try {
			id = rsRegistro.getLong("plantilla_editor");
			if (id.equals(RegistroMama.PLANTILLLA_EDITOR_REGISTROMAMA)) {
				RegistroMama registro = new RegistroMama();
				registro.setId(rsRegistro.getLong("id"));
				registro.setDescripcion(rsRegistro.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(rsRegistro.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(rsRegistro.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(rsRegistro.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(rsRegistro.getLong("fecha")));
				registro.setHora(rsRegistro.getLong("hora"));
				registro.setEstado(rsRegistro.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(rsRegistro.getString("userid")));
				registro.setProblema(new ProcesoDAO().getRegistroId(rsRegistro.getLong("problema")));
				registro.setUseridredactor(new UsuarioDAO().getUsuarioUserid(rsRegistro.getString("useridredactor")));
				registro.setUseridtranscriptor(
						new UsuarioDAO().getUsuarioUserid(rsRegistro.getString("useridtranscriptor")));
				registro.setTiporegistro(rsRegistro.getLong("tiporegistro"));
				registro.setCanal(rsRegistro.getLong("canal"));

				return registro;
			}

		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		return null;
	}

	/**
	 * Gets the campos R resul set.
	 *
	 * @param rsRegistro the rs registro
	 * @return the campos R resul set
	 */
	public Campos_r getCamposRResulSet(ResultSet rsRegistro) {
		Campos_r campo = new Campos_r();

		try {

			campo.setId(rsRegistro.getLong("id"));
			campo.setRegistro(rsRegistro.getLong("registro"));
			campo.setItem(rsRegistro.getLong("item"));
			campo.setUserid(rsRegistro.getString("userid"));
			campo.setFecha(rsRegistro.getLong("fecha"));
			campo.setHora(rsRegistro.getLong("hora"));
			campo.setDescripcion(rsRegistro.getString("descripcion"));
			campo.setOrden(rsRegistro.getInt("orden"));
			campo.setUnidades(rsRegistro.getString("unidades"));
			campo.setFlags(rsRegistro.getString("flags"));
			campo.setCodigo(rsRegistro.getString("codigo"));
			campo.setTipo_codigo(rsRegistro.getString("tipo_codigo"));
			campo.setEstado(rsRegistro.getInt("estado"));
			campo.setTipobin(rsRegistro.getInt("tipobin"));
			campo.setDato(rsRegistro.getString("dato"));
			campo.setReferencias(rsRegistro.getString("referencias"));
			campo.setDatobin(rsRegistro.getString("datobin"));
			campo.setLateralidad(rsRegistro.getLong("lateralidad"));
			campo.setCanal(rsRegistro.getLong("canal"));

		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}

		return campo;
	}

	/**
	 * Gets the lista registros.
	 *
	 * @param desde            the desde
	 * @param hasta            the hasta
	 * @param plantilla_editor the plantilla editor
	 * @return the lista registros
	 */
	public ArrayList<Registro> getListaRegistros(LocalDate desde, LocalDate hasta, Long plantilla_editor) {
		Connection connection = null;
		ArrayList<Registro> listaRegistros = new ArrayList<>();
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT * FROM registros WHERE  fecha>=? AND fecha<=? AND plantilla_editor=?  and estado = ? ORDER BY  fecha  ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, Utilidades.getFechaNumeroyyymmddDefecha(desde));
			statement.setLong(2, Utilidades.getFechaNumeroyyymmddDefecha(hasta));
			statement.setLong(3, plantilla_editor);
			statement.setInt(4, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			ResultSet resulSet = statement.executeQuery();
			while (resulSet.next()) {
				listaRegistros.add(getRegistroResulset(resulSet));
			}
			statement.close();
			logger.debug("SELECT * FROM registros WHERE  fecha>=" + Utilidades.getFechaNumeroyyymmddDefecha(desde)
					+ " AND fecha<=" + Utilidades.getFechaNumeroyyymmddDefecha(hasta) + " AND plantilla_editor="
					+ plantilla_editor + "  and estado = " + Registro.VAR_RESGISTRO_ESTADO_NORMAL
					+ " ORDER BY  fecha  ");
		} catch (SQLException e) {
			logger.error("SELECT * FROM registros WHERE  fecha>=" + Utilidades.getFechaNumeroyyymmddDefecha(desde)
					+ " AND fecha<=" + Utilidades.getFechaNumeroyyymmddDefecha(hasta) + " AND plantilla_editor="
					+ plantilla_editor + "  and estado = " + Registro.VAR_RESGISTRO_ESTADO_NORMAL
					+ " ORDER BY  fecha  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaRegistros;
	}

	/**
	 * Gets the lista registros.
	 *
	 * @param subambito the subambito
	 * @return the lista registros
	 */
	public ArrayList<Registro> getListaRegistros(Long idPaciente, Long subambito) {
		Connection connection = null;
		ArrayList<Registro> listaRegistros = new ArrayList<>();

		try {
			connection = super.getConexionBBDD();
			if (subambito.equals(new Long(0)))
				sql = "SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
						+ "   WHERE   r.estado = ? AND r.paciente=?  " + "  ORDER BY  fecha DESC, hora DESC  ";
			else {
				sql = "SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
						+ "   WHERE   r.estado = ? AND r.paciente=?  " + " AND  p.subambito=" + subambito
						+ " ORDER BY  fecha DESC, hora DESC  ";
			}
			PreparedStatement statement = connection.prepareStatement(sql);

			statement.setInt(1, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(2, idPaciente);
			ResultSet resulSet = statement.executeQuery();
			while (resulSet.next()) {
				listaRegistros.add(getRegistroResulset(resulSet));
			}
			statement.close();
			logger.debug("SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   r.estado = " + Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente=" + idPaciente
					+ " " + "ORDER BY  fecha DESC, hora DESC  ");
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaRegistros;
	}

	public ArrayList<Registro> getListaRegistros(Long subambito, Paciente paciente) {
		Connection connection = null;
		ArrayList<Registro> listaRegistros = new ArrayList<>();

		try {
			connection = super.getConexionBBDD();
			sql = "SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   p.subambito =? AND  r.estado = ? AND r.paciente=? "
					+ "ORDER BY  fecha DESC, hora DESC  ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, subambito);
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(3, paciente.getId());
			ResultSet resulSet = statement.executeQuery();
			while (resulSet.next()) {
				listaRegistros.add(getRegistroResulset(resulSet));
			}
			statement.close();
			logger.debug("SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   p.subambito =" + subambito + " AND  r.estado = "
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente=paciente.getId() "
					+ "ORDER BY  fecha DESC, hora DESC  ");
		} catch (SQLException e) {
			logger.error("SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   p.subambito =" + subambito + " AND  r.estado = "
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente=paciente.getId() "
					+ "ORDER BY  fecha DESC, hora DESC  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaRegistros;
	}

	public ArrayList<Registro> getListaRegistrosLight(Long subambito, Paciente paciente) {
		Connection connection = null;
		ArrayList<Registro> listaRegistros = new ArrayList<>();

		try {
			connection = super.getConexionBBDD();
			sql = "SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   p.subambito =? AND  r.estado = ? AND r.paciente=? "
					+ "ORDER BY  fecha DESC, hora DESC  ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, subambito);
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(3, paciente.getId());
			ResultSet resulSet = statement.executeQuery();
			while (resulSet.next()) {
				Registro registro = new Registro();
				registro.setId(resulSet.getLong("id"));
				registro.setProblema(new ProcesoDAO().getRegistroId(resulSet.getLong("problema")));
				registro.setDescripcion(resulSet.getString("descripcion"));
				registro.setPaciente(new Paciente(resulSet.getLong("paciente")));
				registro.setFecha(Utilidades.getFechaLocalDate(resulSet.getLong("fecha")));
				registro.setHora(resulSet.getLong("hora"));
				registro.setEstado(resulSet.getInt("estado"));
				registro.setUserid(new Usuario(resulSet.getString("userid")));
				registro.setProblema(new Proceso(resulSet.getLong("problema"), registro.getPaciente(), subambito));
				registro.setTiporegistro(resulSet.getLong("tiporegistro"));
				registro.setCanal(resulSet.getLong("canal"));
				registro.setPlantilla_edior(resulSet.getLong("plantilla_editor"));
				listaRegistros.add(getRegistroResulset(resulSet));
			}
			statement.close();
			logger.info(sql = "SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   p.subambito =" + subambito + " AND  r.estado = "
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente= " + paciente.getId()
					+ "ORDER BY  fecha DESC, hora DESC  ");
		} catch (SQLException e) {
			logger.error(sql = "SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   p.subambito =" + subambito + " AND  r.estado = "
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente= " + paciente.getId()
					+ "ORDER BY  fecha DESC, hora DESC  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaRegistros;
	}

	/**
	 * Gets the referencias externas.
	 *
	 * @param id the id
	 * @return the referencias externas
	 */
	@Override
	public boolean getReferenciasExternas(Long id) {
		return false;
	}

	/**
	 * Graba datos.
	 *
	 * @param object the object
	 * @return true, if successful
	 */
	@Override
	public boolean grabaDatos(Object object) {
		return false;
	}

	/**
	 * Actualiza datos.
	 *
	 * @param mensajeparam the mensajeparam
	 * @return true, if successful
	 */
	@Override
	public boolean actualizaDatos(Object mensajeparam) {
		return false;
	}

	/**
	 * Inserta datos.
	 *
	 * @param mensajeparam the mensajeparam
	 * @return true, if successful
	 */
	@Override
	public boolean insertaDatos(Object mensajeparam) {
		return false;
	}

	/**
	 * Gets the registro id.
	 *
	 * @param id the id
	 * @return the registro id
	 */
	@Override
	public Object getRegistroId(Long id) {
		return null;
	}

	/**
	 * Gets the sql where.
	 *
	 * @param cadena the cadena
	 * @return the sql where
	 */
	@Override
	public String getSqlWhere(String cadena) {
		return null;
	}

	/**
	 * Borra datos.
	 *
	 * @param objeto the objeto
	 * @return true, if successful
	 */
	@Override
	public boolean borraDatos(Object objeto) {
		return false;
	}
}