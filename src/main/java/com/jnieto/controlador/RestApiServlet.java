package com.jnieto.controlador;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.jnieto.dao.PacienteDAO;
import com.jnieto.dao.UsuarioDAO;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Usuario;
import com.jnieto.excepciones.LoginException;
import com.jnieto.excepciones.UsuarioBajaException;
import com.jnieto.utilidades.Utilidades;

/**
 * The Class RestApiServlet.
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 *          http://localhost:8080/continuidad/resources/registros/6/hhh/1
 */

@ApplicationPath("/")
@Path("/registros/{nhcpaciente}/{subambito}/{userid}")
public class RestApiServlet {

	/**
	 * Prints the feedback.
	 *
	 * @param nhcpaciente the nhc del paciente
	 * @param subambito   the subamBito
	 * 
	 *                    320 para oxigeno. 501 para mama. 502 colon. 555
	 *                    paliativos. 0 todos.
	 * @param userid      the usuario
	 * 
	 * @return the response
	 * @throws UsuarioBajaException
	 * @throws LoginException
	 */
	@GET
	// @Produces("text/plain")
	@Produces("application/json")

	public Response printDatosPaci(@PathParam("nhcpaciente") String nhcpaciente,
			@PathParam("subambito") String subambito, @PathParam("userid") String userid) {

		String jsonRespuesta = null;
		if (nhcpaciente.isEmpty() || subambito.isEmpty() || userid.isEmpty()) {
			jsonRespuesta = new Gson()
					.toJson("Uso:  http://hosts:8080/continuidad/resources/registros/numerohistoria/submambito \n "
							+ " numerohistoria: el nhc del paciente \n" + " subambito :	 \n"
							+ " idusuario:  identificaicón del usuario. ");

		} else {
			Usuario usuario = null;
			try {
				usuario = new UsuarioDAO().getUsuarioLogin(userid);
				if (validaSubambito(subambito) == false) {
					jsonRespuesta = new Gson()
							.toJson(" El valor del subambito= " + Proceso.SUBAMBITO_COLON + " " + Proceso.SUBAMBITO_MAMA
									+ " " + Proceso.SUBAMBITO_OXIGENO + " " + Proceso.SUBAMBITO_PALIATIVOS + " 0 ");
				} else {
					Paciente paciente = new PacienteDAO().getPacientePorNhcConRegistros(nhcpaciente,
							Long.parseLong(subambito));
					if (paciente == null) {
						jsonRespuesta = new Gson().toJson("Paciente " + nhcpaciente + " no encontrado.");
					} else {
						new AccesoControlador().doAccesoUsuarioPacienteRest(paciente, usuario);
						jsonRespuesta = new Gson().toJson(paciente);
					}
				}
			} catch (LoginException e) {
				jsonRespuesta = new Gson().toJson("Usuario " + userid + " no encontrado. ");
			} catch (UsuarioBajaException e) {
				jsonRespuesta = new Gson().toJson("Usuario " + userid + " de baja. ");
			}
		}
		return Response.status(200).entity(jsonRespuesta).build();
	}

	public boolean validaSubambito(String subambito) {
		if (Utilidades.isNumeric(subambito)) {
			Long subLong = Long.parseLong(subambito);
			if (subLong.equals(Proceso.SUBAMBITO_COLON) || subLong.equals(Proceso.SUBAMBITO_MAMA)
					|| subLong.equals(Proceso.SUBAMBITO_OXIGENO) || subLong.equals(Proceso.SUBAMBITO_PALIATIVOS)
					|| subLong.equals(new Long(0))) {
				return true;
			}
		}
		return false;
	}

}