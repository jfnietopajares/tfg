package com.jnieto.controlador;

import com.jnieto.dao.MensajesDAO;
import com.jnieto.dao.ParametroDAO;
import com.jnieto.dao.UsuarioDAO;
import com.jnieto.entity.Mensaje;
import com.jnieto.entity.ParametBBDD;
import com.jnieto.entity.RegistroMama;
import com.jnieto.ui.NotificacionInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class MensajeCtrlInserta. Inserta mensjes en la tabla en función de la
 * lógica y los datos configurados.
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 * 
 */
public class MensajeCtrlInserta {

	/** The mensaje. */
	private Mensaje mensaje;

	/**
	 * Instantiates a new mensaje ctrl inserta.
	 */
	public MensajeCtrlInserta() {
		mensaje = new Mensaje();
		mensaje.setId(new Long(0));
		mensaje.setEstado(Mensaje.MENSAJE_PENDIENTE_ENVIO);
	}

	/**
	 * Do inserta msg mama.
	 *
	 * @param registro the registro
	 */
	public void doInsertaMsgMama(RegistroMama registro) {
		mensaje.setPaciente(registro.getPaciente());

		if (registro.getId().equals(new Long(0))) {
			mensaje.setContenido(" Nuevo paciente en el proceso de mama. Id=" + registro.getPaciente().getNumerohc());
		} else if (registro.getFechatratamientoValue() != null) {
			mensaje.setContenido(" Paciente del proceso de mama cerrado . Id=" + registro.getPaciente().getNumerohc());
		} else {
			mensaje.setContenido(
					" Paciente del proceso de mama actualizado . Id=" + registro.getPaciente().getNumerohc());
		}
		mensaje.setContenido(mensaje.getContenido().concat(registro.toString()));

		procesaMensaje();
	}

	/**
	 * Procesa mensaje.
	 */
	public void procesaMensaje() {
		String canaleString = null;
		canaleString = new ParametroDAO().getRegistroPorCodigo(ParametBBDD.MENSAJES_CANALES).getValor();
		if (canaleString == null) {
			new NotificacionInfo("Sin datos para el parámetro " + "mensajes.canales");
		} else {
			String[] canalesArrayStrings = canaleString.trim().split(",");
			for (String canal : canalesArrayStrings) {
				int canalInt = Integer.parseInt(canal);
				if (canalInt == Mensaje.MENSAJE_TIPO_MAIL.getTipo()) {
					doInsertaMail();
				}
				if (canalInt == Mensaje.MENSAJE_TIPO_SMS.getTipo()) {
					mensaje.setTipo(Mensaje.MENSAJE_TIPO_SMS);
					mensaje.setId(new Long(0));
					new MensajesDAO().grabaDatos(mensaje);
				}
				if (canalInt == Mensaje.MENSAJE_TIPO_WHASTASP.getTipo()) {
					doInsertaWhatsapp();
				}
				if (canalInt == Mensaje.MENSAJE_TIPO_PANTALLA.getTipo()) {
					doInsertaPantalla();
				}
			}
		}
	}

	private void doInsertaMail() {
		mensaje.setTipo(Mensaje.MENSAJE_TIPO_MAIL);
		mensaje.setId(new Long(0));
		String destinoString = new ParametroDAO().getRegistroPorCodigo(ParametBBDD.MENSAJES_MAMA_CORREO_ENVIOS)
				.getValor();
		if (destinoString == null) {
			new NotificacionInfo("Sin datos para el parámetro " + ParametBBDD.MENSAJES_MAMA_CORREO_ENVIOS);
		} else {
			mensaje.setUserid_destino(destinoString);
			new MensajesDAO().grabaDatos(mensaje);
		}
	}

	private void doInsertaWhatsapp() {
		mensaje.setTipo(Mensaje.MENSAJE_TIPO_WHASTASP);
		mensaje.setId(new Long(0));
		String canaleString = new ParametroDAO().getRegistroPorCodigo(ParametBBDD.MENSAJES_MAMA_WHATSAPP).getValor();
		String[] usuarios = canaleString.trim().split(",");
		for (String usu : usuarios) {
			mensaje.setId(new Long(0));
			String tfString;
			tfString = new UsuarioDAO().getUsuarioUserid(usu).getTelefono();
			if (tfString != null) {
				mensaje.setUserid_destino(tfString);
				new MensajesDAO().grabaDatos(mensaje);
			}

		}
	}

	private void doInsertaPantalla() {
		mensaje.setTipo(Mensaje.MENSAJE_TIPO_PANTALLA);
		mensaje.setId(new Long(0));
		String canaleString = new ParametroDAO().getRegistroPorCodigo(ParametBBDD.MENSAJES_MAMA_PANTALLA).getValor();
		String[] usuarios = canaleString.trim().split(",");
		for (String usu : usuarios) {
			mensaje.setId(new Long(0));
			mensaje.setUserid_destino(usu);
			new MensajesDAO().grabaDatos(mensaje);
		}
	}
}
