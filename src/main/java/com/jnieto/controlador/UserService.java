package com.jnieto.controlador;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.UsuarioDAO;
import com.jnieto.entity.Usuario;
import com.jnieto.excepciones.LoginException;
import com.jnieto.excepciones.PasswordException;
import com.jnieto.excepciones.UsuarioBajaException;
import com.jnieto.ui.PantallaLogin;
import com.jnieto.utilidades.LDAP;
import com.jnieto.utilidades.Parametros;

/**
 * The Class UserService. Modificado de la original
 *
 * @author Alejandro Duarte.
 * 
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class UserService {

	/** The remembered users. */
	private static Map<String, String> rememberedUsers = new HashMap<>();

	/** The usuario. */
	private static Usuario usuario = null;

	/**
	 * Checks if is authentic user.
	 *
	 * @param username the username
	 * @param password the password
	 * @return the usuario
	 * @throws LoginException    the login exception
	 * @throws PasswordException
	 */
	public static Usuario isAuthenticUser(String username, String password)
			throws LoginException, UsuarioBajaException, PasswordException {

		String valorString = (String) MyUI.objParametros.get(Parametros.KEY_LDAP);
		if (valorString.trim().equals(new String("SI"))) {
			usuario = LDAP.login(username, password);
		} else {
			usuario = new UsuarioDAO().getUsuarioLogin(username);
			if (!DigestUtils.sha1Hex(password.trim()).equals(usuario.getPasswordhash().trim())) {
				throw new PasswordException(PantallaLogin.LOGIN_CONTRASEÑAINCORRECTA);
			}
		}

		return usuario;
	}

	/**
	 * Gets the usuario.
	 *
	 * @return the usuario
	 */
	public static Usuario getUsuario() {
		return usuario;
	}

	/**
	 * Gets the remembered user.
	 *
	 * @param id the id
	 * @return the remembered user
	 */
	public static String getRememberedUser(String id) {
		return rememberedUsers.get(id);
	}

	/**
	 * Removes the remembered user.
	 *
	 * @param id the id
	 */
	public static void removeRememberedUser(String id) {
		rememberedUsers.remove(id);
	}

}