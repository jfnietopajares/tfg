package com.jnieto.utilidades;

import com.jnieto.dao.ParametroDAO;
import com.jnieto.entity.ParametBBDD;
import com.jnieto.entity.Usuario;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.WebBrowser;

public class LlamadaExterna {
	String URL, APL, USR, ADDR, TIME, NHC, password, cadenaLlamada = null;

	public LlamadaExterna(String nhc, String app) {
		try {
			WebBrowser webBrowser = Page.getCurrent().getWebBrowser();
			APL = app;
			if (app.equals("jimena")) {
				URL = new ParametroDAO().getRegistroPorCodigo(ParametBBDD.URL_JIMENA).getValor();

			} else {
				/**
				 * para cada aplicación hay que configurar la llamada
				 */
			}
			USR = ((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME)).getUserid();
			ADDR = "1";
			TIME = Long.toString(System.currentTimeMillis());
			NHC = nhc;
			password = new ParametroDAO().getRegistroPorCodigo(ParametBBDD.CLAVE_APLICACIONES).getValor();

			cadenaLlamada = password + APL + USR;
			if (URL.indexOf("%ADDR%") != -1) {
				ADDR = webBrowser.getAddress();
			}
			cadenaLlamada += ADDR;
			cadenaLlamada += TIME;

			// Comprobaciones
			if (URL.indexOf("%NHC%") != -1 || URL.indexOf("%PAC%") != -1) {
				cadenaLlamada += NHC;
			}

			// Crear el token
			String TKN = MD5.SHA1(cadenaLlamada).toUpperCase();

			URL = URL.replace("%APL%", APL);
			URL = URL.replace("%USR%", USR);
			if (URL.indexOf("%ADDR%") != -1) {
				// La ip es opcional
				URL = URL.replace("%ADDR%", ADDR);
			}
			if (URL.indexOf("%NHC%") != -1) {
				URL = URL.replace("%NHC%", NHC);
			}
			if (URL.indexOf("%PAC%") != -1) {
				URL = URL.replace("%PAC%", NHC);
			}
			URL = URL.replace("%TIME%", TIME);
			URL = URL.replace("%TKN%", TKN);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	public String getUrl() {
		return URL;
	}
}
