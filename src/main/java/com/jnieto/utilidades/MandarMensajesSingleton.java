package com.jnieto.utilidades;

import com.jnieto.controlador.MensajeCtrProcesa;

/**
 * The Class MandarMensajesSingleton. Para que el timer que lanza MyUI no genere
 * objetos por cada usuario para mandar los mensajes pendientes creo una clase
 * con el del patrón de diseño "Singleton". Un sólo objeto de la clase
 */
public class MandarMensajesSingleton {

	private static MandarMensajesSingleton ms;

	private MandarMensajesSingleton() {
	}

	public static MandarMensajesSingleton getSingletonInstance() {
		if (ms == null) {
			ms = new MandarMensajesSingleton();
		} else {
		}

		return ms;
	}

	public void procesa() {
		new MensajeCtrProcesa();
	}
}
