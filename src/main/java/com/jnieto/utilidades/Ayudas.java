package com.jnieto.utilidades;

// TODO: Auto-generated Javadoc
/**
 * The Class Ayudas.
 */
public abstract class Ayudas {

	/** The Constant AYUDA_BOTONES_ABCA. */
	// ABCA Añadir, borrar, cerrar ayuda
	public static final String AYUDA_BOTONES_ABCA = "Descripción de botones: \n  <br>"
			+ "<ul> <li><b>&radic;</b>  Guardar: almacena los datos actuales  </li>"
			+ "<li><b>-</b>  Borrar: Borra el dato actual. </li>" + "<li><b>?</b> Ayuda. Este texto  </li>"
			+ "<li><b>x</b>  Cierra el formulario sin guardar los datos </li>" + "</ul>  <hr>";

	/** The Constant AYUDA_BOTONES_BARCA. */
	// BARCA Buscar Añadir, Refrescar Cerrar Ayuda
	public static final String AYUDA_BOTONES_BARCA = "Descripción de botones: \n  <br>"
			+ "<ul> <li><b>Buscar</b>  Búsqueda por el texto indicado </li>"
			+ "<li><b>+</b>  Añadir un nuevo dato </li>" + "<li><b>Refrescar</b> Actualiza la lista de datos  </li>"
			+ "<li><b>?</b> Ayuda. Este texto  </li>" + "<li><b>x</b>  Cierra el formulario de datos </li>"
			+ "</ul>  <hr>";

}
