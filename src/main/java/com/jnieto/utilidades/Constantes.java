package com.jnieto.utilidades;

import java.util.Arrays;
import java.util.List;

public abstract class Constantes {

	public static final String COOKIE_NAME = "recordar-sesion";

	public static final String SESSION_USERNAME = "usuario";

	public static final String PROPERTIESNOMBREFICHERO = System.getProperty("catalina.home")
			+ System.getProperty("file.separator") + "conf" + System.getProperty("file.separator")
			+ "continuidad.properties";

	public static final String DIRECTORIOREPORTS = System.getProperty("catalina.home")
			+ System.getProperty("file.separator") + "webapps" + System.getProperty("file.separator") + "reports"
			+ System.getProperty("file.separator");

	public static final String URLREPORTS = System.getProperty("file.separator") + "webapps"
			+ System.getProperty("file.separator") + "reports" + System.getProperty("file.separator");

	public final static String SEPARADOR_FECHA = "/";

	public final static String APLICACION_VERSION = "Ver 1.0 de 1 de abril de 2019";

	public final static String APLICACION_NOMBRE_VENTANA = "TFG JUAN NIETO";

	public final static String APLICACION_TITULO_PROGRAMA = "<h2><b>C o n t i n u i d a d</b></h2>";

//	public final static String APLICACION_SUBTITULO_PROGRAMA = "<h3><b> C o n t i n u i d a d</b></h3> ";

	public final static String APLICACION_PIE = "© Juan Nieto (2019) ";

	public final static String MYSQL_STRING = "MYSQL";

	public final static String ORACLE_STRING = "ORACLE";

	public final static String CONFIRMACION_TITULO = "Confirmación de acción:";

	public final static String CONFIRMACION_BORRADO_MENSAJE = "¿Seguro que quieres borrar estos datos ?";

	public final static String CONFIRMACION_SALIR_MENSAJE = "¿Seguro que quieres salir del programa ?";

	public final static String CONFIRMACION_BOTONSI = "Si";

	public final static String CONFIRMACION_BOTONNO = "No";

	public final static Long FEHAFIN_DEFECTO = new Long(99999999);

	public final static Long HORAFIN_DEFECTO = new Long(0);

	public final static String NOMBRESEXOHOMBRE = "Hombre";

	public final static String NOMBRESEXOMUJER = "Mujer";

	public final static String SEXO_DEFECTO = Constantes.NOMBRESEXOMUJER;

	public final static List<String> LISTASEXOS = Arrays.asList(Constantes.NOMBRESEXOHOMBRE,
			Constantes.NOMBRESEXOMUJER);

}
